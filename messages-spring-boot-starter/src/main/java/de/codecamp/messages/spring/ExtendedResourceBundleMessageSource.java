package de.codecamp.messages.spring;


import de.codecamp.messages.runtime.MessageFormatFactory;
import de.codecamp.messages.spring.impl.ExtendedMessageSourceSupport;
import java.text.MessageFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.util.Date;
import java.util.Locale;
import java.util.Set;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.context.MessageSourceResolvable;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.util.ObjectUtils;


/**
 * {@link ResourceBundleMessageSource}-based implementation of {@link ExtendedMessageSource}.
 * <p>
 * Typically the locale-less bundle file (e.g. basename.properties) contains localized messages for
 * a locale that is arbitrarily chosen as the default. This is an anti-pattern because the file name
 * does not reflect the language of its content and it's not possible to change this default without
 * copying or moving files around. The more sensible approach (as supported by this message source)
 * is to use the locale-less bundle file only for language independent text and make the
 * {@link #setDefaultLocale(Locale) default locale an explicit configuration}. The explicit locale
 * also replaces any use of the {@link Locale#getDefault() default / system locale} within this
 * message source.
 *
 * <ul>
 * <li>Can {@link #setLogWhenMissing(boolean) log an error} when a key could not be resolved for the
 * requested locale. Turned on per default.</li>
 * <li>Can {@link #setLogWhenNullArg(boolean) log an error} when a message argument is null, as null
 * values are not handled in any meaningful way by {@link MessageFormat}. Turned on per
 * default.</li>
 * <li>Can {@link #setFallbackToDefaultLocale(boolean) fall back} to the {@link #getDefaultLocale()
 * default locale} on a per-key basis, when the message for the requested locale is missing or
 * empty. Turned on per default. This is different from the Vanilla
 * {@link #setFallbackToSystemLocale(boolean)}, that can only fall back on a per-file basis and only
 * uses the system locale.</li>
 * <li>Uses {@link #getDefaultLocale()} when the requested locale is null. Vanilla
 * {@link ResourceBundleMessageSource} would use {@link Locale#getDefault()}).</li>
 * <li>Supports the java.time." package as format arguments. {@link MessageFormat} in Java 8 (and
 * maybe beyond) only supports {@link Date}. For {@link Date} and {@link Instant} the time zone from
 * the {@link LocaleContextHolder#getTimeZone() LocaleContextHolder} is used to format them.</li>
 * <li>For improved validation of message bundles, the empty string is considered to be a missing
 * translation. A single <a href="https://en.wikipedia.org/wiki/Zero-width_space">zero-width
 * space</a> ({@code \u200B}) can be used to confirm that the empty string is actually intended and
 * will be replaced with the empty string accordingly.</li>
 * </ul>
 */
public class ExtendedResourceBundleMessageSource
  extends
    ResourceBundleMessageSource
  implements
    ExtendedMessageSource
{

  protected ExtendedMessageSourceSupport support = new ExtendedMessageSourceSupport();


  public ExtendedResourceBundleMessageSource()
  {
    super.setFallbackToSystemLocale(false);
  }


  public MessageFormatFactory getMessageFormatFactory()
  {
    return support.getMessageFormatFactory();
  }

  public void setMessageFormatFactory(MessageFormatFactory messageFormatFactory)
  {
    support.setMessageFormatFactory(messageFormatFactory);
  }

  public MessageArgConverterRegistry getMessageArgConverterRegistry()
  {
    return support.getMessageArgConverterRegistry();
  }

  public void setMessageArgConverterRegistry(
      MessageArgConverterRegistry messageArgConverterRegistry)
  {
    support.setMessageArgConverterRegistry(messageArgConverterRegistry);
  }


  public boolean isFallbackToDefaultLocale()
  {
    return support.isFallbackToDefaultLocale();
  }

  public void setFallbackToDefaultLocale(boolean fallbackToDefaultLocale)
  {
    support.setFallbackToDefaultLocale(fallbackToDefaultLocale);
  }

  /**
   * @deprecated This method is not supported. Use {@link #setFallbackToDefaultLocale(boolean)}
   *             instead.
   */
  @Deprecated
  @Override
  public void setFallbackToSystemLocale(boolean fallbackToSystemLocale)
  {
    throw new UnsupportedOperationException("Use setFallbackToDefaultLocale(boolean) instead.");
  }


  public boolean isLogWhenMissing()
  {
    return support.isLogWhenMissing();
  }

  public void setLogWhenMissing(boolean logWhenMissing)
  {
    support.setLogWhenMissing(logWhenMissing);
  }

  public boolean isLogWhenNullArg()
  {
    return support.isLogWhenNullArg();
  }

  public void setLogWhenNullArg(boolean logWhenNullArg)
  {
    support.setLogWhenNullArg(logWhenNullArg);
  }

  @Override
  public void setUseCodeAsDefaultMessage(boolean useCodeAsDefaultMessage)
  {
    super.setUseCodeAsDefaultMessage(useCodeAsDefaultMessage);
    support.setUseCodeWhenMissing(useCodeAsDefaultMessage);
  }

  public void setShortenCodeWhenMissing(boolean shortenCodeWhenMissing)
  {
    support.setShortenCodeWhenMissing(shortenCodeWhenMissing);
  }

  public void setMissingMessagePattern(String missingMessagePattern)
  {
    support.setMissingMessagePattern(missingMessagePattern);
  }


  @Override
  public Set<Locale> getAvailableLocales()
  {
    return support.getAvailableLocales();
  }

  public void setAvailableLocales(Set<Locale> availableLocales)
  {
    support.setAvailableLocales(availableLocales);
  }

  @Override
  public Locale getDefaultLocale()
  {
    return support.getDefaultLocale();
  }

  @Override
  public void setDefaultLocale(Locale defaultLocale)
  {
    support.setDefaultLocale(defaultLocale);
  }

  @Override
  public ZoneId getDefaultTimeZone()
  {
    return support.getDefaultTimeZone();
  }

  @Override
  public void setDefaultTimeZone(ZoneId defaultTimeZone)
  {
    support.setDefaultTimeZone(defaultTimeZone);
  }


  @Override
  protected String getMessageInternal(String code, Object[] args, Locale locale)
  {
    return support.getMessageInternal(code, args, locale, super::getMessageInternal,
        this::getDefaultMessage);
  }

  @Override
  protected String getDefaultMessage(String code)
  {
    return support.getDefaultMessage(code);
  }

  @Override
  protected final MessageFormat createMessageFormat(String message, Locale locale)
  {
    return support.createMessageFormat(message, locale);
  }

  @Override
  protected Object[] resolveArguments(Object[] args, Locale locale)
  {
    /*
     * small optimization over the original implementation: only copy array when it actually
     * contains MessageSourceResolvable
     */
    if (ObjectUtils.isEmpty(args))
      return ArrayUtils.EMPTY_OBJECT_ARRAY;

    boolean argsCopied = false;
    for (int i = 0; i < args.length; i++)
    {
      Object argValue = args[i];

      if (argValue instanceof MessageSourceResolvable)
      {
        if (!argsCopied)
          args = args.clone();

        args[i] = getMessage((MessageSourceResolvable) argValue, locale);
      }
    }

    return args;
  }

}
