package de.codecamp.messages.spring;


import de.codecamp.messages.proxy.MessageProxy;
import de.codecamp.messages.proxy.MessageProxyUtils;
import de.codecamp.messages.proxy.NamedArgsMessageProvider;
import java.lang.reflect.Modifier;
import org.springframework.util.ReflectionUtils;


/**
 * Utility methods related to message access.
 */
public final class MessageAccessUtils
{

  private MessageAccessUtils()
  {
    // utility class
  }


  /**
   * Inject message proxies into any appropriate fields of the given bean.
   *
   * @param bean
   *          the bean using message proxies
   * @param messageProvider
   *          the provider used to retrieve messages
   */
  public static void injectMessageProxies(Object bean, NamedArgsMessageProvider messageProvider)
  {
    ReflectionUtils.doWithFields(bean.getClass(), field ->
    {
      if (!MessageProxy.class.isAssignableFrom(field.getType()))
        return;

      ReflectionUtils.makeAccessible(field);
      if (ReflectionUtils.getField(field, bean) != null)
        return;

      Class<? extends MessageProxy> messageProxyType =
          field.getType().asSubclass(MessageProxy.class);
      MessageProxy messageProxy =
          MessageProxyUtils.createNamedArgsMessageProxy(messageProxyType, messageProvider);

      ReflectionUtils.setField(field, bean, messageProxy);
    }, field -> !Modifier.isStatic(field.getModifiers()));
  }

}
