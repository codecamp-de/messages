package de.codecamp.messages.spring.impl;


import java.text.FieldPosition;
import java.text.Format;
import java.text.MessageFormat;
import java.text.ParsePosition;


/**
 * Spring's message sources require instances of Java's {@link MessageFormat}. Unfortunately many
 * important methods of {@link Format} and {@link MessageFormat} are final which makes their
 * behavior extremely hard to customize. This adapter basically allows to intercept a format method
 * and adapt its behavior, by using the actual formatter as the only (wrapped) {@link Format}.
 * <b>However, the actual arguments passed to this adapter must be wrapped in an additional Object
 * array!</b> This is something that unfortunately can't be encapsulated here.
 */
public abstract class MessageFormatAdapter
  extends
    MessageFormat
{

  public MessageFormatAdapter()
  {
    super("{0}");
    setFormat(0, new Format()
    {

      @Override
      public StringBuffer format(Object obj, StringBuffer result, FieldPosition pos)
      {
        return wrappedFormat(obj, result, pos);
      }

      @Override
      public Object parseObject(String source, ParsePosition pos)
      {
        throw new UnsupportedOperationException();
      }

    });
  }

  protected abstract StringBuffer wrappedFormat(Object obj, StringBuffer result, FieldPosition pos);

}
