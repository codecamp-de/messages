package de.codecamp.messages.spring.impl;


import de.codecamp.messages.spring.DefaultMessageAccessor;
import de.codecamp.messages.spring.MessageAccessUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.annotation.Lazy;


/**
 * A {@link BeanPostProcessor} that injects message proxies into all matching fields.
 */
public class MessageProxyInjectingBeanPostProcessor
  implements
    BeanPostProcessor
{

  @Autowired
  @Lazy
  private DefaultMessageAccessor messageAccessor;


  @Override
  public Object postProcessBeforeInitialization(Object bean, String beanName)
    throws BeansException
  {
    // at least skip some beans that will definitely not use message proxies
    String beanClassName = bean.getClass().getName();
    if (beanClassName.startsWith("org.springframework."))
    {
      return bean;
    }

    MessageAccessUtils.injectMessageProxies(bean, messageAccessor::getMessage);

    return bean;
  }

}
