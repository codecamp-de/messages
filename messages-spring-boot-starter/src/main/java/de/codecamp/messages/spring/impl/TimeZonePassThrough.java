package de.codecamp.messages.spring.impl;


import de.codecamp.messages.spring.ExtendedMessageSource;
import java.time.ZoneId;
import java.util.function.Supplier;


/**
 * Allows a time zone to be passed through API which is internally used by
 * {@link ExtendedMessageSource} and does not have a time zone in its signature.
 */
public final class TimeZonePassThrough
{

  private static final ThreadLocal<ZoneId> TIMEZONE = new ThreadLocal<>();


  private TimeZonePassThrough()
  {
    // utility class
  }


  public static String withTimeZone(ZoneId timeZone, Supplier<String> messageSupplier)
  {
    setTimeZone(timeZone);
    try
    {
      return messageSupplier.get();
    }
    finally
    {
      clearTimeZone();
    }
  }


  public static ZoneId getTimeZone()
  {
    return TIMEZONE.get();
  }

  public static void setTimeZone(ZoneId timeZone)
  {
    TIMEZONE.set(timeZone);
  }

  public static void clearTimeZone()
  {
    TIMEZONE.remove();
  }

}
