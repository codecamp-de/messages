package de.codecamp.messages.spring.autoconfigure;


import de.codecamp.messages.spring.ExtendedMessageSource;
import de.codecamp.messages.spring.impl.ExtendedMessageSourceSupport;
import jakarta.validation.constraints.Pattern;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Locale;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.convert.DurationUnit;
import org.springframework.validation.annotation.Validated;


/**
 * Configuration properties for {@link ExtendedMessageSource}.
 */
@Validated
@ConfigurationProperties(prefix = ExtendedMessageSourceProperties.PREFIX)
public class ExtendedMessageSourceProperties
{

  public static final String PREFIX = "codecamp.messages";


  /**
   * Basenames of the message bundles to load. If combined with <b>bundle-file-patterns</b>, the
   * basenames provided here will have higher precedence when resolving message keys. Can be used to
   * specify bundles that should override messages of other bundles. Please note that if e.g. all
   * message bundle files are kept in a "messages" folder, that folder is part of the effective
   * basename.
   */
  private String[] basenames;

  /**
   * Locales available in the application. Leave empty to discover automatically. See
   * <b>bundle-file-patterns</b> to define where to look.
   */
  private Locale[] availableLocales;

  /**
   * The default locale for your application. Used by the message source when no locale is specified
   * on requesting a message. If specified, must be one of the available locales. When empty,
   * Locale.getDefault() will be attempted. Missing default locale will lead to an exception.
   */
  private Locale defaultLocale;

  private ZoneId defaultTimeZone;

  /**
   * Comma-separated list of Ant-style resource path patterns that will be used to automatically
   * discover message bundles and available locales.
   */
  private String[] bundleFilePatterns = {"classpath*:messages/**/*.properties"};

  /**
   * Message bundles encoding.
   */
  private Charset encoding = StandardCharsets.UTF_8;

  /**
   * Whether to fall back to the default locale if no message (or an empty string) was found for the
   * requested locale. Recommended even for production.
   */
  private boolean fallbackToDefaultLocale = true;

  /**
   * Whether to log an error whenever a message for the requested locale could not be found, before
   * any fallback to the default locale.
   */
  private boolean logWhenMissing = true;

  /**
   * Whether to use the message key code as message instead of throwing a
   * <b>NoSuchMessageException</b>. This is considered <b>after</b>
   * <em>fallback-to-default-locale</em>.
   */
  private boolean useCodeWhenMissing = true;

  /**
   * Whether to shorten the code when returned instead of a (missing) message. Only has an effect
   * when <em>use-code-when-missing</em> is <em>true</em>.
   */
  private boolean shortenCodeWhenMissing = true;

  /**
   * The pattern to use when returning the code for a missing message. <code>{code}</code> will be
   * replaced with the (possibly shortened) code.
   */
  @Pattern(regexp = ".*\\{code\\}.*")
  private String missingMessagePattern =
      "!" + ExtendedMessageSourceSupport.MISSING_MESSAGE_PATTERN_CODE_PLACEHOLDER + "!";


  /**
   * Loaded resource bundle files cache duration. When not set, bundles are cached forever. If a
   * duration suffix is not specified, seconds will be used.
   */
  @DurationUnit(ChronoUnit.SECONDS)
  private Duration cacheDuration;

  /**
   * Whether to always apply the MessageFormat rules, parsing even messages without arguments.
   */
  private boolean alwaysUseMessageFormat = false;

  /**
   * Set other sources for default locales, like <b>Locale#setDefault(Locale)</b>, to the same
   * default as configured in <b>default-locale</b>.
   */
  private boolean normalizeDefaultLocales = true;


  public ExtendedMessageSourceProperties()
  {
  }


  public String[] getBasenames()
  {
    return basenames;
  }

  public void setBasenames(String[] basenames)
  {
    this.basenames = basenames;
  }

  public Locale[] getAvailableLocales()
  {
    return availableLocales;
  }

  public void setAvailableLocales(Locale[] availableLocales)
  {
    this.availableLocales = availableLocales;
  }

  public Locale getDefaultLocale()
  {
    return defaultLocale;
  }

  public void setDefaultLocale(Locale defaultLocale)
  {
    this.defaultLocale = defaultLocale;
  }

  public ZoneId getDefaultTimeZone()
  {
    return defaultTimeZone;
  }

  public void setDefaultTimeZone(ZoneId defaultTimeZone)
  {
    this.defaultTimeZone = defaultTimeZone;
  }

  public String[] getBundleFilePatterns()
  {
    return bundleFilePatterns;
  }

  public void setBundleFilePatterns(String[] bundleFilePatterns)
  {
    this.bundleFilePatterns = bundleFilePatterns;
  }

  public Charset getEncoding()
  {
    return encoding;
  }

  public void setEncoding(Charset encoding)
  {
    this.encoding = encoding;
  }

  public boolean isFallbackToDefaultLocale()
  {
    return fallbackToDefaultLocale;
  }

  public void setFallbackToDefaultLocale(boolean fallbackToDefaultLocale)
  {
    this.fallbackToDefaultLocale = fallbackToDefaultLocale;
  }

  public boolean isLogWhenMissing()
  {
    return logWhenMissing;
  }

  public void setLogWhenMissing(boolean logWhenMissing)
  {
    this.logWhenMissing = logWhenMissing;
  }

  public boolean isUseCodeWhenMissing()
  {
    return useCodeWhenMissing;
  }

  public void setUseCodeWhenMissing(boolean useCodeWhenMissing)
  {
    this.useCodeWhenMissing = useCodeWhenMissing;
  }

  public boolean isShortenCodeWhenMissing()
  {
    return shortenCodeWhenMissing;
  }

  public void setShortenCodeWhenMissing(boolean shortenCodeWhenMissing)
  {
    this.shortenCodeWhenMissing = shortenCodeWhenMissing;
  }

  public String getMissingMessagePattern()
  {
    return missingMessagePattern;
  }

  public void setMissingMessagePattern(String missingCodePattern)
  {
    this.missingMessagePattern = missingCodePattern;
  }

  public Duration getCacheDuration()
  {
    return cacheDuration;
  }

  public void setCacheDuration(Duration cacheDuration)
  {
    this.cacheDuration = cacheDuration;
  }

  public boolean isAlwaysUseMessageFormat()
  {
    return alwaysUseMessageFormat;
  }

  public void setAlwaysUseMessageFormat(boolean alwaysUseMessageFormat)
  {
    this.alwaysUseMessageFormat = alwaysUseMessageFormat;
  }

  public boolean isNormalizeDefaultLocales()
  {
    return normalizeDefaultLocales;
  }

  public void setNormalizeDefaultLocales(boolean normalizeDefaultLocales)
  {
    this.normalizeDefaultLocales = normalizeDefaultLocales;
  }

}
