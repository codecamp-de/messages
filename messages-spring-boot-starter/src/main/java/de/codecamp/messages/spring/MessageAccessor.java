package de.codecamp.messages.spring;


import de.codecamp.messages.MessageKeyUtils;
import de.codecamp.messages.ResolvableMessage;
import de.codecamp.messages.proxy.MessageProxy;
import de.codecamp.messages.proxy.MessageProxyUtils;
import java.time.ZoneId;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.support.MessageSourceAccessor;


/**
 * The {@link MessageAccessor} provides easy access to the messages of the application, implicitly
 * considering the default and current (user-based) locale during lookup. Essentially replaces
 * {@link MessageSourceAccessor}.
 */
public interface MessageAccessor
{

  /**
   * Returns all locales available in the application.
   *
   * @return all locales available in the application
   */
  Set<Locale> getAvailableLocales();

  /**
   * Returns the default locale of the application.
   *
   * @return the default locale of the application
   */
  Locale getDefaultLocale();

  /**
   * Returns the default locale of the application.
   *
   * @return the default locale of the application
   */
  ZoneId getDefaultTimeZone();


  /**
   * Returns the current locale that will be used to lookup localized resources.
   *
   * @return the current locale that will be used to lookup localized resources
   */
  Locale getLocale();

  /**
   * Returns the current time zone that will be used to format message arguments.
   *
   * @return the current time zone that will be used to format message arguments
   */
  ZoneId getTimeZone();


  /**
   * Returns locales in the order they should be used to attempt to lookup localized resources.
   * <p>
   * This can be useful to achieve consistent behavior when other sources for localizations need to
   * also be accessed. The result can optionally contain the parent locales (other than the
   * {@link Locale#ROOT root locale}) for each main locale, depending on whether those other sources
   * do handle that fallback on their own.
   *
   * @param includeParentLevels
   *          whether to include implicit parent locales (other than the {@link Locale#ROOT root
   *          locale})
   * @return the locales in the order they should be used to attempt to lookup localized resources
   */
  Locale[] getCandidateLocales(boolean includeParentLevels);


  /**
   * Returns the message for the given message key.
   *
   * @param messageKey
   *          the message key code
   * @return the message
   * @throws NoSuchMessageException
   *           if no corresponding message was found
   * @see MessageKeyUtils
   */
  default String getMessage(String messageKey)
  {
    return getMessage(messageKey, new Object[0]);
  }

  /**
   * Returns the message for the given message key and arguments.
   *
   * @param messageKey
   *          the message key code
   * @param args
   *          either a regular array of message arguments; or a single element array containing a
   *          map of named arguments ({@code Map<String, Object>}) or a {@link ResolvableMessage};
   *          may be null
   * @return the message
   * @throws NoSuchMessageException
   *           if no corresponding message was found
   * @see MessageKeyUtils
   */
  String getMessage(String messageKey, Object... args);

  /**
   * Returns the message for the given message key and arguments.
   *
   * @param messageKey
   *          the message key code
   * @param args
   *          the map of named arguments; may be null
   * @return the message
   * @throws NoSuchMessageException
   *           if no corresponding message was found
   * @see MessageKeyUtils
   */
  default String getMessage(String messageKey, Map<String, Object> args)
  {
    return getMessage(messageKey, args != null ? new Object[] {args} : new Object[0]);
  }

  /**
   * Returns the message for the given {@link ResolvableMessage}.
   *
   * @param resolvableMessage
   *          the resolvable message
   * @return the resolved message or null
   * @throws NoSuchMessageException
   *           if no corresponding message was found
   */
  default String getMessage(ResolvableMessage resolvableMessage)
  {
    return getMessage(resolvableMessage.getCode(), resolvableMessage);
  }


  /**
   * Returns a {@link MessageAccessor} for the specified locale.
   *
   * @param locale
   *          the desired locale
   * @return a {@link MessageAccessor} for the specified locale
   */
  MessageAccessor forLocale(Locale locale);


  /**
   * Returns a message proxy for the given message proxy interface.
   *
   * @param <MP>
   *          the message proxy type
   * @param messageProxyType
   *          the message proxy type
   * @return a message proxy for the given message proxy interface
   */
  default <MP extends MessageProxy> MP resolve(Class<MP> messageProxyType)
  {
    return MessageProxyUtils.createNamedArgsMessageProxy(messageProxyType, this::getMessage);
  }

}
