package de.codecamp.messages.spring;


import java.time.ZoneId;
import java.util.Locale;
import java.util.Optional;
import java.util.TimeZone;
import org.springframework.context.i18n.LocaleContext;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.i18n.TimeZoneAwareLocaleContext;


/**
 * The default implementation of {@link MessageAccessor}, using {@link LocaleContextHolder} to
 * determine the current locale and time zone.
 * <p>
 * This implementation is not serializable.
 */
public class DefaultMessageAccessor
  extends
    AbstractMessageAccessor
{

  public DefaultMessageAccessor(ExtendedMessageSource messageSource)
  {
    super(messageSource);
  }

  private DefaultMessageAccessor(ExtendedMessageSource messageSource, Locale locale,
      ZoneId timeZone)
  {
    super(messageSource, locale, timeZone);
  }


  @Override
  protected Optional<Locale> getContextualLocale()
  {
    return Optional.ofNullable(LocaleContextHolder.getLocaleContext())
        .map(LocaleContext::getLocale);
  }

  @Override
  protected Optional<ZoneId> getContextualTimeZone()
  {
    return Optional.ofNullable(LocaleContextHolder.getLocaleContext())
        .map(lc -> lc instanceof TimeZoneAwareLocaleContext tzalc ? tzalc.getTimeZone() : null)
        .map(TimeZone::toZoneId);
  }


  @Override
  public DefaultMessageAccessor forLocale(Locale locale)
  {
    return new DefaultMessageAccessor(messageSource, locale, timeZone);
  }

}
