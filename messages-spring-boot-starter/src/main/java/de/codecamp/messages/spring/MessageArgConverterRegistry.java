package de.codecamp.messages.spring;


import java.time.ZoneId;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;


/**
 * <p>
 * Use {@link Order} and/or {@link Ordered} on the converters to determine the order in which they
 * should be called. Within the same order value, the order of multiple converters is undefined.
 */
public interface MessageArgConverterRegistry
{

  Map<String, Object> convertValues(Map<String, Object> args, Locale locale, ZoneId timeZone);

  List<Object> convertValues(List<Object> args, Locale locale, ZoneId timeZone);

  Object[] convertValues(Object[] args, Locale locale, ZoneId timeZone);

}
