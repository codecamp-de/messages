package de.codecamp.messages.spring;


import static java.util.Objects.requireNonNull;

import java.time.ZoneId;
import java.util.LinkedHashSet;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import org.apache.commons.lang3.LocaleUtils;


public abstract class AbstractMessageAccessor
  implements
    MessageAccessor
{

  protected final ExtendedMessageSource messageSource;

  protected final Locale locale;

  protected final ZoneId timeZone;


  protected AbstractMessageAccessor(ExtendedMessageSource messageSource)
  {
    this.messageSource = requireNonNull(messageSource, "messageSource must not be null");
    this.locale = null;
    this.timeZone = null;
  }

  protected AbstractMessageAccessor(ExtendedMessageSource messageSource, Locale locale,
      ZoneId timeZone)
  {
    this.messageSource = requireNonNull(messageSource, "messageSource must not be null");
    this.locale = requireNonNull(locale, "locale must not be null");
    this.timeZone = timeZone;
  }


  @Override
  public Set<Locale> getAvailableLocales()
  {
    return messageSource.getAvailableLocales();
  }

  @Override
  public Locale getDefaultLocale()
  {
    return messageSource.getDefaultLocale();
  }

  @Override
  public ZoneId getDefaultTimeZone()
  {
    return messageSource.getDefaultTimeZone();
  }


  @Override
  public Locale getLocale()
  {
    return Optional.ofNullable(locale) //
        .or(this::getContextualLocale) //
        .orElseGet(this::getDefaultLocale);
  }

  /**
   * Returns the local of the current context, if available, without any defaults or fallbacks.
   *
   * @return the local of the current context, if available
   */
  protected abstract Optional<Locale> getContextualLocale();

  @Override
  public ZoneId getTimeZone()
  {
    return Optional.ofNullable(timeZone) //
        .or(this::getContextualTimeZone) //
        .orElseGet(this::getDefaultTimeZone);
  }

  /**
   * Returns the time zone of the current context, if available, without any defaults or fallbacks.
   *
   * @return the time zone of the current context, if available
   */
  protected abstract Optional<ZoneId> getContextualTimeZone();


  @Override
  public Locale[] getCandidateLocales(boolean includeParentLevels)
  {
    Locale contextLocale = getLocale();
    Locale defaultLocale = getDefaultLocale();

    if (includeParentLevels)
    {
      // keep order; avoid duplicates
      Set<Locale> locales = new LinkedHashSet<>();
      locales.addAll(LocaleUtils.localeLookupList(contextLocale));
      locales.addAll(LocaleUtils.localeLookupList(defaultLocale));
      return locales.toArray(new Locale[locales.size()]);
    }
    else
    {
      if (!Objects.equals(contextLocale, defaultLocale))
        return new Locale[] {contextLocale, defaultLocale};
      else
        return new Locale[] {contextLocale};
    }
  }


  @Override
  public String getMessage(String messageKey, Object... args)
  {
    return messageSource.getMessage(messageKey, args, getLocale(), getTimeZone());
  }

}
