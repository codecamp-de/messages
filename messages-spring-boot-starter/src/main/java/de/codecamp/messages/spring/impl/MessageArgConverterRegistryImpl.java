package de.codecamp.messages.spring.impl;


import de.codecamp.messages.runtime.MessageArgConverter;
import de.codecamp.messages.spring.MessageArgConverterRegistry;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


public class MessageArgConverterRegistryImpl
  implements
    MessageArgConverterRegistry
{

  private final List<MessageArgConverter> converters;


  public MessageArgConverterRegistryImpl(List<MessageArgConverter> converters)
  {
    this.converters = converters;
  }


  @Override
  public Map<String, Object> convertValues(Map<String, Object> args, Locale locale, ZoneId timeZone)
  {
    if (converters == null || converters.isEmpty() || args == null || args.isEmpty())
      return args;

    Map<String, Object> result = new HashMap<>(args.size());
    args.forEach((key, value) -> result.put(key, convertValue(value, locale, timeZone)));
    return result;
  }

  @Override
  public List<Object> convertValues(List<Object> args, Locale locale, ZoneId timeZone)
  {
    if (converters == null || converters.isEmpty() || args == null || args.isEmpty())
      return args;

    List<Object> result = new ArrayList<>(args.size());
    args.forEach(value -> result.add(convertValue(value, locale, timeZone)));
    return result;
  }

  @Override
  public Object[] convertValues(Object[] args, Locale locale, ZoneId timeZone)
  {
    if (converters == null || converters.isEmpty() || args == null || args.length == 0)
      return args;

    Object[] result = new Object[args.length];
    for (int i = 0; i < args.length; i++)
      result[i] = convertValue(args[i], locale, timeZone);
    return result;
  }

  private Object convertValue(Object value, Locale locale, ZoneId timeZone)
  {
    for (MessageArgConverter converter : converters)
    {
      value = converter.convert(value, locale, timeZone);
    }

    return value;
  }

}
