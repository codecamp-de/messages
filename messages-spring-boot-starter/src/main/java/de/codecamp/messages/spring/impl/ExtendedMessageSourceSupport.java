package de.codecamp.messages.spring.impl;


import static java.util.Objects.requireNonNullElse;

import de.codecamp.messages.ResolvableMessage;
import de.codecamp.messages.runtime.MessageFormatFactory;
import de.codecamp.messages.spring.MessageArgConverterRegistry;
import java.text.FieldPosition;
import java.text.Format;
import java.text.MessageFormat;
import java.time.ZoneId;
import java.util.Collections;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


public class ExtendedMessageSourceSupport
{

  public static final String MISSING_MESSAGE_PATTERN_CODE_PLACEHOLDER = "{code}";


  /**
   * Unicode's zero-width space character. Can be used to explicitly signal that the empty string is
   * actually intended for a message.
   */
  private static final String ZERO_WIDTH_SPACE = "\u200B";


  private static final Log MISSING_LOG = LogFactory.getLog("de.codecamp.messages.missing"); // NOPMD:ProperLogger

  private MessageFormatFactory messageFormatFactory;

  private MessageArgConverterRegistry messageArgConverterRegistry;

  private Set<Locale> availableLocales = Collections.emptySet();

  private Locale defaultLocale;

  private ZoneId defaultTimeZone;

  private boolean fallbackToDefaultLocale = true;

  private boolean logWhenMissing = true;

  private boolean logWhenNullArg = true;

  private boolean useCodeWhenMissing = true;

  private boolean shortenCodeWhenMissing = true;

  private String missingMessagePattern = "!" + MISSING_MESSAGE_PATTERN_CODE_PLACEHOLDER + "!";


  /**
   * Cache keys of missing messages so the error is only logged once per application start.
   */
  private final Set<String> missingMessages = ConcurrentHashMap.newKeySet();


  public ExtendedMessageSourceSupport()
  {
  }


  public MessageFormatFactory getMessageFormatFactory()
  {
    return messageFormatFactory;
  }

  public void setMessageFormatFactory(MessageFormatFactory messageFormatFactory)
  {
    this.messageFormatFactory = messageFormatFactory;
  }

  public MessageArgConverterRegistry getMessageArgConverterRegistry()
  {
    return messageArgConverterRegistry;
  }

  public void setMessageArgConverterRegistry(
      MessageArgConverterRegistry messageArgConverterRegistry)
  {
    this.messageArgConverterRegistry = messageArgConverterRegistry;
  }


  public boolean isFallbackToDefaultLocale()
  {
    return fallbackToDefaultLocale;
  }

  public void setFallbackToDefaultLocale(boolean fallbackToDefaultLocale)
  {
    this.fallbackToDefaultLocale = fallbackToDefaultLocale;
  }



  public boolean isLogWhenMissing()
  {
    return logWhenMissing;
  }

  public void setLogWhenMissing(boolean logWhenMissing)
  {
    this.logWhenMissing = logWhenMissing;
  }

  public boolean isLogWhenNullArg()
  {
    return logWhenNullArg;
  }

  public void setLogWhenNullArg(boolean logWhenNullArg)
  {
    this.logWhenNullArg = logWhenNullArg;
  }

  public boolean isUseCodeWhenMissing()
  {
    return useCodeWhenMissing;
  }

  public void setUseCodeWhenMissing(boolean useCodeWhenMissing)
  {
    this.useCodeWhenMissing = useCodeWhenMissing;
  }

  public boolean isShortenCodeWhenMissing()
  {
    return shortenCodeWhenMissing;
  }

  public void setShortenCodeWhenMissing(boolean shortenCodeWhenMissing)
  {
    this.shortenCodeWhenMissing = shortenCodeWhenMissing;
  }

  public String getMissingMessagePattern()
  {
    return missingMessagePattern;
  }

  public void setMissingMessagePattern(String missingMessagePattern)
  {
    if (missingMessagePattern != null
        && !missingMessagePattern.contains(MISSING_MESSAGE_PATTERN_CODE_PLACEHOLDER))
    {
      throw new IllegalArgumentException("missingMessagePattern must contain "
          + MISSING_MESSAGE_PATTERN_CODE_PLACEHOLDER + " placeholder.");
    }

    this.missingMessagePattern = missingMessagePattern;
  }


  public Set<Locale> getAvailableLocales()
  {
    return availableLocales;
  }

  public void setAvailableLocales(Set<Locale> availableLocales)
  {
    this.availableLocales = Set.copyOf(availableLocales);
  }

  public Locale getDefaultLocale()
  {
    return defaultLocale;
  }

  public void setDefaultLocale(Locale defaultLocale)
  {
    this.defaultLocale = defaultLocale;
  }

  public ZoneId getDefaultTimeZone()
  {
    return defaultTimeZone;
  }

  public void setDefaultTimeZone(ZoneId defaultTimeZone)
  {
    this.defaultTimeZone = defaultTimeZone;
  }


  public String getMessageInternal(String code, Object[] args, Locale locale,
      GetMessageInternalDelegate internalMessageDelegate,
      GetDefaultMessageDelegate defaultMessageDelegate)
  {
    /*
     * For a null locale super.getMessageInternal(..) would fallback to Locale.getDefault. Use the
     * default from this message source instead.
     */
    if (locale == null)
      locale = getDefaultLocale();

    if (args == null)
    {
      args = ArrayUtils.EMPTY_OBJECT_ARRAY;
      /*
       * Why args cannot be null: args is wrapped in an object array further down and later treated
       * as the argument of a sub-format. But MessageFormat would only see that the argument is null
       * and print "null" without even considering the sub-format. That's why it is replaced with an
       * empty array.
       */
    }
    else if (isLogWhenNullArg())
    {
      for (int i = 0; i < args.length; i++)
      {
        Object arg = args[i];
        if (arg == null)
        {
          String msg = "Message argument at index %s was null for message key '%s'.";
          msg = String.format(msg, i, code);
          MISSING_LOG.error(msg);
        }
      }
    }


    // wrapping args in another Object[] is required by the MessageFormatAdapter
    String message = internalMessageDelegate.getMessageInternal(code, new Object[] {args}, locale);

    if (message == null || message.isEmpty())
    {
      if (isLogWhenMissing() && missingMessages.add(code))
      {
        String msg = "At least one localization missing for message key '%s'.";
        msg = String.format(msg, code);
        MISSING_LOG.error(msg);
      }

      if (isFallbackToDefaultLocale() && !locale.equals(getDefaultLocale()))
      {
        // wrapping args in another Object[] is required by the MessageFormatAdapter
        message = internalMessageDelegate.getMessageInternal(code, new Object[] {args},
            getDefaultLocale());
      }
    }

    if (message == null || message.isEmpty())
    {
      message = defaultMessageDelegate.getDefaultMessage(code);
    }

    if (message != null && message.isEmpty())
    {
      message = null;
    }

    // intentionally empty?
    if (ZERO_WIDTH_SPACE.equals(message))
      message = "";

    return message;
  }

  public final MessageFormat createMessageFormat(String message, Locale locale)
  {
    ZoneId timeZone = requireNonNullElse(TimeZonePassThrough.getTimeZone(), getDefaultTimeZone());

    Format mf = messageFormatFactory.createFormat(message, locale, timeZone);

    return new MessageFormatAdapter()
    {

      @Override
      protected StringBuffer wrappedFormat(Object obj, StringBuffer result, FieldPosition pos)
      {
        Object[] args = (Object[]) obj;

        if (args != null && args.length == 1)
        {
          if (args[0] instanceof ResolvableMessage)
          {
            ResolvableMessage resolvableMessage = (ResolvableMessage) args[0];
            if (messageFormatFactory.hasNamedArgsSupport())
            {
              Map<String, Object> argsByName = getMessageArgConverterRegistry()
                  .convertValues(resolvableMessage.getArgsByName(true), getLocale(), timeZone);
              return mf.format(argsByName, result, pos);
            }
            else
            {
              Object[] argsByIndex = getMessageArgConverterRegistry()
                  .convertValues(resolvableMessage.getArgsByIndex(), getLocale(), timeZone)
                  .toArray();
              return mf.format(argsByIndex, result, pos);
            }
          }
          else if (args[0] instanceof Map)
          {
            if (messageFormatFactory.hasNamedArgsSupport())
            {
              @SuppressWarnings("unchecked")
              Map<String, Object> argsByName = getMessageArgConverterRegistry()
                  .convertValues((Map<String, Object>) args[0], getLocale(), timeZone);
              return mf.format(argsByName, result, pos);
            }
            else
            {
              throw new UnsupportedOperationException("Named message arguments not supported.");
            }
          }
        }

        if (args != null && args.length == 0)
          args = null;

        args = getMessageArgConverterRegistry().convertValues(args, getLocale(), timeZone);
        return mf.format(args, result, pos);
      }

    };
  }

  public String getDefaultMessage(String code)
  {
    String defaultMessage = null;
    if (isUseCodeWhenMissing())
    {
      if (isShortenCodeWhenMissing())
        code = shortenCode(code);

      if (getMissingMessagePattern() != null)
      {
        defaultMessage =
            getMissingMessagePattern().replace(MISSING_MESSAGE_PATTERN_CODE_PLACEHOLDER, code);
      }
      else
      {
        defaultMessage = code;
      }
    }
    return defaultMessage;
  }

  private String shortenCode(String code)
  {
    return StringUtils.substringAfterLast(code, ".");
  }


  @FunctionalInterface
  public interface GetMessageInternalDelegate
  {

    String getMessageInternal(String code, Object[] args, Locale locale);

  }

  @FunctionalInterface
  public interface GetDefaultMessageDelegate
  {

    String getDefaultMessage(String code);

  }

}
