package de.codecamp.messages.spring.autoconfigure;


import static java.util.stream.Collectors.joining;

import de.codecamp.messages.runtime.DefaultMessageArgConverter;
import de.codecamp.messages.runtime.DefaultMessageFormatFactory;
import de.codecamp.messages.runtime.IcuJavaMoneyMessageArgConverter;
import de.codecamp.messages.runtime.IcuMessageArgConverter;
import de.codecamp.messages.runtime.IcuMessageFormatFactory;
import de.codecamp.messages.runtime.MessageArgConverter;
import de.codecamp.messages.runtime.MessageFormatFactory;
import de.codecamp.messages.spring.DefaultMessageAccessor;
import de.codecamp.messages.spring.ExtendedMessageSource;
import de.codecamp.messages.spring.ExtendedResourceBundleMessageSource;
import de.codecamp.messages.spring.MessageArgConverterRegistry;
import de.codecamp.messages.spring.impl.MessageArgConverterRegistryImpl;
import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.URL;
import java.time.Duration;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Stream;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.LocaleUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.context.MessageSourceAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.core.io.support.ResourcePatternResolver;


@AutoConfiguration(before = MessageSourceAutoConfiguration.class)
@EnableConfigurationProperties(ExtendedMessageSourceProperties.class)
public class MessagesAutoConfiguration
{

  private static final Log LOG = LogFactory.getLog(MessagesAutoConfiguration.class);


  private final ApplicationContext applicationContext;

  private final ExtendedMessageSourceProperties properties;


  public MessagesAutoConfiguration(ApplicationContext applicationContext,
      ExtendedMessageSourceProperties properties)
  {
    this.applicationContext = applicationContext;
    this.properties = properties;
  }


  //  @Bean
  //  static BeanPostProcessor messageProxyInjectingBeanPostProcessor()
  //  {
  //    return new MessageProxyInjectingBeanPostProcessor();
  //  }


  /**
   * A bean post processor that retrieves the determined default locale from an
   * {@link ExtendedMessageSource} and sets it as the JVM's default locale, in an attempt to provide
   * a consistent environment. Not everyone will check the ExtendedMessageSource for a default.
   *
   * @return the bean post processor
   */
  @Bean
  @ConditionalOnProperty(
      prefix = ExtendedMessageSourceProperties.PREFIX,
      name = "normalize-default-locales")
  static BeanPostProcessor staticDefaultLocaleInitializer()
  {
    return new BeanPostProcessor()
    {

      @Override
      public Object postProcessAfterInitialization(Object bean, String beanName)
        throws BeansException
      {
        if (bean instanceof ExtendedMessageSource)
        {
          Locale defaultLocale = ((ExtendedMessageSource) bean).getDefaultLocale();

          Locale.setDefault(defaultLocale);
        }
        return bean;
      }

    };
  }


  @Bean(name = AbstractApplicationContext.MESSAGE_SOURCE_BEAN_NAME)
  ExtendedMessageSource messageSource(MessageFormatFactory messageFormatFactory,
      MessageArgConverterRegistry messageArgConverterRegistry)
  {
    // basenames and locales may appear only once, but the order is important
    Set<String> bundleBasenames = new LinkedHashSet<>();
    Set<Locale> bundleLocales = new LinkedHashSet<>();

    List<String> patterns = new ArrayList<>();


    if (ArrayUtils.isNotEmpty(properties.getAvailableLocales()))
    {
      LOG.debug("Available message bundle locales explicitly configured. Autodiscovery disabled.");

      bundleLocales.addAll(Arrays.asList(properties.getAvailableLocales()));
    }

    if (ArrayUtils.isNotEmpty(properties.getBasenames()))
    {
      bundleBasenames.addAll(Arrays.asList(properties.getBasenames()));

      if (bundleLocales.isEmpty())
      {
        // autodiscovery of the locales
        Stream.of(bundleBasenames).map(basename -> "classpath*:" + basename + "_*.properties")
            .forEach(patterns::add);
      }
    }

    if (ArrayUtils.isNotEmpty(properties.getBundleFilePatterns()))
    {
      patterns.addAll(Arrays.asList(properties.getBundleFilePatterns()));
    }

    if (!patterns.isEmpty())
    {
      Set<String> bundleNamesTemp = new LinkedHashSet<>();
      Set<Locale> bundleLocalesTemp = new LinkedHashSet<>();

      for (String pattern : patterns)
      {
        discoverBundlesAndLocales(applicationContext, pattern, bundleNamesTemp, bundleLocalesTemp);
      }

      if (bundleNamesTemp.isEmpty())
      {
        if (bundleBasenames.isEmpty())
          LOG.warn("No message bundle basenames discovered or configured.");
      }
      else
      {
        bundleBasenames.addAll(bundleNamesTemp);
      }

      if (bundleLocalesTemp.isEmpty())
      {
        if (bundleLocales.isEmpty())
          LOG.warn("No message bundle locales discovered or configured.");
      }
      else
      {
        bundleLocales = bundleLocalesTemp;
      }
    }

    {
      String msg = "Available message bundles: %s";
      msg = String.format(msg, StringUtils.join(bundleBasenames, ", "));
      LOG.info(msg);

      msg = "Available message bundle locales: %s";
      msg = String.format(msg, StringUtils.join(bundleLocales, ", "));
      LOG.info(msg);
    }


    ExtendedResourceBundleMessageSource messageSource = new ExtendedResourceBundleMessageSource();

    messageSource.setMessageFormatFactory(messageFormatFactory);
    messageSource.setMessageArgConverterRegistry(messageArgConverterRegistry);

    messageSource.setBasenames(bundleBasenames.toArray(new String[bundleBasenames.size()]));
    messageSource.setAvailableLocales(bundleLocales);


    Locale defaultLocale = properties.getDefaultLocale();
    if (defaultLocale == null)
    {
      for (Locale locale : LocaleUtils.localeLookupList(Locale.getDefault()))
      {
        if (bundleLocales.contains(locale))
        {
          defaultLocale = locale;
          break;
        }
      }

      if (defaultLocale == null)
      {
        String msg = "No explicit application default locale configured and the system default"
            + " locale '%s' was not found among the discovered or configured locales: %s";
        msg = String.format(msg, Locale.getDefault(),
            bundleLocales.stream().map(Locale::toString).collect(joining(", ")));
        throw new IllegalStateException(msg);
      }

      LOG.debug("No explicit application default locale configured. Using Locale.getDefault().");
    }

    if (Collections.disjoint(bundleLocales, LocaleUtils.localeLookupList(defaultLocale)))
    {
      String msg = "The message bundle default locale '%s' was not found among the discovered or"
          + " configured locales: %s";
      msg = String.format(msg, defaultLocale,
          bundleLocales.stream().map(Locale::toString).collect(joining(", ")));
      throw new IllegalStateException(msg);
    }

    LOG.info("Application default locale: %s".formatted(defaultLocale));
    messageSource.setDefaultLocale(defaultLocale);


    ZoneId defaultTimeZone = properties.getDefaultTimeZone();
    if (defaultTimeZone == null)
      defaultTimeZone = ZoneId.systemDefault();

    LOG.info("Application default time zone: %s".formatted(defaultTimeZone));
    messageSource.setDefaultTimeZone(defaultTimeZone);


    if (properties.getEncoding() != null)
    {
      messageSource.setDefaultEncoding(properties.getEncoding().name());
    }
    Duration cacheDuration = properties.getCacheDuration();
    if (cacheDuration != null)
    {
      messageSource.setCacheMillis(cacheDuration.toMillis());
    }
    messageSource.setFallbackToDefaultLocale(properties.isFallbackToDefaultLocale());
    messageSource.setLogWhenMissing(properties.isLogWhenMissing());
    messageSource.setUseCodeAsDefaultMessage(properties.isUseCodeWhenMissing());
    messageSource.setShortenCodeWhenMissing(properties.isShortenCodeWhenMissing());
    messageSource.setMissingMessagePattern(properties.getMissingMessagePattern());
    messageSource.setAlwaysUseMessageFormat(properties.isAlwaysUseMessageFormat());

    return messageSource;
  }

  private static void discoverBundlesAndLocales(ResourcePatternResolver patternResolver,
      String pattern, Set<String> bundleNames, Set<Locale> bundleLocales)
  {
    try
    {
      Resource[] resources = patternResolver.getResources(pattern);
      if (resources.length == 0)
        LOG.warn("No message bundles found for pattern: {}" + pattern);

      for (Resource resource : resources)
      {
        String bundlePath;

        if (resource instanceof FileSystemResource)
        {
          File file = resource.getFile();
          bundlePath = StringUtils.substringAfterLast(file.getAbsolutePath(), "classes\\");
          String msg = "Found message bundle file %s -> %s.";
          msg = String.format(msg, file.toString(), bundlePath);
          LOG.debug(msg);
        }
        else if (resource instanceof UrlResource)
        {
          URL url = resource.getURL();
          bundlePath = StringUtils.substringAfterLast(url.toString(), "!/");
          String msg = "Found message bundle URL %s -> %s.";
          msg = String.format(msg, url.toString(), bundlePath);
          LOG.debug(msg);
        }
        else
        {
          String msg = "Resource type %s not yet supported for message bundle basename discovery.";
          msg = String.format(msg, resource.getClass().getName());
          LOG.warn(msg);
          bundlePath = null;
        }

        if (bundlePath != null)
        {
          if (!bundlePath.endsWith(".properties"))
            continue;

          String bundlePathNoExt = StringUtils.removeEnd(bundlePath, ".properties");

          if (bundleNames != null)
          {
            String bundleName = StringUtils.substringBefore(bundlePathNoExt, "_");
            bundleNames.add(bundleName);
          }

          if (bundleLocales != null)
          {
            String localeString = StringUtils.substringAfter(bundlePathNoExt, "_");
            if (!localeString.isEmpty())
              bundleLocales.add(LocaleUtils.toLocale(localeString));
          }
        }
      }
    }
    catch (IOException ex)
    {
      throw new UncheckedIOException("Failed to discover message bundle basenames.", ex);
    }
  }

  @Bean
  MessageArgConverterRegistry messageArgConverterRegistry(List<MessageArgConverter> converters)
  {
    return new MessageArgConverterRegistryImpl(converters);
  }


  @Bean
  @Primary
  DefaultMessageAccessor messageAccessor(ExtendedMessageSource messageSource)
  {
    return new DefaultMessageAccessor(messageSource);
  }


  @Configuration
  @ConditionalOnMissingClass("com.ibm.icu.text.MessageFormat")
  @ConditionalOnMissingBean(MessageFormatFactory.class)
  static class DefaultMessageFormatConfiguration
  {

    @Bean
    @ConditionalOnMissingBean
    MessageFormatFactory defaultMessageFormatFactory()
    {
      LOG.info(String.format("Using regular %s.", java.text.MessageFormat.class.getName()));
      return new DefaultMessageFormatFactory();
    }

    @Bean
    MessageArgConverter defaultJavaTimeMessageArgConverter()
    {
      return new DefaultMessageArgConverter();
    }

  }

  @Configuration
  @ConditionalOnClass(com.ibm.icu.text.MessageFormat.class)
  @ConditionalOnMissingBean(MessageFormatFactory.class)
  static class IcuMessageFormatConfiguration
  {

    @Bean
    MessageFormatFactory icuMessageFormatFactory()
    {
      LOG.info(String.format("ICU4J detected. Using %s.",
          com.ibm.icu.text.MessageFormat.class.getName()));
      return new IcuMessageFormatFactory();
    }

    @Bean
    MessageArgConverter icuJavaTimeMessageArgConverter()
    {
      return new IcuMessageArgConverter();
    }

    @Bean
    @ConditionalOnClass(name = {"javax.money.MonetaryAmount"})
    MessageArgConverter icuJavaMoneyMessageArgConverter()
    {
      return new IcuJavaMoneyMessageArgConverter();
    }

  }

}
