package de.codecamp.messages;


/**
 * A message argument.
 */
public interface MessageArg
{

  /**
   * Returns the name of the message argument.
   *
   * @return the name of the message argument
   */
  String getName();

  /**
   * Returns the index of the message argument.
   *
   * @return the index of the message argument
   */
  int getIndex();

  /**
   * Returns the type of the message argument.
   *
   * @return the type of the message argument
   */
  String getType();

}
