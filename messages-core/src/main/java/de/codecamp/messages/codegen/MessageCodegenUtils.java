package de.codecamp.messages.codegen;


public final class MessageCodegenUtils
{

  /** prefix for message-related generated types */
  private static final String MESSAGES_PREFIX = "M";

  /** suffix for the interface containing the message key constants */
  private static final String MESSAGE_CONSTANTS_SUFFIX = "_";

  private static final String NESTED_TYPE_SEPARATOR = "_";


  private MessageCodegenUtils()
  {
    // utility class
  }


  public static String getMessageConstantsSimpleTypeNameFor(String simpleSourceTypeNames)
  {
    return MESSAGES_PREFIX + simpleSourceTypeNames.replace(".", NESTED_TYPE_SEPARATOR)
        + MESSAGE_CONSTANTS_SUFFIX;
  }

  public static String getMessageProxiesSimpleTypeNameFor(String simpleSourceTypeNames)
  {
    return MESSAGES_PREFIX + simpleSourceTypeNames.replace(".", NESTED_TYPE_SEPARATOR);
  }

}
