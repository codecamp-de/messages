package de.codecamp.messages.codegen;


import de.codecamp.messages.MessageKey;
import java.util.Objects;


/**
 * Basic implementation of {@link MessageKey} with a few extensions. Used for the message key
 * constants.
 */
public class DeclaredMessageKey
  implements
    MessageKey
{

  private static final Class<?>[] EMPTY_ARG_TYPES = {};

  private static final String[] EMPTY_ARG_NAMES = {};


  private final Class<?> sourceType;

  private final String code;

  private final Class<?>[] argTypes;

  private final String[] argNames;


  /**
   * Constructs a new {@link DeclaredMessageKey}.
   *
   * @param sourceType
   *          the type the message key was declared in
   * @param code
   *          the message key
   * @param argTypes
   *          the message argument types
   * @param argNames
   *          the message argument names
   */
  public DeclaredMessageKey(Class<?> sourceType, String code, Class<?>[] argTypes,
      String[] argNames)
  {
    if (sourceType == null)
      throw new IllegalArgumentException("sourceType must not be null");
    if (code == null)
      throw new IllegalArgumentException("code must not be null");

    this.sourceType = sourceType;
    this.code = code;
    this.argTypes = argTypes == null ? null : argTypes.clone();
    this.argNames = argTypes == null ? null : argNames.clone();
  }


  /**
   * Returns the top-level type the message key was declared in.
   *
   * @return the top-level type the message key was declared in
   */
  public Class<?> getSourceType()
  {
    return sourceType;
  }

  @Override
  public String getCode()
  {
    return code;
  }

  public boolean hasArgs()
  {
    return argTypes != null && argTypes.length > 0;
  }

  public Class<?>[] getArgTypes()
  {
    if (hasArgs())
      return argTypes.clone();
    else
      return EMPTY_ARG_TYPES;
  }

  public String[] getArgNames()
  {
    if (hasArgs())
      return argNames.clone();
    else
      return EMPTY_ARG_NAMES;
  }


  @Override
  public int hashCode()
  {
    return getCode().hashCode();
  }

  @Override
  public boolean equals(Object obj)
  {
    if (obj == null)
      return false;
    if (obj == this)
      return true;

    if (!(obj instanceof MessageKey))
      return false;

    MessageKey other = (MessageKey) obj;

    return Objects.equals(getCode(), other.getCode());
  }

}
