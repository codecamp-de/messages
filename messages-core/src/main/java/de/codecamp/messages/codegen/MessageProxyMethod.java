package de.codecamp.messages.codegen;


import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;


/**
 * Used to mark the methods of generated message proxy interfaces.
 */
@Target(METHOD)
@Retention(RUNTIME)
@Documented
public @interface MessageProxyMethod
{

  /**
   * Returns the code of the message key.
   *
   * @return the code of the message key
   */
  String code();

}
