package de.codecamp.messages.codegen;


import static java.lang.annotation.RetentionPolicy.RUNTIME;

import de.codecamp.messages.proxy.MessageProvider;
import de.codecamp.messages.proxy.MessageProxyUtils;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;


/**
 * Used to mark parameters of message proxy methods that have an message argument name associated
 * with it.
 *
 * @see MessageProxyUtils#createMessageProxy(Class, MessageProvider)
 */
@Target(ElementType.PARAMETER)
@Retention(RUNTIME)
@Documented
public @interface MessageProxyParam
{

  /**
   * Returns the message argument name.
   *
   * @return the message argument name
   */
  String name();

}
