package de.codecamp.messages.codegen;


import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import de.codecamp.messages.proxy.MessageProvider;
import de.codecamp.messages.proxy.MessageProxyUtils;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;


/**
 * Used to mark generated message proxy interfaces.
 *
 * @see MessageProxyUtils#createMessageProxy(Class, MessageProvider)
 */
@Target(TYPE)
@Retention(RUNTIME)
@Documented
public @interface MessageProxyInterface
{

  /**
   * Returns the type the message keys were declared in.
   *
   * @return the type the message keys were declared in
   */
  Class<?> sourceType() default Void.class;

}
