package de.codecamp.messages.codegen;


import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;


/**
 * Used to mark generated message key constants interfaces.
 */
@Target(TYPE)
@Retention(RUNTIME)
@Documented
public @interface MessageKeyConstants
{

  /**
   * Returns the type the message keys were declared in.
   *
   * @return the type the message keys were declared in
   */
  Class<?> value() default Void.class;

}
