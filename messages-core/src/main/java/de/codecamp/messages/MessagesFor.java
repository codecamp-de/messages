package de.codecamp.messages;


import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * This annotation is used to declare message keys for types that cannot be annotated directly. This
 * means {@code @MessagesFor(type=SomeType.class, ...)} is equivalent to annotating {@code SomeType}
 * directly using {@link Messages @Messages(...)}.
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Repeatable(MessagesFor.Container.class)
@Documented
public @interface MessagesFor
{

  /**
   * Returns the type that should be treated as the source type, i.e. if it were annotated with
   * {@link Messages} directly.
   *
   * @return the type that should be treated as the source type, i.e. if it were annotated with
   *         {@link Messages} directly
   */
  Class<?> type();

  /**
   * Returns whether a message key for the annotated type should be created. Only relevant when the
   * annotation is directly on the type.
   *
   * @return whether a message key for the annotated type should be created
   */
  boolean forType() default false;

  /**
   * Returns whether message keys for all bean properties of the annotated type should be created.
   * Only relevant on classes.
   *
   * @return whether message keys for all bean properties of the annotated type should be created
   */
  boolean forProperties() default false;

  /**
   * Returns a list of message keys that should be ignored for message key creation, like specfic
   * bean properties or enum elements.
   *
   * @return whether the annotated element should be ignored for the implicit generation of message
   *         keys
   */
  String[] ignoreImplicitKeys() default {};

  /**
   * The prefix to be used for message keys. This can only be set if this annotation is at the type
   * level. If not set, the fully qualified name of the source type is used.
   *
   * @return prefix to be used for message keys
   */
  String prefix() default Messages.PREFIX_SOURCE_TYPE;

  /**
   * (Additional) Message keys. The provided strings will be used as local parts in the message
   * keys. This is an alias for {@link #keys()}.
   *
   * @return message keys
   */
  String[] value() default {};

  /**
   * (Additional) Message keys. The provided strings will be used as local parts in the message
   * keys. This is an alias for {@link #value()}.
   *
   * @return message keys
   */
  String[] keys() default {};


  /**
   * Container for repeated {@link MessagesFor} annotations.
   */
  @Target({ElementType.TYPE, ElementType.METHOD})
  @Retention(RetentionPolicy.RUNTIME)
  @Documented
  @interface Container
  {

    /**
     * Returns the {@link MessagesFor} annotations.
     *
     * @return the {@link MessagesFor} annotations
     */
    MessagesFor[] value();

  }

}
