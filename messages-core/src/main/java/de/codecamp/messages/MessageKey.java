package de.codecamp.messages;


/**
 * A message key.
 */
public interface MessageKey
{

  /**
   * Suffix of message keys for types.
   */
  String TYPE_KEY_SUFFIX = "Type";


  /**
   * Returns the actual code of the message key.
   *
   * @return the code
   */
  String getCode();

}
