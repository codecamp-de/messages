package de.codecamp.messages.runtime;


import java.text.DateFormat;
import java.text.Format;
import java.text.MessageFormat;
import java.time.ZoneId;
import java.util.Locale;
import java.util.TimeZone;


public class DefaultMessageFormatFactory
  implements
    MessageFormatFactory
{

  @Override
  public boolean hasNamedArgsSupport()
  {
    return false;
  }

  @Override
  public Format createFormat(String message, Locale locale, ZoneId timeZone)
  {
    MessageFormat mf = new MessageFormat(message, locale);

    TimeZone julTimeZone = null;
    for (Format format : mf.getFormats())
    {
      if (format instanceof DateFormat df)
      {
        if (julTimeZone == null)
          julTimeZone = TimeZone.getTimeZone(timeZone);

        df.setTimeZone(julTimeZone);
      }
    }

    return mf;
  }

}
