package de.codecamp.messages.runtime;


import com.ibm.icu.util.CurrencyAmount;
import java.math.BigDecimal;
import java.time.ZoneId;
import java.util.Currency;
import java.util.Locale;
import javax.money.MonetaryAmount;


/**
 * Converts {@link MonetaryAmount} to {@link CurrencyAmount}.
 * <p>
 * If changes are made to the mapped types, this must be reflected in
 * {@code de.codecamp.messages.shared.messageformat.IcuMessageFormatSupport}.
 */
public class IcuJavaMoneyMessageArgConverter
  implements
    MessageArgConverter
{

  @Override
  public Object convert(Object value, Locale locale, ZoneId timeZone)
  {
    if (value instanceof MonetaryAmount)
    {
      MonetaryAmount monetaryAmount = (MonetaryAmount) value;
      BigDecimal amount = monetaryAmount.getNumber().numberValueExact(BigDecimal.class);
      Currency currency = Currency.getInstance(monetaryAmount.getCurrency().getCurrencyCode());
      value = new CurrencyAmount(amount, currency);
    }

    return value;
  }

}
