package de.codecamp.messages.runtime;


import java.text.MessageFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.OffsetTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


/**
 * Converts several java.time types to {@link Date}. {@link MessageFormat} unfortunately doesn't
 * understand {@link Calendar} or anything else with a time zone.
 * <p>
 * If changes are made to the mapped types, this must be reflected in
 * {@code de.codecamp.messages.shared.messageformat.DefaultMessageFormatSupport}.
 */
public class DefaultMessageArgConverter
  implements
    MessageArgConverter
{

  @Override
  public Object convert(Object value, Locale locale, ZoneId timeZone)
  {
    if (value instanceof LocalDate)
    {
      return Date.from(((LocalDate) value).atStartOfDay(timeZone).toInstant());
    }
    else if (value instanceof LocalTime)
    {
      return Date
          .from(((LocalTime) value).atDate(LocalDate.now(timeZone)).atZone(timeZone).toInstant());
    }
    else if (value instanceof LocalDateTime)
    {
      return Date.from(((LocalDateTime) value).atZone(timeZone).toInstant());
    }
    else if (value instanceof ZonedDateTime)
    {
      return Date.from(((ZonedDateTime) value).toInstant());
    }
    else if (value instanceof OffsetTime)
    {
      ZoneId zoneId = timeZone;
      return Date.from(
          ((OffsetTime) value).atDate(LocalDate.now(zoneId)).atZoneSameInstant(zoneId).toInstant());
    }
    else if (value instanceof OffsetDateTime)
    {
      return Date.from(((OffsetDateTime) value).atZoneSameInstant(timeZone).toInstant());
    }
    else if (value instanceof Instant)
    {
      return Date.from(((Instant) value).atZone(timeZone).toInstant());
    }

    return value;
  }

}
