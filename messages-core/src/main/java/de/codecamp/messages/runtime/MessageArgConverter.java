package de.codecamp.messages.runtime;


import java.time.ZoneId;
import java.util.Locale;


/**
 * Converts message arguments from a type that is used in the application to a type that can be
 * handled by the used message format.
 */
@FunctionalInterface
public interface MessageArgConverter
{

  /**
   * Converts message arguments from a type that is used in the application to a type that can be
   * handled by the used message format.
   *
   * @param value
   *          the original value
   * @param locale
   *          the locale used for conversion
   * @param timeZone
   *          the time zone used for conversion
   * @return the converted value
   */
  Object convert(Object value, Locale locale, ZoneId timeZone);

}
