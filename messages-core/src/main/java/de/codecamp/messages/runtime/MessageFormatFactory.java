package de.codecamp.messages.runtime;


import java.text.Format;
import java.time.ZoneId;
import java.util.Locale;


public interface MessageFormatFactory
{

  boolean hasNamedArgsSupport();

  Format createFormat(String message, Locale locale, ZoneId timeZone);

}
