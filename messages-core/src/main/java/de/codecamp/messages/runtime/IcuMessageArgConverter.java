package de.codecamp.messages.runtime;


import com.ibm.icu.util.Calendar;
import com.ibm.icu.util.TimeZone;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.OffsetTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Locale;


/**
 * Converts several java.time types and {@link java.util.Calendar} to {@link Calendar}.
 * <p>
 * If changes are made to the mapped types, this must be reflected in
 * {@code de.codecamp.messages.shared.messageformat.IcuMessageFormatSupport}.
 */
public class IcuMessageArgConverter
  implements
    MessageArgConverter
{

  @Override
  public Object convert(Object value, Locale locale, ZoneId timeZone)
  {
    ZonedDateTime zonedDateTime = null;
    if (value instanceof java.util.Calendar calendar)
    {
      zonedDateTime =
          ZonedDateTime.ofInstant(calendar.toInstant(), calendar.getTimeZone().toZoneId());
    }
    else if (value instanceof LocalDate localDate)
    {
      zonedDateTime = localDate.atStartOfDay(timeZone);
    }
    else if (value instanceof LocalTime localTime)
    {
      zonedDateTime = localTime.atDate(LocalDate.now(timeZone)).atZone(timeZone);
    }
    else if (value instanceof LocalDateTime localDateTime)
    {
      zonedDateTime = localDateTime.atZone(timeZone);
    }
    else if (value instanceof ZonedDateTime)
    {
      zonedDateTime = (ZonedDateTime) value;
    }
    else if (value instanceof OffsetTime offsetTime)
    {
      zonedDateTime = offsetTime.atDate(LocalDate.now(offsetTime.getOffset())).toZonedDateTime();
    }
    else if (value instanceof OffsetDateTime offsetDateTime)
    {
      zonedDateTime = offsetDateTime.toZonedDateTime();
    }
    else if (value instanceof Instant instant)
    {
      zonedDateTime = instant.atZone(timeZone);
    }

    if (zonedDateTime != null)
    {
      Calendar calendar =
          Calendar.getInstance(TimeZone.getTimeZone(zonedDateTime.getZone().getId()));
      calendar.setTimeInMillis(zonedDateTime.toInstant().toEpochMilli());
      return calendar;
    }

    return value;
  }

}
