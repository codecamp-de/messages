package de.codecamp.messages.runtime;


import com.ibm.icu.text.DateFormat;
import com.ibm.icu.text.MessageFormat;
import com.ibm.icu.util.TimeZone;
import java.text.Format;
import java.time.ZoneId;
import java.util.Locale;


public class IcuMessageFormatFactory
  implements
    MessageFormatFactory
{

  @Override
  public boolean hasNamedArgsSupport()
  {
    return true;
  }

  @Override
  public Format createFormat(String message, Locale locale, ZoneId timeZone)
  {
    MessageFormat mf = new MessageFormat(message, locale);

    /*
     * IcuMessageArgConverter already adds a time zone to each formatted date, so setting a time
     * zone on the DateFormat might actually be redundant.
     */
    TimeZone icuTimeZone = null;
    for (Format format : mf.getFormats())
    {
      if (format instanceof DateFormat df)
      {
        if (icuTimeZone == null)
        {
          java.util.TimeZone temp = java.util.TimeZone.getTimeZone(timeZone);
          icuTimeZone = TimeZone.getFrozenTimeZone(temp.getID());
        }

        df.setTimeZone(icuTimeZone);
      }
    }

    return mf;
  }

}
