package de.codecamp.messages.proxy;


import java.io.Serializable;


/**
 * Simple functional interface that takes a message key plus an array of message arguments and
 * returns the formatted message.
 */
@FunctionalInterface
public interface MessageProvider
  extends
    Serializable
{

  /**
   * Resolves the given message key and formats it using the given message arguments.
   *
   * @param code
   *          the message key
   * @param args
   *          the message arguments
   * @return the formatted message
   */
  String getMessage(String code, Object... args);

}
