package de.codecamp.messages.proxy;


import de.codecamp.messages.codegen.MessageProxyInterface;
import de.codecamp.messages.codegen.MessageProxyMethod;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;


/**
 * An {@link InvocationHandler} for {@link MessageProxyInterface} interfaces that uses a
 * {@link MessageProvider}.
 *
 * @see MessageProxyUtils#createMessageProxy(Class, MessageProvider)
 */
public class MessageProxyInvocationHandler
  extends
    AbstractMessageProxyInvocationHandler
{

  private final MessageProvider messageProvider;


  /**
   * Constructs a new {@link MessageProxyInvocationHandler}.
   *
   * @param messageProvider
   *          the message provider used to resolve and format message keys
   */
  public MessageProxyInvocationHandler(MessageProvider messageProvider)
  {
    if (messageProvider == null)
      throw new IllegalArgumentException("messageProvider must not be null");

    this.messageProvider = messageProvider;
  }


  @Override
  public Object invoke(Object proxy, Method method, Object[] args)
    throws Throwable
  {
    MessageProxyMethod messageProxyMethodAt = method.getAnnotation(MessageProxyMethod.class);
    if (messageProxyMethodAt != null)
    {
      return messageProvider.getMessage(messageProxyMethodAt.code(), args);
    }

    else
    {
      return super.invoke(proxy, method, args);
    }
  }

}
