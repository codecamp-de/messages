package de.codecamp.messages.proxy;


import de.codecamp.messages.ResolvableMessage;
import java.io.Serializable;


/**
 * Simple functional interface that takes a {@link ResolvableMessage} and returns the formatted
 * message.
 */
@FunctionalInterface
public interface NamedArgsMessageProvider
  extends
    Serializable
{

  /**
   * Resolves the given {@link ResolvableMessage}.
   *
   * @param resolvableMessage
   *          the resolvable message
   * @return the formatted message
   */
  String getMessage(ResolvableMessage resolvableMessage);

}
