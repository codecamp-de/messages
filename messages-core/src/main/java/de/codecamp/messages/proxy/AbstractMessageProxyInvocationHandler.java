package de.codecamp.messages.proxy;


import de.codecamp.messages.codegen.MessageProxyInterface;
import java.io.Serializable;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;


/**
 * An {@link InvocationHandler} for {@link MessageProxyInterface message proxy interfaces}.
 *
 * @see MessageProxyUtils#createMessageProxy(Class, MessageProvider)
 */
public class AbstractMessageProxyInvocationHandler
  implements
    InvocationHandler,
    Serializable
{

  @Override
  public Object invoke(Object proxy, Method method, Object[] args)
    throws Throwable
  {
    if (method.getName().equals("hashCode") && method.getParameterCount() == 0)
    {
      return System.identityHashCode(proxy);
    }
    else if (method.getName().equals("toString") && method.getParameterCount() == 0)
    {
      return proxy.getClass().getName() + "@" + Integer.toHexString(System.identityHashCode(proxy));
    }
    else if (method.getName().equals("equals") && method.getParameterCount() == 1
        && method.getParameterTypes()[0] == Object.class)
    {
      return proxy == args[0]; // NOPMD:CompareObjectsWithEquals comparing the proxy with equals would trigger endless recursion
    }
    else if (method.isDefault())
    {
      return method.invoke(proxy, args);
    }
    else
    {
      String msg = "Execution of method '%s' not supported on MessageProxy interface.";
      msg = String.format(msg, method.toString());
      throw new UnsupportedOperationException(msg);
    }
  }

}
