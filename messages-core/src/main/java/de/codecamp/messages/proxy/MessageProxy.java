package de.codecamp.messages.proxy;


/**
 * A marker interface extended by all generated message proxy interfaces.
 */
public interface MessageProxy
{
  // marker interface
}
