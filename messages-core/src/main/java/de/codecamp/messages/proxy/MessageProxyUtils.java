package de.codecamp.messages.proxy;


import java.lang.reflect.Proxy;


public final class MessageProxyUtils
{

  private MessageProxyUtils()
  {
  }


  public static <T extends MessageProxy> T createMessageProxy(Class<T> messageProxyIface,
      MessageProvider messageProvider)
  {
    if (messageProxyIface == null)
      throw new IllegalArgumentException("messageProxyIface must not be null");

    MessageProxyInvocationHandler invocationHandler =
        new MessageProxyInvocationHandler(messageProvider);

    return messageProxyIface.cast(Proxy.newProxyInstance(messageProxyIface.getClassLoader(),
        new Class<?>[] {messageProxyIface}, invocationHandler));
  }

  public static <T extends MessageProxy> T createNamedArgsMessageProxy(Class<T> messageProxyIface,
      NamedArgsMessageProvider messageProvider)
  {
    if (messageProxyIface == null)
      throw new IllegalArgumentException("messageProxyIface must not be null");

    NamedArgsMessageProxyInvocationHandler invocationHandler =
        new NamedArgsMessageProxyInvocationHandler(messageProvider);

    return messageProxyIface.cast(Proxy.newProxyInstance(messageProxyIface.getClassLoader(),
        new Class<?>[] {messageProxyIface}, invocationHandler));
  }

}
