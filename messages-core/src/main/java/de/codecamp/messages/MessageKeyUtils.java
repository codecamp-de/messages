package de.codecamp.messages;


import java.lang.annotation.Annotation;
import java.util.Objects;


public final class MessageKeyUtils
{

  private MessageKeyUtils()
  {
    // utility class
  }


  /**
   * Returns the message key for the given type.
   *
   * @param type
   *          the type
   * @return the message key
   */
  public static String getKeyFor(Class<?> type)
  {
    return getKeyFor(getPrefixFor(type), MessageKey.TYPE_KEY_SUFFIX);
  }

  /**
   * Returns the message key for the given enum constant.
   *
   * @param enumConstant
   *          the enum constant
   * @return the message key
   */
  public static String getKeyFor(Enum<?> enumConstant)
  {
    return getKeyFor(enumConstant.getDeclaringClass(), enumConstant.name());
  }

  /**
   * Returns the message key for the given type and local part.
   *
   * @param type
   *          the type containing the message key
   * @param localPart
   *          the local part of the message key
   * @return the message key
   */
  public static String getKeyFor(Class<?> type, String localPart)
  {
    return getKeyFor(getPrefixFor(type), localPart);
  }

  /**
   * Returns the message key for the given type and local part.
   *
   * @param type
   *          the type containing the message key
   * @param localPartPrefix
   *          the local part prefix of the message key
   * @param localPart
   *          the local part of the message key
   * @return the message key
   */
  public static String getKeyFor(Class<?> type, String localPartPrefix, String localPart)
  {
    return getKeyFor(getPrefixFor(type), localPartPrefix, localPart);
  }

  /**
   * Returns the message key for the given prefix and local part.
   *
   * @param prefix
   *          the prefix of the message key (e.g. the fully qualified name of the type containing
   *          the message key)
   * @param localPart
   *          the local part of the message key
   * @return the message key
   */
  public static String getKeyFor(String prefix, String localPart)
  {
    Objects.requireNonNull("prefix must not be null");

    if (localPart != null && !localPart.isEmpty())
    {
      return prefix + localPart;
    }
    else
    {
      return prefix;
    }
  }

  /**
   * Returns the message key for the given prefix and local part.
   *
   * @param prefix
   *          the prefix of the message key (e.g. the fully qualified name of the type containing
   *          the message key)
   * @param localPartPrefix
   *          the local part prefix of the message key
   * @param localPart
   *          the local part of the message key
   * @return the message key
   */
  public static String getKeyFor(String prefix, String localPartPrefix, String localPart)
  {
    String fullLocalPart;
    if (localPartPrefix != null && !localPartPrefix.isEmpty())
    {
      if (localPart != null && !localPart.isEmpty())
        fullLocalPart = localPartPrefix + "_" + localPart;
      else
        fullLocalPart = localPartPrefix;
    }
    else
    {
      if (localPart != null && !localPart.isEmpty())
        fullLocalPart = localPart;
      else
        fullLocalPart = null;
    }

    return getKeyFor(prefix, fullLocalPart);
  }

  private static String getPrefixFor(Class<?> sourceType)
  {
    String prefix = null;

    // use the first prefix that isn't the default
    Messages messagesAt = sourceType.getAnnotation(Messages.class);
    if (messagesAt != null)
      prefix = messagesAt.prefix();

    if (prefix == null || prefix.equals(Messages.PREFIX_SOURCE_TYPE))
    {
      for (Annotation at : sourceType.getAnnotations())
      {
        messagesAt = at.annotationType().getAnnotation(Messages.class);
        if (messagesAt != null)
        {
          prefix = messagesAt.prefix();
          if (!prefix.equals(Messages.PREFIX_SOURCE_TYPE))
            break;
        }
      }
    }

    if (prefix == null || prefix.equals(Messages.PREFIX_SOURCE_TYPE))
      prefix = sourceType.getCanonicalName() + ".";

    return prefix;
  }

}
