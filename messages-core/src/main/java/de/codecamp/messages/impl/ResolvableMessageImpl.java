package de.codecamp.messages.impl;


import de.codecamp.messages.ResolvableMessage;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Default implementation of {@link ResolvableMessage}.
 */
public class ResolvableMessageImpl
  implements
    ResolvableMessage
{

  private final String code;

  private final Map<String, Object> argsByName;

  private final List<Object> argsByIndex;


  public ResolvableMessageImpl(String code)
  {
    this.code = code;
    this.argsByName = Collections.emptyMap();
    this.argsByIndex = Collections.emptyList();
  }

  public ResolvableMessageImpl(String code, Map<String, Object> argsByName,
      List<Object> argsByIndex)
  {
    this.code = code;
    this.argsByName = Collections.unmodifiableMap(argsByName);
    this.argsByIndex = Collections.unmodifiableList(argsByIndex);
  }


  @Override
  public String getCode()
  {
    return code;
  }

  @Override
  public Map<String, Object> getArgsByName()
  {
    return getArgsByName(false);
  }

  @Override
  public Map<String, Object> getArgsByName(boolean includeByIndex)
  {
    if (!includeByIndex)
    {
      if (argsByName.size() != argsByIndex.size())
        throw new IllegalStateException("Names aren't available for all message arguments.");

      return argsByName;
    }

    Map<String, Object> result = new HashMap<>(argsByName);
    for (int i = 0; i < argsByIndex.size(); i++)
      result.put(Integer.toString(i), argsByIndex.get(i));
    return result;
  }

  @Override
  public List<Object> getArgsByIndex()
  {
    return argsByIndex;
  }

}
