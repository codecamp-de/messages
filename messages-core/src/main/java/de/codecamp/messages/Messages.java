package de.codecamp.messages;


import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * This annotation is used to declare message keys on types, fields or methods and can also be used
 * as a meta-annotation.
 */
@Target({ElementType.TYPE, ElementType.FIELD, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Messages
{

  String PREFIX_SOURCE_TYPE = "__SOURCE_TYPE__";


  /**
   * Returns whether a message key for the annotated type should be created. Only considered when
   * the annotation is directly on the type.
   *
   * @return whether a message key for the annotated type should be created
   */
  boolean forType() default false;

  /**
   * Returns whether message keys for all bean properties of the annotated type should be created.
   * Only considered when the annotation is directly on the type.
   *
   * @return whether message keys for all bean properties of the annotated type should be created
   */
  boolean forProperties() default false;

  /**
   * Returns whether the annotated element should be ignored for the implicit generation of message
   * keys. E.g. properties (represented by the associated getter) and enum elements annotated with
   * {@code ignoreImplicit = true} will be ignored. However, explicitly provided message keys on
   * that element will still be processed!
   *
   * @return whether the annotated element should be ignored for the implicit generation of message
   *         keys
   */
  boolean ignoreImplicit() default false;


  /**
   * The prefix to be used for message keys. The default is to use the fully qualified name of the
   * source type followed by a period (.). The prefix should usually not be changed, but sometimes
   * it may be necessary to declare very specific message keys. The prefix can only be set at the
   * type level.
   *
   * @return prefix to be used for message keys
   */
  String prefix() default PREFIX_SOURCE_TYPE;

  /**
   * (Additional) Message keys. The provided strings will be used as local parts in the full message
   * keys. This is an alias for {@link #keys()}.
   *
   * @return message keys
   */
  String[] value() default {};

  /**
   * (Additional) Message keys. The provided strings will be used as local parts in the full message
   * keys. This is an alias for {@link #value()}.
   *
   * @return message keys
   */
  String[] keys() default {};

}
