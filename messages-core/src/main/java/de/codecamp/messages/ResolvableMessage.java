package de.codecamp.messages;


import de.codecamp.messages.impl.ResolvableMessageImpl;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Contains a message key code and the message arguments as named and indexed values to resolve it.
 * But even named arguments have an implicit order (and therefore index) based on the order they are
 * declared in for the message key. Use {@link #forCode(String)} to create new instances.
 */
public interface ResolvableMessage
  extends
    MessageKey,
    Serializable
{

  Map<String, Object> getArgsByName();

  Map<String, Object> getArgsByName(boolean includeByIndex);

  List<Object> getArgsByIndex();


  static ResolvableMessage of(String code)
  {
    return new ResolvableMessageImpl(code);
  }

  static ResolvableMessage of(String code, Map<String, Object> argsByName, List<Object> argsByIndex)
  {
    return new ResolvableMessageImpl(code, argsByName, argsByIndex);
  }

  static Builder forCode(String code)
  {
    return new Builder(code);
  }

  static ResolvableMessage forCodeNoArgs(String code)
  {
    return new ResolvableMessageImpl(code);
  }


  class Builder
  {

    private final String code;

    private Map<String, Object> argsByName;

    private List<Object> argsByIndex;


    public Builder(String code)
    {
      this.code = code;
    }


    public Builder arg(String name, Object value)
    {
      if (argsByName == null)
      {
        argsByName = new HashMap<>();
        argsByIndex = new ArrayList<>();
      }

      if (name != null)
        argsByName.put(name, value);
      argsByIndex.add(value);
      return this;
    }


    public ResolvableMessage build()
    {
      return new ResolvableMessageImpl(code,
          argsByName == null || argsByName.isEmpty() ? Collections.emptyMap()
              : new HashMap<>(argsByName),
          argsByIndex == null || argsByIndex.isEmpty() ? Collections.emptyList()
              : new ArrayList<>(argsByIndex));
    }

  }

}
