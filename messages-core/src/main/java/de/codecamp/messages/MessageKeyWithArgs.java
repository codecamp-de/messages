package de.codecamp.messages;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * A message key with information about its arguments.
 */
public interface MessageKeyWithArgs
  extends
    MessageKey
{

  /**
   * Returns whether the message key has arguments.
   *
   * @return whether the message key has arguments
   */
  boolean hasArgs();

  /**
   * Returns the types of the message arguments in their declared order.
   *
   * @return the types of the message arguments in their declared order
   */
  String[] getArgTypes();

  /**
   * Returns the names of the message arguments in their declared order. The names may be null if
   * none were declared.
   *
   * @return the names of the message arguments in their declared order
   */
  String[] getArgNames();

  /**
   * Returns the list of message arguments.
   *
   * @return the list of message arguments
   */
  default List<MessageArg> getArgs()
  {
    if (!hasArgs())
      return Collections.emptyList();

    String[] argNames = getArgNames();
    String[] argTypes = getArgTypes();

    List<MessageArg> args = new ArrayList<>(argNames.length);
    for (int i = 0; i < argNames.length; i++)
    {
      int index = i;
      args.add(new MessageArg()
      {

        @Override
        public String getName()
        {
          return argNames[index];
        }

        @Override
        public int getIndex()
        {
          return index;
        }

        @Override
        public String getType()
        {
          return argTypes[index];
        }

      });
    }
    return args;
  }

}
