package de.codecamp.messages;


import static org.assertj.core.api.Assertions.assertThat;

import java.util.concurrent.TimeUnit;
import org.junit.jupiter.api.Test;


class MessageKeyUtilsTests
{

  @Test
  void getKeyFor()
  {
    assertThat(MessageKeyUtils.getKeyFor(Object.class))
        .isEqualTo("java.lang.Object." + MessageKey.TYPE_KEY_SUFFIX);

    assertThat(MessageKeyUtils.getKeyFor(TimeUnit.SECONDS))
        .isEqualTo("java.util.concurrent.TimeUnit.SECONDS");

    assertThat(MessageKeyUtils.getKeyFor(Object.class, "localPart"))
        .isEqualTo("java.lang.Object.localPart");
    assertThat(MessageKeyUtils.getKeyFor(Object.class, "localPartPrefix", "localPart"))
        .isEqualTo("java.lang.Object.localPartPrefix_localPart");
  }

  @Test
  void getKeyForWithCustomPrefix()
  {
    assertThat(MessageKeyUtils.getKeyFor(TypeWithPrefix.class))
        .isEqualTo("prefix." + MessageKey.TYPE_KEY_SUFFIX);

    assertThat(MessageKeyUtils.getKeyFor(TypeWithPrefix.ELEMENT)).isEqualTo("prefix.ELEMENT");

    assertThat(MessageKeyUtils.getKeyFor(TypeWithPrefix.class, "localPart"))
        .isEqualTo("prefix.localPart");
    assertThat(MessageKeyUtils.getKeyFor(TypeWithPrefix.class, "localPartPrefix", "localPart"))
        .isEqualTo("prefix.localPartPrefix_localPart");
  }


  @Messages(prefix = "prefix.")
  static enum TypeWithPrefix
  {
    ELEMENT
  }

}
