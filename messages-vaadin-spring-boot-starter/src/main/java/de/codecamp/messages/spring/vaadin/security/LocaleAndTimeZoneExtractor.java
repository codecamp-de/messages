package de.codecamp.messages.spring.vaadin.security;


import java.util.Locale;
import java.util.Optional;
import java.util.TimeZone;
import org.springframework.security.core.Authentication;


/**
 * Attempts to extract a {@link Locale} and {@link TimeZone} from a successful
 * {@link Authentication}. If provided, locale and time zone will then be stored in the Vaadin
 * session.
 */
public interface LocaleAndTimeZoneExtractor
{

  /**
   * Returns the user's locale if available in the {@link Authentication}.
   *
   * @param authentication
   *          the authentication to examine
   * @return the user's locale if available
   */
  Optional<Locale> getLocale(Authentication authentication);

  /**
   * Returns the user's time zone if available in the {@link Authentication}.
   *
   * @param authentication
   *          the authentication to examine
   * @return the user's time zone if available
   */
  Optional<TimeZone> getTimeZone(Authentication authentication);

}
