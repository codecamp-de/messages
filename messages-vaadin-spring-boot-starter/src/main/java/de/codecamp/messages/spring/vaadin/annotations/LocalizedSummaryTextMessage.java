package de.codecamp.messages.spring.vaadin.annotations;


import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import de.codecamp.messages.Messages;
import de.codecamp.messages.spring.vaadin.LocalizationUtils;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;


/**
 * Used to translate the {@value LocalizedSummaryText#PROPERTY} property of components using a newly
 * declared message key.
 *
 * @see LocalizationUtils#localizeComponents(com.vaadin.flow.component.Component)
 * @see LocalizedSummaryText
 */
@Target({FIELD})
@Retention(RUNTIME)
@LocalizedProperty
@Messages(LocalizedSummaryText.PROPERTY)
public @interface LocalizedSummaryTextMessage
{
  // no attributes
}
