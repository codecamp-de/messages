package de.codecamp.messages.spring.vaadin;


import com.vaadin.flow.component.Component;
import de.codecamp.messages.spring.MessageAccessor;


/**
 * A mix-in interface primarily for {@link Component Components} to provide easy access to localized
 * messages and related information in the form of a {@link MessageAccessor}.
 */
public interface HasLocalization
{

  /**
   * Returns access to localized messages and related information for the locale of the current UI.
   * <p>
   * <em>The returned accessor is not serializable, so no references to it should be stored in
   * Vaadin components, unless they're not required to be serializable.</em>
   *
   * @return access to localized messages
   */
  default MessageAccessor getL10n()
  {
    return LocalizationUtils.getL10n();
  }

}
