package de.codecamp.messages.spring.vaadin;


import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.page.Page;
import com.vaadin.flow.component.page.Page.ExtendedClientDetailsReceiver;
import com.vaadin.flow.server.ServiceInitEvent;
import com.vaadin.flow.server.VaadinServiceInitListener;
import com.vaadin.flow.server.VaadinSession;
import java.time.ZoneId;
import java.util.TimeZone;


/**
 * This {@link VaadinServiceInitListener} is responsible for the following:
 * <ul>
 * <li>It initializes the session's time zone (stored in a session custom attribute} by
 * {@link Page#retrieveExtendedClientDetails(ExtendedClientDetailsReceiver) detecting it from the
 * browser}. Due to the required second round trip to the client, the time zone might not be
 * available during the very first request of a new session. If the time zone is already set by that
 * point it will not be overridden here.</li>
 * </ul>
 */
public class VaadinSessionLocaleServiceInitListener
  implements
    VaadinServiceInitListener
{

  @Override
  public void serviceInit(ServiceInitEvent serviceInitEvent)
  {
    /* Detects the time zone from the client and stores it in the Vaadin session. */
    serviceInitEvent.getSource().addUIInitListener(uiInitEvent ->
    {
      UI ui = uiInitEvent.getUI();
      VaadinSession session = ui.getSession();
      if (session.getAttribute(ZoneId.class) == null)
      {
        ui.getPage().retrieveExtendedClientDetails(details ->
        {
          String timeZoneId = details.getTimeZoneId();
          if (timeZoneId != null)
          {
            TimeZone timeZone = TimeZone.getTimeZone(timeZoneId);
            SessionLocaleUtils.setTimeZone(session, timeZone);
          }
        });
      }
    });
  }

}
