package de.codecamp.messages.spring.vaadin.ui;


import com.vaadin.flow.component.Component;
import de.codecamp.messages.spring.vaadin.HasLocalization;
import de.codecamp.messages.spring.vaadin.LocalizationUtils;
import de.codecamp.vaadin.base.Composite;


/**
 * A convenient base class for {@link Composite Composites} that will be localized.
 */
public abstract class LocalizedComposite
  extends
    Composite
  implements
    HasLocalization
{

  /**
   * Localizes components stored in fields of this composite. Call this method once all translated
   * components have been created within {@link #createContent()} or after calling
   * {@link #initializeContent()}.
   *
   * @see LocalizationUtils#localizeComponents(Component)
   */
  protected void localizeComponents()
  {
    LocalizationUtils.localizeComponents(this);
  }

  /**
   * {@inheritDoc}
   * <p>
   * <em>This implementation only calls {@link #localizeComponents()}. So if you override it, make
   * sure to call {@code super.contentCreated();} or {@code localizeComponents();}.</em>
   *
   */
  @Override
  protected void contentCreated()
  {
    localizeComponents();
  }

}
