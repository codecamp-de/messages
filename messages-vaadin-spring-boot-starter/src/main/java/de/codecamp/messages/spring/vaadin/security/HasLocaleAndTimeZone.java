package de.codecamp.messages.spring.vaadin.security;


import java.util.Locale;
import java.util.Optional;
import java.util.TimeZone;
import org.springframework.security.core.Authentication;


/**
 * Can be implemented by {@link org.springframework.security.core.userdetails.UserDetails} (or
 * generally any {@link Authentication#getPrincipal() principal}) to provide a {@link Locale} and
 * {@link TimeZone} after a successful authentication. If provided, locale and time zone will then
 * be stored in the Vaadin session.
 */
public interface HasLocaleAndTimeZone
{

  /**
   * Returns the user's locale if available.
   *
   * @return the user's locale if available
   */
  Optional<Locale> getLocale();

  /**
   * Returns the user's time zone if available.
   *
   * @return the user's time zone if available
   */
  Optional<TimeZone> getTimeZone();

}
