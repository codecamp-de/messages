package de.codecamp.messages.spring.vaadin.annotations;


import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import de.codecamp.messages.spring.vaadin.LocalizationUtils;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;


/**
 * Used to translate the {@value #PROPERTY} property of components reusing an already declared
 * message key.
 *
 * @see LocalizationUtils#localizeComponents(com.vaadin.flow.component.Component)
 * @see LocalizedHelperTextMessage
 */
@Target({FIELD})
@Retention(RUNTIME)
@LocalizedProperty(property = LocalizedHelperText.PROPERTY)
public @interface LocalizedHelperText
{

  /**
   * The name of the property.
   */
  String PROPERTY = "helperText";


  /**
   * Returns the full message key code to localize the property.
   *
   * @return the full message key code to localize the property
   */
  String key();

}
