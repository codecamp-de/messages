package de.codecamp.messages.spring.vaadin.annotations;


import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasText;
import de.codecamp.messages.Messages;
import de.codecamp.messages.spring.vaadin.LocalizationUtils;
import java.lang.annotation.Documented;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;


/**
 * This annotation is used by {@link LocalizationUtils#localizeComponents(Component)} to to
 * automatically localize properties of {@link Component Components} - like
 * {@link HasText#setText(String)} or
 * {@link com.vaadin.flow.component.textfield.TextField#setLabel(String)} - stored in the fields of
 * a host object. It can be used on the fields directly or as meta-annotation on other annotations
 * and is repeatable.
 * <ul>
 * <li>When {@link #property()} is empty, the properties to be localized will be determined from the
 * local parts of the {@link Messages messages keys} declared at the same location - i.e. on the
 * same field directly or on the same annotation. In that case {@link #key()} also must be
 * empty.</li>
 * <li>When a {@link #property} is set, only the specified property will be localized - unless there
 * are multiple {@link LocalizedProperty}.<br>
 * <ul>
 * <li>If {@link #key()} is empty, then it's assumed that a {@link Messages message key} with the
 * property name as local part has been declared at the same location.</li>
 * <li>If {@link #key()} is set, then it's assumed to be a (full) message key that has been declared
 * somewhere else.</li>
 * </ul>
 * </li>
 * <li>When used as meta-annotation, the host annotation may also provide a {@link #property()} or
 * {@link #key()} attribute. If set they will override the respective value provided in the
 * {@link LocalizedProperty}.</li>
 * </ul>
 *
 * @see LocalizationUtils#localizeComponents(Component)
 */
@Target({ANNOTATION_TYPE, FIELD})
@Retention(RUNTIME)
@Repeatable(LocalizedProperty.Container.class)
@Documented
public @interface LocalizedProperty
{

  /**
   * The name of the property to be translated.
   * <p>
   * If empty/not specified, the properties to be localized will be determined from the local parts
   * of {@link Messages message keys} declared at the same location (on the same field directly or
   * on the same annotation). In that case {@link #key()} also must be empty.
   *
   * @return the name of the property to be translated
   */
  String property() default "";

  /**
   * The (full) message key code used to translate the property. This can be set to reuse message
   * keys that are already declared somewhere else. In that case {@link #property()} must also be
   * set.
   *
   * @return the (full) message key code used to translate the property
   */
  String key() default "";


  /**
   * Container for repeated {@link LocalizedProperty @LocalizedProperty} annotations.
   */
  @Target({ANNOTATION_TYPE, FIELD})
  @Retention(RUNTIME)
  @Documented
  @interface Container
  {

    /**
     * Returns the {@link LocalizedProperty @LocalizedProperty} annotations.
     *
     * @return the {@link LocalizedProperty @LocalizedProperty} annotations
     */
    LocalizedProperty[] value();

  }

}
