package de.codecamp.messages.spring.vaadin.autoconfigure;


import com.vaadin.flow.i18n.I18NProvider;
import com.vaadin.flow.server.VaadinServiceInitListener;
import de.codecamp.messages.spring.ExtendedMessageSource;
import de.codecamp.messages.spring.autoconfigure.MessagesAutoConfiguration;
import de.codecamp.messages.spring.vaadin.MessageSourceI18NProvider;
import de.codecamp.messages.spring.vaadin.VaadinMessageAccessor;
import de.codecamp.messages.spring.vaadin.VaadinSessionLocaleServiceInitListener;
import de.codecamp.messages.spring.vaadin.security.LocaleAndTimeZoneExtractor;
import de.codecamp.messages.spring.vaadin.security.PostAuthenticationSessionLocaleHandler;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.event.AbstractAuthenticationEvent;


@AutoConfiguration(before = MessagesAutoConfiguration.class)
@EnableConfigurationProperties
public class MessagesVaadinAutoConfiguration
{

  @Bean
  @ConditionalOnMissingBean
  I18NProvider vaadinI18NProvider(ExtendedMessageSource messageSource)
  {
    MessageSourceI18NProvider bean = new MessageSourceI18NProvider();
    bean.setMessageSource(messageSource);
    return bean;
  }

  @Bean
  VaadinServiceInitListener vaadinSessionLocaleServiceInitListener()
  {
    return new VaadinSessionLocaleServiceInitListener();
  }

  @Bean
  VaadinMessageAccessor vaadinMessageAccessor(ExtendedMessageSource messageSource)
  {
    return new VaadinMessageAccessor(messageSource);
  }


  @Configuration
  @ConditionalOnClass(AbstractAuthenticationEvent.class)
  static class PostAuthenticationSessionLocaleHandlerConfig
  {

    @Bean
    PostAuthenticationSessionLocaleHandler vaadinPostAuthenticationSessionLocaleHandler(
        @Autowired(required = false) List<LocaleAndTimeZoneExtractor> localeAndTimeZoneExtractors)
    {
      PostAuthenticationSessionLocaleHandler bean = new PostAuthenticationSessionLocaleHandler();
      if (localeAndTimeZoneExtractors != null)
        bean.setLocaleAndTimeZoneExtractors(localeAndTimeZoneExtractors);
      return bean;
    }

  }

}
