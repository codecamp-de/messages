package de.codecamp.messages.spring.vaadin;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.server.VaadinSession;
import de.codecamp.messages.MessageKeyUtils;
import de.codecamp.messages.Messages;
import de.codecamp.messages.spring.MessageAccessUtils;
import de.codecamp.messages.spring.MessageAccessor;
import de.codecamp.messages.spring.vaadin.annotations.LocalizedProperty;
import de.codecamp.messages.spring.vaadin.annotations.LocalizedText;
import de.codecamp.messages.spring.vaadin.annotations.LocalizedTextMessage;
import java.beans.PropertyDescriptor;
import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.function.Supplier;
import org.springframework.beans.BeanUtils;
import org.springframework.util.ReflectionUtils;


/**
 * Utility methods related to message access in Vaadin UIs.
 */
public final class LocalizationUtils
{

  private LocalizationUtils()
  {
    // utility class
  }


  /**
   * Localizes the components stored in the fields of the given component instance using
   * {@link LocalizedProperty} or one of the pre-made {@code Localized*} annotations, like
   * {@link LocalizedText} / {@link LocalizedTextMessage}.
   *
   * @param composite
   *          the component whose fields should be localized
   */
  public static void localizeComponents(Component composite)
  {
    ReflectionUtils.doWithFields(composite.getClass(), field ->
    {
      processFieldForLocalizableComponent(composite, field);
    }, field -> !Modifier.isStatic(field.getModifiers()));
  }

  private static void processFieldForLocalizableComponent(Component composite, Field field)
  {
    try
    {
      ReflectionUtils.makeAccessible(field);

      Object fieldObject = ReflectionUtils.getField(field, composite);
      if (fieldObject == null)
        return;

      Set<String> processedProperties = new HashSet<>();

      processContext(composite, field, fieldObject, null, null, field,
          "[Field " + field.getName() + " in " + composite.getClass().getName() + "]",
          processedProperties);

      // process where @LocalizedProperty is used as meta-annotation
      for (Annotation fieldAnnotation : field.getAnnotations())
      {
        String overrideProperty = readOverridePropertyOrKey(fieldAnnotation, "property");
        String overrideKey = readOverridePropertyOrKey(fieldAnnotation, "key");

        processContext(composite, field, fieldObject, overrideProperty, overrideKey,
            fieldAnnotation.annotationType(),
            "[Annotation @" + fieldAnnotation.annotationType().getSimpleName() + "]",
            processedProperties);
      }
    }
    catch (IllegalAccessException | InvocationTargetException ex)
    {
      String msg = "Failed to localize '%s' in '%s'.";
      msg = String.format(msg, field.getName(), composite.getClass().getName());
      throw new IllegalStateException(msg, ex);
    }
  }

  private static String readOverridePropertyOrKey(Annotation annotation, String attributeName)
  {
    try
    {
      Method method = annotation.getClass().getMethod(attributeName);
      if (method.getReturnType() != String.class || method.getParameterCount() != 0)
        return null;

      String value = (String) method.invoke(annotation);
      if (value.isEmpty())
        value = null;
      return value;
    }
    catch (@SuppressWarnings("unused") NoSuchMethodException | IllegalAccessException
        | InvocationTargetException ex)
    {
      return null;
    }
  }

  private static void processContext(Component composite, Field field, Object fieldObject,
      String overrideProperty, String overrideKey, AnnotatedElement context, String contextInfo,
      Set<String> processedProperties)
    throws IllegalAccessException,
      InvocationTargetException
  {
    LocalizedProperty[] localizedPropertyAts =
        context.getAnnotationsByType(LocalizedProperty.class);
    if (localizedPropertyAts.length == 0)
      return;

    Set<String> localKeys = null;

    for (LocalizedProperty localizedPropertyAt : localizedPropertyAts)
    {
      String propertyName =
          overrideProperty == null ? localizedPropertyAt.property() : overrideProperty;
      String keyForProperty = overrideKey == null ? localizedPropertyAt.key() : overrideKey;

      if (propertyName.isEmpty())
      {
        /*
         * When a @LocalizedProperty has an empty 'property', the properties to be localized will be
         * determined from the local parts of the messages keys declared at the same location
         * via @Messages. In that case 'key' also must be empty.
         */
        if (!keyForProperty.isEmpty())
        {
          String msg = "When 'property' is empty on @LocalizedProperty, then 'key' must also be"
              + " empty. " + contextInfo;
          throw new LocalizationException(msg);
        }

        if (localKeys == null)
        {
          Messages messages = context.getAnnotation(Messages.class);
          if (messages == null)
          {
            String msg = "When 'property' is empty on @LocalizedProperty, then there must be"
                + " @Messages besides it to determine the localized properties. " + contextInfo;
            throw new LocalizationException(msg);
          }

          localKeys = new LinkedHashSet<>();
          localKeys.addAll(Arrays.asList(messages.keys()));
          localKeys.addAll(Arrays.asList(messages.value()));
        }

        for (String localKey : localKeys)
        {
          if (processedProperties.contains(localKey))
            continue;

          // key is assumed to also be the property name
          String text = composite.getTranslation(
              MessageKeyUtils.getKeyFor(composite.getClass(), field.getName(), localKey));
          setLocalizedText(field, fieldObject, localKey, text, processedProperties);
        }
      }
      else
      {
        /*
         * When a @LocalizedProperty has a 'property' set, for this annotation only the specified
         * property will be localized.
         */
        if (keyForProperty.isEmpty())
        {
          /*
           * If 'key' is empty then it's assumed that a message key with the property name as local
           * part has been declared at the same location via @Messages.
           */
          if (localKeys == null)
          {
            Messages messages = context.getAnnotation(Messages.class);
            if (messages == null)
            {
              String msg = "When 'property' is set on @LocalizedProperty and 'key' is empty, then"
                  + " @Messages must be used besides it to declare a message key with the same"
                  + " (local) name as the property." + contextInfo;
              throw new LocalizationException(msg);
            }

            localKeys = new LinkedHashSet<>();
            localKeys.addAll(Arrays.asList(messages.keys()));
            localKeys.addAll(Arrays.asList(messages.value()));
          }

          if (!localKeys.contains(propertyName))
          {
            String msg = "When 'property' is set on @LocalizedProperty and 'key' is empty, then"
                + " @Messages must be used besides it to declare a message key with the same"
                + " (local) name as the property." + contextInfo;
            throw new LocalizationException(msg);
          }

          String text = composite.getTranslation(
              MessageKeyUtils.getKeyFor(composite.getClass(), field.getName(), propertyName));
          setLocalizedText(field, fieldObject, propertyName, text, processedProperties);
        }
        else
        {
          /*
           * If 'key' is set then it's assumed to be a (full) message key that has been declared
           * somewhere else.
           */
          String text = composite.getTranslation(keyForProperty);
          setLocalizedText(field, fieldObject, propertyName, text, processedProperties);
        }
      }
    }
  }

  private static void setLocalizedText(Field field, Object component, String propertyName,
      String localizedText, Set<String> processedProperties)
    throws IllegalAccessException,
      InvocationTargetException
  {
    PropertyDescriptor propertyDescriptor =
        BeanUtils.getPropertyDescriptor(component.getClass(), propertyName);
    if (propertyDescriptor == null && !(component instanceof Component)
        && component instanceof Supplier)
    {
      Object wrappedObject = ((Supplier<?>) component).get();
      if (wrappedObject instanceof Component)
      {
        propertyDescriptor =
            BeanUtils.getPropertyDescriptor(wrappedObject.getClass(), propertyName);
        if (propertyDescriptor != null)
        {
          component = wrappedObject;
        }
      }
    }
    if (propertyDescriptor == null)
    {
      String msg = "The object of type %s in field %s#%s does not have the bean property '%s'.";
      msg = String.format(msg, component.getClass().getName(), field.getDeclaringClass().getName(),
          field.getName(), propertyName);
      throw new LocalizationException(msg);
    }

    Method setter = propertyDescriptor.getWriteMethod();
    if (setter == null)
    {
      String msg =
          "The object of type %s in field %s#%s does not have a setter for the bean property '%s'.";
      msg = String.format(msg, component.getClass().getName(), field.getDeclaringClass().getName(),
          field.getName(), propertyName);
      throw new LocalizationException(msg);
    }

    setter.invoke(component, localizedText);

    processedProperties.add(propertyName);
  }


  /**
   * Inject message proxies into any appropriate fields of the given component.
   *
   * @param component
   *          the component using message proxies
   */
  public static void injectMessageProxies(Component component)
  {
    MessageAccessUtils.injectMessageProxies(component, component::getTranslation);
  }


  /**
   * Returns access to localized messages and related information for the locale of the current
   * Vaadin UI or session.
   * <p>
   * <em>The returned accessor is not serializable, so no references to it should be stored in
   * Vaadin components, unless they're not required to be serializable.</em>
   *
   * @return access to localized messages
   */
  public static MessageAccessor getL10n()
  {
    VaadinSession session = VaadinSession.getCurrent();
    if (session == null)
      throw new IllegalStateException("No current Vaadin session found.");

    return session.getService().getInstantiator().getOrCreate(VaadinMessageAccessor.class);
  }

}
