package de.codecamp.messages.spring.vaadin;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Composite;


/**
 * A convenient base class for {@link Composite Composites} that will be localized.
 *
 * @param <T>
 *          the type of the content
 */
public abstract class LocalizedComposite<T extends Component>
  extends
    Composite<T>
  implements
    HasLocalization
{

  protected LocalizedComposite()
  {
    LocalizationUtils.injectMessageProxies(this);
  }

  /**
   * Localizes components stored in fields of this composite. Call this method once all localizable
   * components have been created within {@link #initContent()}.
   *
   * @see LocalizationUtils#localizeComponents(Component)
   */
  protected void localizeComponents()
  {
    LocalizationUtils.localizeComponents(this);
  }

}
