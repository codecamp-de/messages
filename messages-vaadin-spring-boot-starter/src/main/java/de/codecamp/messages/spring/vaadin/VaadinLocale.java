package de.codecamp.messages.spring.vaadin;


import com.vaadin.flow.component.UI;
import com.vaadin.flow.server.VaadinSession;
import java.time.ZoneId;
import java.util.Locale;
import java.util.TimeZone;
import org.springframework.context.i18n.LocaleContextHolder;


/**
 * Utility class to access locale-related information associated with the current thread or more
 * specifically with the current Vaadin {@link UI#getCurrent() UI} or
 * {@link VaadinSession#getCurrent() VaadinSession}.
 * <p>
 * Essentially, the purpose is provide a Vaadin-specific replacement for
 * {@link LocaleContextHolder}.
 */
public final class VaadinLocale
{

  private VaadinLocale()
  {
    // utility class
  }


  /**
   * Returns the {@link Locale} associated with the current thread.
   *
   * @return the {@link Locale} associated with the current thread; never {@code null}
   * @throws IllegalStateException
   *           if the current thread is not associated with a Vaadin UI or session
   */
  public static Locale getLocale()
  {
    VaadinSession session = VaadinSession.getCurrent();
    if (session == null)
      throw new IllegalStateException("No current Vaadin UI or session found.");

    UI ui = UI.getCurrent();
    if (ui != null)
      return ui.getLocale();

    return session.getLocale();
  }

  /**
   * Returns the {@link TimeZone} associated with the current thread or the system default.
   *
   * @return the {@link TimeZone} associated with the current thread; never {@code null}
   * @throws IllegalStateException
   *           if the current thread is not associated with a Vaadin UI or session
   */
  public static ZoneId getTimeZone()
  {
    VaadinSession session = VaadinSession.getCurrent();
    if (session == null)
      throw new IllegalStateException("No current Vaadin UI or session found.");

    ZoneId timeZone = SessionLocaleUtils.getTimeZone(session);
    if (timeZone != null)
      return timeZone;

    return ZoneId.systemDefault();
  }

}
