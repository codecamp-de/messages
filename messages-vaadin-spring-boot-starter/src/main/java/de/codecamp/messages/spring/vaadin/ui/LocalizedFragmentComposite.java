package de.codecamp.messages.spring.vaadin.ui;


import com.vaadin.flow.component.Component;
import de.codecamp.messages.spring.vaadin.LocalizationUtils;
import de.codecamp.messages.spring.vaadin.LocalizedComposite;
import de.codecamp.vaadin.flowdui.FragmentComposite;


/**
 * An extension of {@link FragmentComposite} with the same functionality of
 * {@link LocalizedComposite}.
 */
public class LocalizedFragmentComposite
  extends
    FragmentComposite
{

  /**
   * Localizes components stored in fields of this composite. Call this method once all translated
   * components have been created within {@link #createContent()} or after calling
   * {@link #initializeContent()}.
   *
   * @see LocalizationUtils#localizeComponents(Component)
   */
  protected void localizeComponents()
  {
    LocalizationUtils.localizeComponents(this);
  }

  /**
   * {@inheritDoc}
   * <p>
   * <em>This implementation only calls {@link #localizeComponents()}. So if you override it, make
   * sure to call {@code super.contentCreated();} or {@code localizeComponents();} at an appropriate
   * moment.</em>
   *
   */
  @Override
  protected void contentCreated()
  {
    localizeComponents();
  }

}
