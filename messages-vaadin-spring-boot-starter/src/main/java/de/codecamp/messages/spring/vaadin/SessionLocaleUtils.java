package de.codecamp.messages.spring.vaadin;


import static java.util.Objects.requireNonNull;

import com.vaadin.flow.server.VaadinSession;
import java.time.ZoneId;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.atomic.AtomicReference;


/**
 * Utility class to change the {@link Locale} and/or {@link TimeZone} of a Vaadin session from the
 * initial values that have been determined automatically. E.g. after logging in when a user's
 * explicit settings could be loaded.
 */
public final class SessionLocaleUtils
{

  private SessionLocaleUtils()
  {
    // utility class
  }


  /**
   * Returns the locale of the current session.
   *
   * @return the session locale
   */
  public static Locale getLocale()
  {
    VaadinSession session =
        requireNonNull(VaadinSession.getCurrent(), "No current VaadinSession found.");
    return getLocale(session);
  }

  /**
   * Updates the locale of the current session.
   *
   * @param locale
   *          the new session locale
   */
  public static void setLocale(Locale locale)
  {
    VaadinSession session =
        requireNonNull(VaadinSession.getCurrent(), "No current VaadinSession found.");
    setLocale(session, locale);
  }

  /**
   * Returns the locale of the given session.
   * <p>
   * <em>This method cannot be called while already holding the lock to a different Vaadin session
   * than the provided one.</em>
   *
   * @param session
   *          the session to update
   * @return the session locale
   */
  public static Locale getLocale(VaadinSession session)
  {
    requireNonNull(session, "session must not be null");
    AtomicReference<Locale> localeHolder = new AtomicReference<>();
    session.access(() ->
    {
      localeHolder.set(session.getLocale());
    });
    return localeHolder.get();
  }

  /**
   * Updates the locale of the given session.
   * <p>
   * <em>This method cannot be called while already holding the lock to a different Vaadin session
   * than the provided one.</em>
   *
   * @param session
   *          the session to update
   * @param locale
   *          the new session locale
   */
  public static void setLocale(VaadinSession session, Locale locale)
  {
    requireNonNull(session, "session must not be null");
    session.access(() ->
    {
      session.setLocale(locale);
    });
  }


  /**
   * Returns the time zone of the current session.
   *
   * @return the session time zone
   */
  public static ZoneId getTimeZone()
  {
    VaadinSession session =
        requireNonNull(VaadinSession.getCurrent(), "No current VaadinSession found.");
    return getTimeZone(session);
  }

  /**
   * Updates the time zone of the current session.
   *
   * @param timeZone
   *          the new session time zone
   */
  public static void setTimeZone(ZoneId timeZone)
  {
    VaadinSession session =
        requireNonNull(VaadinSession.getCurrent(), "No current VaadinSession found.");
    setTimeZone(session, timeZone);
  }

  /**
   * Updates the time zone of the current session.
   *
   * @param timeZone
   *          the new session time zone
   */
  public static void setTimeZone(TimeZone timeZone)
  {
    setTimeZone(timeZone == null ? null : timeZone.toZoneId());
  }

  /**
   * Returns the time zone of the given session.
   * <p>
   * <em>This method cannot be called while already holding the lock to a different Vaadin session
   * than the provided one.</em>
   *
   * @param session
   *          the session to update
   * @return the session time zone
   */
  public static ZoneId getTimeZone(VaadinSession session)
  {
    requireNonNull(session, "session must not be null");

    if (session.hasLock())
    {
      return session.getAttribute(ZoneId.class);
    }
    else
    {
      AtomicReference<ZoneId> timeZoneHolder = new AtomicReference<>();
      session.accessSynchronously(() ->
      {
        timeZoneHolder.set(session.getAttribute(ZoneId.class));
      });
      return timeZoneHolder.get();
    }
  }

  /**
   * Updates the time zone of the given session.
   * <p>
   * <em>This method cannot be called while already holding the lock to a different Vaadin session
   * than the provided one.</em>
   *
   * @param session
   *          the session to update
   * @param timeZone
   *          the new session time zone
   */
  public static void setTimeZone(VaadinSession session, ZoneId timeZone)
  {
    requireNonNull(session, "session must not be null");

    if (session.hasLock())
    {
      session.setAttribute(ZoneId.class, timeZone);
    }
    else
    {
      session.access(() ->
      {
        session.setAttribute(ZoneId.class, timeZone);
      });
    }
  }

  /**
   * Updates the time zone of the given session.
   * <p>
   * <em>This method cannot be called while already holding the lock to a different Vaadin session
   * than the provided one.</em>
   *
   * @param session
   *          the session to update
   * @param timeZone
   *          the new session time zone
   */
  public static void setTimeZone(VaadinSession session, TimeZone timeZone)
  {
    setTimeZone(session, timeZone == null ? null : timeZone.toZoneId());
  }

}
