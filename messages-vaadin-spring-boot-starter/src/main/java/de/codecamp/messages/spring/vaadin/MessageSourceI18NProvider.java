package de.codecamp.messages.spring.vaadin;


import com.vaadin.flow.i18n.I18NProvider;
import de.codecamp.messages.ResolvableMessage;
import de.codecamp.messages.spring.ExtendedMessageSource;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import org.springframework.context.NoSuchMessageException;


/**
 * A {@link I18NProvider} that adapts an {@link ExtendedMessageSource}.
 */
public class MessageSourceI18NProvider
  implements
    I18NProvider
{

  private ExtendedMessageSource messageSource;


  /* package */ ExtendedMessageSource getMessageSource()
  {
    return messageSource;
  }

  public void setMessageSource(ExtendedMessageSource messageSource)
  {
    this.messageSource = messageSource;
  }


  @Override
  public List<Locale> getProvidedLocales()
  {
    Set<Locale> availableLocales = messageSource.getAvailableLocales();
    Locale defaultLocale = messageSource.getDefaultLocale();

    /*
     * In certain circumstances, Vaadin uses the first locale in the list as a default. E.g. if no
     * other matching locale is found.
     */
    List<Locale> providedLocales = new ArrayList<>(availableLocales.size());
    if (defaultLocale != null)
      providedLocales.add(defaultLocale);

    for (Locale locale : availableLocales)
    {
      if (!locale.equals(defaultLocale))
        providedLocales.add(locale);
    }

    return providedLocales;
  }

  @Override
  public String getTranslation(String key, Locale locale, Object... params)
  {
    try
    {
      if (params != null && params.length == 1 && params[0] instanceof Map)
      {
        @SuppressWarnings("unchecked")
        Map<String, Object> args = (Map<String, Object>) params[0];
        return messageSource.getMessage(key, args, locale, VaadinLocale.getTimeZone());
      }

      return messageSource.getMessage(key, params, locale, VaadinLocale.getTimeZone());
    }
    catch (@SuppressWarnings("unused") NoSuchMessageException ex)
    {
      return "!" + key + "!";
    }
  }

  @Override
  public String getTranslation(Object key, Locale locale, Object... params)
  {
    try
    {
      if (key instanceof ResolvableMessage rm)
      {
        if (params != null && params.length > 0)
        {
          throw new IllegalArgumentException(
              "ResolvableMessages do not support additional message arguments.");
        }
        return messageSource.getMessage(rm, locale, VaadinLocale.getTimeZone());
      }
      else if (params != null && params.length == 1 && params[0] instanceof Map)
      {
        @SuppressWarnings("unchecked")
        Map<String, Object> args = (Map<String, Object>) params[0];
        return messageSource.getMessage(key.toString(), args, locale, VaadinLocale.getTimeZone());
      }

      return messageSource.getMessage(key.toString(), params, locale, VaadinLocale.getTimeZone());
    }
    catch (@SuppressWarnings("unused") NoSuchMessageException ex)
    {
      return "!" + key + "!";
    }
  }

}
