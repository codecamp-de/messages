package de.codecamp.messages.spring.vaadin;


import de.codecamp.messages.spring.AbstractMessageAccessor;
import de.codecamp.messages.spring.ExtendedMessageSource;
import de.codecamp.messages.spring.MessageAccessor;
import java.time.ZoneId;
import java.util.Locale;
import java.util.Optional;


/**
 * A specialized {@link MessageAccessor} for Vaadin.
 * <p>
 * This implementation is not serializable.
 */
public class VaadinMessageAccessor
  extends
    AbstractMessageAccessor
{

  public VaadinMessageAccessor(ExtendedMessageSource messageSource)
  {
    super(messageSource);
  }

  private VaadinMessageAccessor(ExtendedMessageSource messageSource, Locale locale, ZoneId timeZone)
  {
    super(messageSource, locale, timeZone);
  }


  @Override
  protected Optional<Locale> getContextualLocale()
  {
    /*
     * Vaadin UI and session always have a locale. And VaadinMessageAccessor is not expected to work
     * without both of them, anyway. So there is no way or need to exclude defaults or fallbacks.
     */
    return Optional.of(VaadinLocale.getLocale());
  }

  @Override
  protected Optional<ZoneId> getContextualTimeZone()
  {
    /*
     * Vaadin UI and session always have a locale. And VaadinMessageAccessor is not expected to work
     * without both of them, anyway. So there is no way or need to exclude defaults or fallbacks.
     */
    return Optional.of(VaadinLocale.getTimeZone());
  }


  @Override
  public MessageAccessor forLocale(Locale locale)
  {
    return new VaadinMessageAccessor(messageSource, locale, timeZone);
  }

}
