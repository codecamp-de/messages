package de.codecamp.messages.spring.vaadin.security;


import com.vaadin.flow.server.VaadinSession;
import de.codecamp.messages.spring.vaadin.SessionLocaleUtils;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Stream;
import org.springframework.context.event.EventListener;
import org.springframework.security.authentication.event.AbstractAuthenticationEvent;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.security.authentication.event.InteractiveAuthenticationSuccessEvent;
import org.springframework.security.core.Authentication;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;


/**
 * Listens to {@link AuthenticationSuccessEvent} and {@link InteractiveAuthenticationSuccessEvent}
 * and tries to update the session {@link SessionLocaleUtils#setLocale(VaadinSession, Locale)
 * locale} and {@link SessionLocaleUtils#setTimeZone(VaadinSession, TimeZone) time zone} based on
 * the {@link Authentication}. The first found locale and time zone will be used. The following
 * sources are considered in the order they're listed:
 * <ol>
 * <li>The {@link Authentication#getPrincipal() principal} is considered if it implements
 * {@link HasLocaleAndTimeZone}.</li>
 * <li>All registered {@link LocaleAndTimeZoneExtractor} instances are considered in the order they
 * are provided.</li>
 * </ol>
 */
public class PostAuthenticationSessionLocaleHandler
{

  private List<LocaleAndTimeZoneExtractor> localeAndTimeZoneExtractors;


  public void setLocaleAndTimeZoneExtractors(
      List<LocaleAndTimeZoneExtractor> localeAndTimeZoneExtractors)
  {
    this.localeAndTimeZoneExtractors = localeAndTimeZoneExtractors;
  }


  @EventListener({AuthenticationSuccessEvent.class, InteractiveAuthenticationSuccessEvent.class})
  public void onAuthenticationSuccess(AbstractAuthenticationEvent event)
  {
    ServletRequestAttributes requestAttributes =
        (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
    if (requestAttributes == null)
      return;


    AtomicReference<Locale> locale = new AtomicReference<>();
    AtomicReference<TimeZone> timeZone = new AtomicReference<>();

    if (event.getAuthentication()
        .getPrincipal() instanceof HasLocaleAndTimeZone hasLocaleAndTimeZone)
    {
      hasLocaleAndTimeZone.getLocale().ifPresent(locale::set);
      hasLocaleAndTimeZone.getTimeZone().ifPresent(timeZone::set);
    }

    if (localeAndTimeZoneExtractors != null && (locale.get() == null || timeZone.get() == null))
    {
      for (LocaleAndTimeZoneExtractor localeAndTimeZoneExtractor : localeAndTimeZoneExtractors)
      {
        if (locale.get() == null)
        {
          localeAndTimeZoneExtractor.getLocale(event.getAuthentication()).ifPresent(locale::set);
        }

        if (timeZone.get() == null)
        {
          localeAndTimeZoneExtractor.getTimeZone(event.getAuthentication())
              .ifPresent(timeZone::set);
        }

        if (locale.get() != null && timeZone.get() != null)
          break;
      }
    }

    if (locale.get() == null && timeZone.get() == null)
      return;

    getVaadinSessions(requestAttributes.getRequest()).forEach(session ->
    {
      if (locale.get() != null)
        SessionLocaleUtils.setLocale(session, locale.get());
      if (timeZone.get() != null)
        SessionLocaleUtils.setTimeZone(session, timeZone.get());
    });
  }

  private static Stream<VaadinSession> getVaadinSessions(HttpServletRequest request)
  {
    HttpSession httpSession = request.getSession(false);
    if (httpSession == null)
      return Stream.empty();

    return VaadinSession.getAllSessions(httpSession).stream();
  }

}
