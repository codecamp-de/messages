package de.codecamp.messages.spring.vaadin.annotations;


import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import de.codecamp.messages.spring.vaadin.LocalizationUtils;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;


/**
 * Used to translate the {@value #PROPERTY} property of components reusing an already declared
 * message key.
 *
 * @see LocalizationUtils#localizeComponents(com.vaadin.flow.component.Component)
 * @see LocalizedTitleMessage
 */
@Target({FIELD})
@Retention(RUNTIME)
@LocalizedProperty(property = LocalizedTitle.PROPERTY)
public @interface LocalizedTitle
{

  /**
   * The name of the property.
   */
  String PROPERTY = "title";


  /**
   * Returns the full message key code to localize the property.
   *
   * @return the full message key code to localize the property
   */
  String key();

}
