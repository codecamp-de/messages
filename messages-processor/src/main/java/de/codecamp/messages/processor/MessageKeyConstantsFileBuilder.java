package de.codecamp.messages.processor;


import static java.util.stream.Collectors.joining;

import com.squareup.javapoet.AnnotationSpec;
import com.squareup.javapoet.ArrayTypeName;
import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.FieldSpec;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;
import de.codecamp.messages.ResolvableMessage;
import de.codecamp.messages.codegen.DeclaredMessageKey;
import de.codecamp.messages.codegen.MessageCodegenUtils;
import de.codecamp.messages.codegen.MessageKeyConstants;
import java.io.IOException;
import java.util.stream.Stream;
import javax.annotation.processing.Filer;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;


public class MessageKeyConstantsFileBuilder
{

  private final TypeElement owningTypeElement;

  private final String packageName;

  private final String constantsSimpleTypeName;

  private final TypeSpec.Builder typeSpecBuilder;


  public MessageKeyConstantsFileBuilder(TypeElement owningTypeElement,
      ClassName generatedAnnotation, String packageName)
  {
    this.owningTypeElement = owningTypeElement;

    this.packageName = packageName;
    this.constantsSimpleTypeName = MessageCodegenUtils.getMessageConstantsSimpleTypeNameFor(
        MessageKeyProcessor.getMessageOwnerSimpleClassName(owningTypeElement));


    this.typeSpecBuilder =
        TypeSpec.interfaceBuilder(constantsSimpleTypeName).addModifiers(Modifier.PUBLIC);

    if (generatedAnnotation != null)
    {
      AnnotationSpec generatedAtSpec = AnnotationSpec.builder(generatedAnnotation)
          .addMember("value", "$S", MessageKeyProcessor.class.getName()).build();
      typeSpecBuilder.addAnnotation(generatedAtSpec);
    }

    AnnotationSpec messageKeyConstantsAtSpec = AnnotationSpec.builder(MessageKeyConstants.class)
        .addMember("value", "$T.class", ClassName.get(owningTypeElement)).build();
    typeSpecBuilder.addAnnotation(messageKeyConstantsAtSpec);
  }


  public TypeSpec.Builder getTypeSpecBuilder()
  {
    return typeSpecBuilder;
  }

  public String getConstantsSimpleTypeName()
  {
    return constantsSimpleTypeName;
  }


  public void addMessageKey(String constantBaseName, String code, String[] argTypes,
      String[] argNames)
  {
    String argTypesString;
    String argNamesString;

    if (argTypes == null || argTypes.length == 0)
    {
      argTypesString = "null";
      argNamesString = "null";
    }
    else
    {
      argTypesString =
          Stream.of(argTypes).collect(joining(".class, ", "new Class<?>[]{", ".class}"));

      CodeBlock.Builder argNameArrayBuilder = CodeBlock.builder();
      argNameArrayBuilder.add("new $T{ ", ArrayTypeName.of(String.class));
      boolean first = true;
      for (String argName : argNames)
      {
        if (first)
          first = false;
        else
          argNameArrayBuilder.add(", ");

        argNameArrayBuilder.add("$S", argName);
      }
      argNameArrayBuilder.add(" }");

      argNamesString = argNameArrayBuilder.build().toString();
    }

    if (SourceVersion.isKeyword(constantBaseName))
      constantBaseName += "_";


    typeSpecBuilder.addField(FieldSpec
        .builder(DeclaredMessageKey.class, constantBaseName, Modifier.PUBLIC, Modifier.STATIC,
            Modifier.FINAL) //
        .initializer("new $T($T.class, $S, $L, $L)", DeclaredMessageKey.class,
            ClassName.get(owningTypeElement), code, argTypesString, argNamesString) //
        .build());


    typeSpecBuilder.addField(FieldSpec
        .builder(String.class, constantBaseName + "_", Modifier.PUBLIC, Modifier.STATIC,
            Modifier.FINAL) //
        .initializer("$S", code) //
        .build());


    MethodSpec.Builder method =
        MethodSpec.methodBuilder(constantBaseName).addModifiers(Modifier.PUBLIC, Modifier.STATIC)
            .returns(ClassName.get(ResolvableMessage.class));
    if (argTypes != null)
    {
      for (int i = 0; i < argTypes.length; i++)
      {
        String argType = argTypes[i];
        String argName = argNames[i];

        TypeName argTypeName;
        if (argType.contains("."))
          argTypeName = ClassName.bestGuess(argType);
        else
          argTypeName = ClassName.get("", argType);

        String paramName = argName == null ? "arg" + i : argName;

        method.addParameter(argTypeName, paramName);
      }
    }

    if (argTypes != null)
    {
      method.addCode("return $T.forCode($S)", ResolvableMessage.class, code);
      for (int i = 0; i < argTypes.length; i++)
      {
        String argName = argNames[i];
        String paramName = argName == null ? "arg" + i : argName;
        method.addCode(".arg($S, $L)", argName, paramName);
      }
      method.addCode(".build();\n");
    }
    else
    {
      method.addStatement("return $T.forCodeNoArgs($S)", ResolvableMessage.class, code);
    }

    typeSpecBuilder.addMethod(method.build());
  }


  public void writeTo(Filer filer)
    throws IOException
  {
    TypeSpec typeSpec = typeSpecBuilder.build();

    JavaFile javaFile = JavaFile.builder(packageName, typeSpec).build();

    javaFile.writeTo(filer);
  }

}
