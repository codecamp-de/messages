package de.codecamp.messages.processor;


import de.codecamp.messages.MessageKeyUtils;
import de.codecamp.messages.MessageKeyWithArgs;
import java.util.Objects;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;


public class ProcessedMessageKey
  implements
    MessageKeyWithArgs,
    Comparable<ProcessedMessageKey>
{

  private static final String[] EMPTY_ARG_ARRAY = {};


  private final Element sourceElement;

  private final TypeElement owningTypeElement;

  private final String prefix;

  private final String localPart;

  private final String[] argTypes;

  private final String[] argNames;


  public ProcessedMessageKey(Element sourceElement, TypeElement owningTypeElement, String prefix,
      String localPart, String[] argTypes, String[] argNames)
  {
    Objects.requireNonNull(sourceElement, "sourceElement must not be null");
    Objects.requireNonNull(prefix, "prefix must not be null");
    Objects.requireNonNull(localPart, "localPart must not be null");

    this.sourceElement = sourceElement;
    this.owningTypeElement = owningTypeElement;
    this.prefix = prefix;
    this.localPart = localPart;
    this.argTypes = argTypes == null ? null : argTypes.clone();
    this.argNames = argNames == null ? null : argNames.clone();
  }


  public Element getSourceElement()
  {
    return sourceElement;
  }

  public TypeElement getOwningTypeElement()
  {
    return owningTypeElement;
  }

  public String getPrefix()
  {
    return prefix;
  }

  public String getLocalPart()
  {
    return localPart;
  }

  @Override
  public String getCode()
  {
    return MessageKeyUtils.getKeyFor(prefix, localPart);
  }

  @Override
  public boolean hasArgs()
  {
    return argTypes != null && argTypes.length > 0;
  }

  public int getArgCount()
  {
    if (!hasArgs())
      return 0;

    return argTypes.length;
  }

  @Override
  public String[] getArgTypes()
  {
    if (hasArgs())
      return argTypes.clone();
    else
      return EMPTY_ARG_ARRAY;
  }

  @Override
  public String[] getArgNames()
  {
    if (hasArgs())
      return argNames.clone();
    else
      return EMPTY_ARG_ARRAY;
  }


  @Override
  public int compareTo(ProcessedMessageKey o)
  {
    return toString().compareTo(o.toString());
  }

  @Override
  public int hashCode()
  {
    return toString().hashCode();
  }

  @Override
  public boolean equals(Object obj)
  {
    if (obj == null)
      return false;
    if (obj == this)
      return true;

    if (!(obj instanceof ProcessedMessageKey))
      return false;

    ProcessedMessageKey other = (ProcessedMessageKey) obj;
    return other.getPrefix().equals(getPrefix())
        && Objects.equals(other.getLocalPart(), getLocalPart());
  }

  @Override
  public String toString()
  {
    return getCode();
  }

}
