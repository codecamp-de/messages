package de.codecamp.messages.processor;


import com.squareup.javapoet.AnnotationSpec;
import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.MethodSpec.Builder;
import com.squareup.javapoet.ParameterSpec;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;
import de.codecamp.messages.codegen.MessageCodegenUtils;
import de.codecamp.messages.codegen.MessageProxyInterface;
import de.codecamp.messages.codegen.MessageProxyMethod;
import de.codecamp.messages.codegen.MessageProxyParam;
import de.codecamp.messages.proxy.MessageProxy;
import java.io.IOException;
import javax.annotation.processing.Filer;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;


public class MessageProxyFileBuilder
{

  private final String packageName;

  private final String constantsSimpleTypeName;

  private final TypeSpec.Builder typeSpecBuilder;


  public MessageProxyFileBuilder(TypeElement owningTypeElement, ClassName generatedAnntation,
      String packageName)
  {
    this.packageName = packageName;
    this.constantsSimpleTypeName = MessageCodegenUtils.getMessageProxiesSimpleTypeNameFor(
        MessageKeyProcessor.getMessageOwnerSimpleClassName(owningTypeElement));


    typeSpecBuilder =
        TypeSpec.interfaceBuilder(constantsSimpleTypeName).addModifiers(Modifier.PUBLIC);

    typeSpecBuilder.addSuperinterface(ClassName.get(MessageProxy.class));

    if (generatedAnntation != null)
    {
      AnnotationSpec generatedAtSpec = AnnotationSpec.builder(generatedAnntation)
          .addMember("value", "$S", MessageKeyProcessor.class.getName()).build();
      typeSpecBuilder.addAnnotation(generatedAtSpec);
    }

    AnnotationSpec messageProxyAtSpec = AnnotationSpec.builder(MessageProxyInterface.class)
        .addMember("sourceType", "$T.class", ClassName.get(owningTypeElement)).build();
    typeSpecBuilder.addAnnotation(messageProxyAtSpec);
  }


  public TypeSpec.Builder getTypeSpecBuilder()
  {
    return typeSpecBuilder;
  }

  public String getConstantsSimpleTypeName()
  {
    return constantsSimpleTypeName;
  }


  public void addMessageMethod(String constantName, String messageKey, String[] argTypes,
      String[] argNames)
  {
    AnnotationSpec messageProxyMethodAtSpec = AnnotationSpec.builder(MessageProxyMethod.class)
        .addMember("code", "$S", messageKey).build();

    if (SourceVersion.isKeyword(constantName))
      constantName += "_";

    Builder methodBuilder =
        MethodSpec.methodBuilder(constantName).addModifiers(Modifier.PUBLIC, Modifier.ABSTRACT)
            .addAnnotation(messageProxyMethodAtSpec).returns(String.class);

    if (argTypes != null)
    {
      for (int i = 0; i < argTypes.length; i++)
      {
        String argType = argTypes[i];
        String argName = argNames[i];

        TypeName argTypeName;
        if (argType.contains("."))
          argTypeName = ClassName.bestGuess(argType);
        else
          argTypeName = ClassName.get("", argType);

        String paramName = argName == null ? "arg" + i : argName;

        ParameterSpec.Builder builder = ParameterSpec.builder(argTypeName, paramName);
        if (argName != null)
          builder.addAnnotation(AnnotationSpec.builder(MessageProxyParam.class)
              .addMember("name", "$S", argName).build());
        methodBuilder.addParameter(builder.build());
      }
    }

    typeSpecBuilder.addMethod(methodBuilder.build());
  }

  public void writeTo(Filer filer)
    throws IOException
  {
    TypeSpec typeSpec = typeSpecBuilder.build();

    JavaFile javaFile = JavaFile.builder(packageName, typeSpec).build();

    javaFile.writeTo(filer);
  }

}
