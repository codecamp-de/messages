package de.codecamp.messages.processor;


import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toSet;
import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;
import static org.apache.commons.lang3.StringUtils.splitPreserveAllTokens;
import static org.apache.commons.lang3.StringUtils.substringAfter;
import static org.apache.commons.lang3.StringUtils.substringBefore;
import static org.apache.commons.lang3.StringUtils.trim;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.SetMultimap;
import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.MethodSpec.Builder;
import de.codecamp.messages.MessageKey;
import de.codecamp.messages.MessageKeyWithArgs;
import de.codecamp.messages.Messages;
import de.codecamp.messages.MessagesFor;
import de.codecamp.messages.codegen.MessageCodegenUtils;
import de.codecamp.messages.shared.bundle.BundleException;
import de.codecamp.messages.shared.bundle.BundleFile;
import de.codecamp.messages.shared.bundle.MessageBundleManager;
import de.codecamp.messages.shared.conf.BundleMismatchPolicy;
import de.codecamp.messages.shared.conf.MessageArgPolicy;
import de.codecamp.messages.shared.conf.MissingMessagePolicy;
import de.codecamp.messages.shared.conf.ProjectConf;
import de.codecamp.messages.shared.conf.ProjectConfException;
import de.codecamp.messages.shared.conf.UndeclaredKeyPolicy;
import de.codecamp.messages.shared.messageformat.MessageFormatSupport;
import de.codecamp.messages.shared.model.AbstractPersistableData.PersistableDataException;
import de.codecamp.messages.shared.model.MessageKeyIndex;
import de.codecamp.messages.shared.model.MessageKeyWithSourceLocation;
import de.codecamp.messages.shared.validation.MessageValidationUtils;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.annotation.ElementType;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;
import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.PrimitiveType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.tools.Diagnostic.Kind;
import javax.tools.FileObject;
import javax.tools.StandardLocation;
import org.apache.commons.lang3.tuple.Pair;


public class MessageKeyProcessor
  extends
    AbstractProcessor
{

  public static final String OPT_SKIP = "messages.skip";


  private static final Map<String, String> DEFAULT_TYPE_ABBREVIATIONS = new HashMap<>();
  static
  {
    DEFAULT_TYPE_ABBREVIATIONS.put("*", Object.class.getName());

    DEFAULT_TYPE_ABBREVIATIONS.put("b", boolean.class.getName());
    DEFAULT_TYPE_ABBREVIATIONS.put("B", Boolean.class.getName());

    DEFAULT_TYPE_ABBREVIATIONS.put("i", int.class.getName());
    DEFAULT_TYPE_ABBREVIATIONS.put("I", Integer.class.getName());
    DEFAULT_TYPE_ABBREVIATIONS.put("l", long.class.getName());
    DEFAULT_TYPE_ABBREVIATIONS.put("L", Long.class.getName());
    DEFAULT_TYPE_ABBREVIATIONS.put("f", float.class.getName());
    DEFAULT_TYPE_ABBREVIATIONS.put("F", Float.class.getName());
    DEFAULT_TYPE_ABBREVIATIONS.put("d", double.class.getName());
    DEFAULT_TYPE_ABBREVIATIONS.put("D", Double.class.getName());
    DEFAULT_TYPE_ABBREVIATIONS.put("BI", BigInteger.class.getName());
    DEFAULT_TYPE_ABBREVIATIONS.put("BigInteger", BigInteger.class.getName());
    DEFAULT_TYPE_ABBREVIATIONS.put("BD", BigDecimal.class.getName());
    DEFAULT_TYPE_ABBREVIATIONS.put("BigDecimal", BigDecimal.class.getName());
    DEFAULT_TYPE_ABBREVIATIONS.put("N", Number.class.getName());
    DEFAULT_TYPE_ABBREVIATIONS.put("Number", Number.class.getName());

    DEFAULT_TYPE_ABBREVIATIONS.put("s", String.class.getName());
    DEFAULT_TYPE_ABBREVIATIONS.put("S", String.class.getName());
    DEFAULT_TYPE_ABBREVIATIONS.put("string", String.class.getName());
    DEFAULT_TYPE_ABBREVIATIONS.put("String", String.class.getName());

    DEFAULT_TYPE_ABBREVIATIONS.put("Date", Date.class.getName());
    DEFAULT_TYPE_ABBREVIATIONS.put("LocalDate", LocalDate.class.getName());
    DEFAULT_TYPE_ABBREVIATIONS.put("LocalTime", LocalTime.class.getName());
    DEFAULT_TYPE_ABBREVIATIONS.put("LocalDateTime", LocalDateTime.class.getName());
    DEFAULT_TYPE_ABBREVIATIONS.put("Instant", Instant.class.getName());
    DEFAULT_TYPE_ABBREVIATIONS.put("ZonedDateTime", ZonedDateTime.class.getName());

    DEFAULT_TYPE_ABBREVIATIONS.put("money", "javax.money.MonetaryAmount");
    DEFAULT_TYPE_ABBREVIATIONS.put("Money", "javax.money.MonetaryAmount");
    DEFAULT_TYPE_ABBREVIATIONS.put("MonetaryAmount", "javax.money.MonetaryAmount");
  }

  private static final Map<String, PrimitiveType> PRIMITIVE_TYPES = new HashMap<>();

  /**
   * Used to log errors with a throwable that would otherwise be lost.
   */
  private static final Logger LOG = Logger.getLogger(MessageKeyProcessor.class.getName());


  private ProcessorUtils utils;


  private ProjectConf projectConf;


  private boolean dontProcess = false;

  /**
   * whether the message keys are incomplete; e.g. because imported message keys couldn't be loaded
   */
  private boolean messageKeysIncomplete = false;

  /**
   * whether the imported message keys and messages could be loaded completely
   */
  private boolean importsLoaded = true;


  private MessageBundleManager<Path, Path, IOException> messageBundleManager;

  private final Set<TypeElement> processedTypes = new HashSet<>();

  private final Set<String> hasMessageConstantsCache = new HashSet<>();

  // private Set<String> hasMessageProxiesCache = new HashSet<>();

  /**
   * Skipped in regular handling. E.g. bean getters are treated differently than regular methods.
   */
  private final Set<ExecutableElement> skipAsMethod = new HashSet<>();

  /**
   * outside source type (via @MessagesFor) -> forProperties
   */
  private final Map<TypeElement, Boolean> externalTypesForProperties = new HashMap<>();

  /**
   * the keys processed during this compilation; containing TypeElement -> ProcessedMessageKey
   */
  private final SetMultimap<TypeElement, ProcessedMessageKey> messageKeysPerType =
      LinkedHashMultimap.create();

  /**
   * The packages in which associated code was generated. Only saved when different from the source
   * types package.
   */
  private final Map<TypeElement, String> codegenPackagesPerType = new HashMap<>();

  /**
   * the keys processed during this compilation; message key code -> ProcessedMessageKey
   */
  private final Map<String, ProcessedMessageKey> processedMessageKeys = new HashMap<>();

  /**
   * The messages per locale found for the message keys declared in this project. Does not contain
   * messages for imported message keys. Here it's enough that the key is present, indicating that a
   * message is intended to be there eventually. Until then validation will fail, but that's okay
   * for snapshot builds.
   */
  private SetMultimap<Locale, MessageKey> messagesInProject;

  private MessageKeyIndex messageKeysInProject;

  private final Map<String, MessageKeyIndex> importedMessageKeysPerModule = new HashMap<>();


  private MessageFormatSupport messageFormatSupport;


  /**
   * A full and incremental build cannot be directly distinguished. But for the purposes of this
   * annotation processor a clean build can be assumed when the index is missing or empty.
   */
  private boolean cleanBuild;


  public MessageKeyProcessor()
  {
  }


  @Override
  public SourceVersion getSupportedSourceVersion()
  {
    return SourceVersion.latest();
  }

  @Override
  public Set<String> getSupportedAnnotationTypes()
  {
    // To support meta annotations all annotations need to be examined.
    return Collections.singleton("*");
  }

  @Override
  public Set<String> getSupportedOptions()
  {
    return Stream.concat(Stream.of(OPT_SKIP), ProjectConf.ALL_CONF_NAMES.stream()).collect(toSet());
  }


  @Override
  public synchronized void init(ProcessingEnvironment initProcessingEnv)
  {
    super.init(initProcessingEnv);

    utils = new ProcessorUtils(processingEnv);
    boolean skip = utils.getOptionAsBoolean(OPT_SKIP, false);
    if (skip)
    {
      dontProcess = true;
      processingEnv.getMessager().printMessage(Kind.WARNING, "Skipping MessageKeyProcessor.");
      return;
    }

    try
    {
      /*
       * An annotation processor cannot reliably resolve relative paths and has no built-in way to
       * e.g. access the root directory of the project being built. I.e. provided paths must be
       * absolute.
       */

      projectConf = new ProjectConf(utils::getOption);
    }
    catch (ProjectConfException ex)
    {
      processingEnv.getMessager().printMessage(Kind.ERROR, ex.getMessage());
      dontProcess = true;
    }
  }


  @Override
  public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv)
  {
    if (dontProcess)
      return false;


    if (!loadMessageKeyIndex())
      return false;


    // ********************
    // process @MessagesFor

    /*
     * Discover all classes (currently being compiled) containing @MessagesFor annotations before
     * processing them.
     */
    Set<? extends Element> elementsWithMessagesFor =
        ProcessorUtils.getElementsAnnotatedWith(roundEnv, MessagesFor.class);
    for (Element carrierElement : elementsWithMessagesFor)
    {
      Set<AnnotationMirror> annotationMirrors =
          LangModelUtils.getAnnotationMirrors(carrierElement, MessagesFor.class);
      for (AnnotationMirror messagesForAnnot : annotationMirrors)
      {
        TypeElement sourceTypeElement =
            LangModelUtils.getAnnotationValueAsType(messagesForAnnot, "type");
        boolean forProperties = LangModelUtils.getAnnotationValueAs(messagesForAnnot,
            "forProperties", Boolean.class, false);
        externalTypesForProperties.put(sourceTypeElement, forProperties);

        codegenPackagesPerType.put(sourceTypeElement,
            LangModelUtils.getPackageName(carrierElement));
      }
    }
    for (Element carrierElement : elementsWithMessagesFor)
    {
      Set<AnnotationMirror> annotationMirrors =
          LangModelUtils.getAnnotationMirrors(carrierElement, MessagesFor.class);
      for (AnnotationMirror messagesForAnnot : annotationMirrors)
      {
        TypeElement sourceTypeElement =
            LangModelUtils.getAnnotationValueAsType(messagesForAnnot, "type");
        processType(sourceTypeElement, messagesForAnnot);
      }
    }


    // ********************
    // process @Messages

    // find all annotations that use @Messages as meta-annotation; include @Messages in that list
    Set<? extends TypeElement> composedMessagesAnnotations =
        LangModelUtils.findComposedAnnotationTypes(annotations, Messages.class, true);

    for (TypeElement annotationElement : composedMessagesAnnotations)
    {
      for (Element element : roundEnv.getElementsAnnotatedWith(annotationElement))
      {
        if (element.getKind() == ElementKind.ANNOTATION_TYPE)
          continue;

        if (element.getKind().isClass() || element.getKind().isInterface())
        {
          processType((TypeElement) element, null);
        }
        else if (element.getKind() == ElementKind.METHOD || element.getKind() == ElementKind.FIELD
            || element.getKind() == ElementKind.ENUM_CONSTANT)
        {
          TypeElement typeElement = LangModelUtils.getEnclosingTypeElement(element);
          processType(typeElement, null);
        }
        else
        {
          processingEnv.getMessager().printMessage(Kind.WARNING, "unsupported element", element);
        }
      }
    }


    /*
     * For each compilation a new instance of the processor is created. However, at least in
     * Eclipse, there can be multiple "roundEnv.processingOver() == true" during the same
     * compilation. It seems, the second set of rounds compiles the files generated during the first
     * set of rounds, even though they were already compiled in the second round of the first set of
     * rounds.
     *
     * Fortunately it seems that the annotated classes of interest here are not compiled twice. For
     * now, only update the message key index and message bundles when any message key is found in
     * the compiled classes.
     */
    if (roundEnv.processingOver() && !processedTypes.isEmpty())
    {
      if (messageKeysInProject != null)
      {
        updateMessageKeyIndex();

        checkUnmappedKeys();

        processBundleFiles();
      }

      processedTypes.clear();
    }

    return false;
  }

  protected void processType(TypeElement sourceTypeElement, AnnotationMirror messagesForAnnot)
  {
    if (processedTypes.contains(sourceTypeElement))
      return;

    processedTypes.add(sourceTypeElement);

    ClassName generatedClassName = utils.getGeneratedAnnotation().map(ClassName::get).orElse(null);

    String codegenPackageName = getCodegenPackageName(sourceTypeElement);

    MessageKeyConstantsFileBuilder constantsBuilder = null;
    if (projectConf.getGenerateConstants())
    {
      constantsBuilder = new MessageKeyConstantsFileBuilder(sourceTypeElement, generatedClassName,
          codegenPackageName);
    }

    MessageProxyFileBuilder proxyBuilder = null;
    if (projectConf.getGenerateProxies())
    {
      proxyBuilder =
          new MessageProxyFileBuilder(sourceTypeElement, generatedClassName, codegenPackageName);
    }

    boolean hasMessageAtOnType;
    String prefix = null;
    boolean forType;
    boolean forProperties;
    Set<String> ignoreImplicitKeys = null;

    {
      List<Messages> classMsgsAt =
          LangModelUtils.findMetaAnnotations(sourceTypeElement, Messages.class);

      hasMessageAtOnType = !classMsgsAt.isEmpty();

      Set<String> localParts = new LinkedHashSet<>();
      if (!classMsgsAt.isEmpty())
      {
        // use the first prefix that isn't the default
        prefix = classMsgsAt.stream().map(Messages::prefix)
            .filter(p -> !p.equals(Messages.PREFIX_SOURCE_TYPE)).findFirst().orElse(null);

        forType = classMsgsAt.stream().anyMatch(Messages::forType);
        forProperties = classMsgsAt.stream().anyMatch(Messages::forProperties);

        for (Messages msgsAt : classMsgsAt)
        {
          localParts.addAll(Arrays.asList(msgsAt.keys()));
          localParts.addAll(Arrays.asList(msgsAt.value()));
        }
      }
      else if (messagesForAnnot != null)
      {
        hasMessageAtOnType = true;

        forType =
            LangModelUtils.getAnnotationValueAs(messagesForAnnot, "forType", Boolean.class, false);
        forProperties = LangModelUtils.getAnnotationValueAs(messagesForAnnot, "forProperties",
            Boolean.class, false);

        List<String> ignoreImplicitKeysList = LangModelUtils.getAnnotationValuesAs(messagesForAnnot,
            "ignoreImplicitKeys", String.class, emptyList());
        ignoreImplicitKeys = new HashSet<>(ignoreImplicitKeysList);

        prefix = LangModelUtils.getAnnotationValueAs(messagesForAnnot, "prefix", String.class,
            Messages.PREFIX_SOURCE_TYPE);

        localParts
            .addAll(LangModelUtils.getAnnotationValuesAs(messagesForAnnot, "keys", String.class));
        localParts
            .addAll(LangModelUtils.getAnnotationValuesAs(messagesForAnnot, "value", String.class));
      }
      else
      {
        forType = false;
        forProperties = false;
      }

      if (prefix == null || prefix.equals(Messages.PREFIX_SOURCE_TYPE))
        prefix = sourceTypeElement.getQualifiedName().toString() + ".";

      if (localParts.remove(""))
      {
        forType = true;
      }

      if (forType)
        addMsgKey(sourceTypeElement, sourceTypeElement, prefix, MessageKey.TYPE_KEY_SUFFIX);

      addMsgKeys(sourceTypeElement, sourceTypeElement, prefix, localParts);
    }


    if (hasMessageAtOnType)
    {
      List<VariableElement> enumConstants =
          processIfEnum(prefix, sourceTypeElement, ignoreImplicitKeys);
      if (constantsBuilder != null && enumConstants != null && !enumConstants.isEmpty())
      {
        Builder forEnumMethodBuilder = MethodSpec.methodBuilder("forEnum")
            .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
            .addParameter(ClassName.get(sourceTypeElement), "element").returns(MessageKey.class);
        forEnumMethodBuilder.beginControlFlow("switch (element)");

        for (VariableElement enumConstant : enumConstants)
        {
          forEnumMethodBuilder.addCode("case $L:\n", enumConstant.getSimpleName());
          forEnumMethodBuilder.addStatement("$>return $L", enumConstant.getSimpleName());
          forEnumMethodBuilder.addCode("$<");
        }

        forEnumMethodBuilder.addCode("default:\n");
        forEnumMethodBuilder.addCode("$>");
        forEnumMethodBuilder.addStatement("String msg = $S",
            "Element %s for enum %s unknown. %s probably needs to be regenerated.");
        forEnumMethodBuilder.addStatement(
            "msg = String.format(msg, element.name(), $T.class.getName(), $L.class.getName())",
            sourceTypeElement, constantsBuilder.getConstantsSimpleTypeName());
        forEnumMethodBuilder.addStatement("throw new IllegalStateException(msg)");
        forEnumMethodBuilder.endControlFlow();
        constantsBuilder.getTypeSpecBuilder().addMethod(forEnumMethodBuilder.build());
      }
    }

    if (forProperties)
    {
      processProperties(prefix, sourceTypeElement, null, ignoreImplicitKeys, false);
    }

    processFields(prefix, sourceTypeElement, hasMessageAtOnType);
    processMethods(prefix, sourceTypeElement);


    /*
     * Go up type hierarchy to find message key constant interfaces to extend. Also determine
     * whether message keys for properties can be inherited or must be included here.
     */

    Deque<Pair<TypeElement, Boolean>> inheritedMessageTypeElementsQueue = new ArrayDeque<>();
    LangModelUtils.getSuperclassType(sourceTypeElement)
        .ifPresent(type -> inheritedMessageTypeElementsQueue.addLast(Pair.of(type, true)));
    LangModelUtils.getInterfaceTypes(sourceTypeElement)
        .forEach(type -> inheritedMessageTypeElementsQueue.addLast(Pair.of(type, true)));

    while (!inheritedMessageTypeElementsQueue.isEmpty())
    {
      Pair<TypeElement, Boolean> pair = inheritedMessageTypeElementsQueue.pop();

      TypeElement inheritedTypeElement = pair.getLeft();
      boolean extendConstants = pair.getRight();

      boolean inheritedTypeHasMessages = false;
      boolean inheritedTypeForProperties = false;

      List<Messages> messagesAts =
          LangModelUtils.findMetaAnnotations(inheritedTypeElement, Messages.class);
      if (!messagesAts.isEmpty())
      {
        inheritedTypeHasMessages = true;
        for (Messages messagesAt : messagesAts)
        {
          if (messagesAt.forProperties())
          {
            inheritedTypeForProperties = true;
            break;
          }
        }
      }
      if (!inheritedTypeHasMessages)
      {
        for (VariableElement fieldElement : LangModelUtils.getDeclaredFields(inheritedTypeElement))
        {
          if (LangModelUtils.findFirstMetaAnnotation(fieldElement, Messages.class).isPresent())
          {
            inheritedTypeHasMessages = true;
            break;
          }
        }
      }
      if (!inheritedTypeHasMessages)
      {
        for (ExecutableElement methodElement : LangModelUtils
            .getDeclaredMethods(inheritedTypeElement))
        {
          if (LangModelUtils.findFirstMetaAnnotation(methodElement, Messages.class).isPresent())
          {
            inheritedTypeHasMessages = true;
            break;
          }
        }
      }


      if (inheritedTypeHasMessages || externalTypesForProperties.containsKey(inheritedTypeElement)
          || hasMessageConstants(inheritedTypeElement))
      {
        if (extendConstants)
        {
          extendConstants = false;

          if (constantsBuilder != null)
          {
            constantsBuilder.getTypeSpecBuilder()
                .addSuperinterface(getMessageConstantsClassName(inheritedTypeElement));
          }
          if (proxyBuilder != null)
          {
            proxyBuilder.getTypeSpecBuilder()
                .addSuperinterface(getMessageProxiesClassName(inheritedTypeElement));
          }
        }

        if (!forProperties || inheritedTypeForProperties
            || externalTypesForProperties.getOrDefault(inheritedTypeElement, false))
        {
          continue;
        }

        processProperties(prefix, inheritedTypeElement, sourceTypeElement, ignoreImplicitKeys,
            true);
      }
      else if (forProperties)
      {
        processProperties(prefix, inheritedTypeElement, sourceTypeElement, ignoreImplicitKeys,
            true);
      }

      boolean finalExtendConstants = extendConstants;
      LangModelUtils.getSuperclassType(inheritedTypeElement).ifPresent(
          type -> inheritedMessageTypeElementsQueue.addLast(Pair.of(type, finalExtendConstants)));
      LangModelUtils.getInterfaceTypes(inheritedTypeElement).forEach(
          type -> inheritedMessageTypeElementsQueue.addLast(Pair.of(type, finalExtendConstants)));
    }


    for (ProcessedMessageKey key : getMsgKeys(sourceTypeElement))
    {
      if (constantsBuilder != null)
      {
        constantsBuilder.addMessageKey(key.getLocalPart(), key.getCode(), key.getArgTypes(),
            key.getArgNames());
      }
      if (proxyBuilder != null)
      {
        proxyBuilder.addMessageMethod(key.getLocalPart(), key.getCode(), key.getArgTypes(),
            key.getArgNames());
      }
    }


    if (constantsBuilder != null)
    {
      try
      {
        constantsBuilder.writeTo(processingEnv.getFiler());
      }
      catch (IOException ex)
      {
        String msg = String.format("Failed to write the message key constants for %s.",
            sourceTypeElement.getQualifiedName());
        processingEnv.getMessager().printMessage(Kind.ERROR, msg, sourceTypeElement);
        LOG.log(Level.SEVERE, msg, ex);
      }
    }

    if (proxyBuilder != null)
    {
      try
      {
        proxyBuilder.writeTo(processingEnv.getFiler());
      }
      catch (IOException ex)
      {
        String msg = String.format("Failed to write the message proxy for %s.",
            sourceTypeElement.getQualifiedName());
        processingEnv.getMessager().printMessage(Kind.ERROR, msg, sourceTypeElement);
        LOG.log(Level.SEVERE, msg, ex);
      }
    }
  }

  private List<VariableElement> processIfEnum(String prefix, TypeElement typeElement,
      Set<String> ignoredElements)
  {
    if (typeElement.getKind() == ElementKind.ENUM)
    {
      List<VariableElement> processedEnumConstants = new ArrayList<>();

      for (VariableElement enumConstant : LangModelUtils.getEnumConstants(typeElement))
      {
        if (ignoredElements != null
            && ignoredElements.contains(enumConstant.getSimpleName().toString()))
        {
          continue;
        }

        List<Messages> enumElemMsgsAt =
            LangModelUtils.findMetaAnnotations(enumConstant, Messages.class);

        if (!enumElemMsgsAt.isEmpty())
        {
          if (enumElemMsgsAt.get(0).ignoreImplicit())
            continue;

          for (Messages msgsAt : enumElemMsgsAt)
          {
            addMsgKeys(enumConstant, typeElement, prefix, enumConstant.getSimpleName().toString(),
                Arrays.asList(msgsAt.keys()));
            addMsgKeys(enumConstant, typeElement, prefix, enumConstant.getSimpleName().toString(),
                Arrays.asList(msgsAt.value()));
          }
        }

        // for enum constants, the default key is always created
        addMsgKey(enumConstant, typeElement, prefix, enumConstant.getSimpleName().toString());

        processedEnumConstants.add(enumConstant);
      }

      return processedEnumConstants;
    }

    return emptyList();
  }

  private void processProperties(String prefix, TypeElement typeElement,
      TypeElement incorporatingTypeElement, Set<String> ignoredProperties, boolean skipDuplicates)
  {
    if (incorporatingTypeElement == null)
      incorporatingTypeElement = typeElement;

    for (Map.Entry<String, ExecutableElement> property : LangModelUtils
        .findDeclaredBeanProperties(typeElement).entrySet())
    {
      ExecutableElement getterMethodElement = property.getValue();

      skipAsMethod.add(getterMethodElement);

      String propertyName = property.getKey();
      if (ignoredProperties != null && ignoredProperties.contains(propertyName))
        continue;

      List<Messages> propertyMsgsAts =
          LangModelUtils.findMetaAnnotations(getterMethodElement, Messages.class);

      boolean ignoreImplicit = false;
      Messages messagesAt = getterMethodElement.getAnnotation(Messages.class);
      if (messagesAt != null)
        ignoreImplicit = messagesAt.ignoreImplicit();
      // TODO check if the overridden method is actually included in the inherited message keys
      if (getterMethodElement.getAnnotation(Override.class) != null)
        ignoreImplicit = true;

      Set<String> propertyKeys = new HashSet<>();
      for (Messages msgsAt : propertyMsgsAts)
      {
        if (!msgsAt.prefix().equals(Messages.PREFIX_SOURCE_TYPE))
        {
          String msg = "Prefix only allowed on type level.";
          msg = String.format(msg);
          processingEnv.getMessager().printMessage(Kind.ERROR, msg, getterMethodElement);
        }

        propertyKeys.addAll(Arrays.asList(msgsAt.keys()));
        propertyKeys.addAll(Arrays.asList(msgsAt.value()));
      }

      // message key for property is created separately
      if (propertyKeys.remove(""))
        ignoreImplicit = false;

      /*
       * Skipping duplicates is just the easiest way to handle properties / getters that are
       * inherited from multiple classes and/or interfaces.
       */

      if (!ignoreImplicit)
        addMsgKey(getterMethodElement, incorporatingTypeElement, prefix, propertyName,
            skipDuplicates);

      addMsgKeys(getterMethodElement, incorporatingTypeElement, prefix, propertyName, propertyKeys,
          skipDuplicates);
    }
  }

  private void processFields(String prefix, TypeElement typeElement, boolean skipEnumConstants)
  {
    for (VariableElement fieldElement : LangModelUtils.getDeclaredFields(typeElement))
    {
      // enum constants are already handled above
      if (skipEnumConstants && fieldElement.getKind() == ElementKind.ENUM_CONSTANT)
        continue;

      List<Messages> fieldMsgsAt = LangModelUtils.findMetaAnnotations(fieldElement, Messages.class);
      if (fieldMsgsAt.isEmpty())
        continue;

      Set<String> localParts = new LinkedHashSet<>();

      String propertyName = fieldElement.getSimpleName().toString();

      for (Messages msgsAt : fieldMsgsAt)
      {
        if (!msgsAt.prefix().equals(Messages.PREFIX_SOURCE_TYPE))
        {
          String msg = "Prefix only allowed on type level.";
          msg = String.format(msg);
          processingEnv.getMessager().printMessage(Kind.ERROR, msg, fieldElement);
        }

        localParts.addAll(Arrays.asList(msgsAt.keys()));
        localParts.addAll(Arrays.asList(msgsAt.value()));
      }

      if (localParts.isEmpty())
        addMsgKey(fieldElement, typeElement, prefix, propertyName);
      else
        addMsgKeys(fieldElement, typeElement, prefix, propertyName, localParts);
    }
  }

  private void processMethods(String prefix, TypeElement typeElement)
  {
    for (ExecutableElement methodElement : LangModelUtils.getDeclaredMethods(typeElement))
    {
      if (skipAsMethod.contains(methodElement))
        continue;

      List<Messages> methodMsgsAt =
          LangModelUtils.findMetaAnnotations(methodElement, Messages.class);
      if (methodMsgsAt.isEmpty())
        continue;

      Set<String> localParts = new LinkedHashSet<>();

      for (Messages msgsAt : methodMsgsAt)
      {
        if (!msgsAt.prefix().equals(Messages.PREFIX_SOURCE_TYPE))
        {
          String msg = "Prefix only allowed on type level.";
          msg = String.format(msg);
          processingEnv.getMessager().printMessage(Kind.ERROR, msg, methodElement);
        }

        localParts.addAll(Arrays.asList(msgsAt.keys()));
        localParts.addAll(Arrays.asList(msgsAt.value()));

        if (localParts.isEmpty() || localParts.remove(""))
        {
          String msg = "Default message key not supported for methods.";
          msg = String.format(msg);
          processingEnv.getMessager().printMessage(Kind.ERROR, msg, methodElement);
        }
      }

      addMsgKeys(methodElement, typeElement, prefix, localParts);
    }
  }


  /* updates: cleanBuild, messageKeysInProject */
  protected boolean loadMessageKeyIndex()
  {
    messageKeysInProject = null; // NOPMD:UnusedAssignment
    cleanBuild = false; // NOPMD:UnusedAssignment

    // load message index if available
    MessageKeyIndex index;
    String indexFilePath = MessageKeyIndex.getPreferredFilePath(projectConf.getModuleName());

    try
    {
      FileObject indexFile =
          processingEnv.getFiler().getResource(StandardLocation.CLASS_OUTPUT, "", indexFilePath);
      try (InputStream in = indexFile.openInputStream())
      {
        index = MessageKeyIndex.readFrom(in);
      }
      cleanBuild = index.getSize() == 0; // NOPMD:UnusedAssignment
    }
    catch (@SuppressWarnings("unused") FileNotFoundException | NoSuchFileException ex)
    {
      // create new file
      index = new MessageKeyIndex();
      cleanBuild = true;
    }
    catch (IOException | PersistableDataException | RuntimeException ex)
    {
      // in Eclipse a generic ResourceException is wrapped in an IOException
      if (ex.getMessage() != null && ex.getMessage().contains("does not exist"))
      {
        // create new file
        index = new MessageKeyIndex();
        cleanBuild = true;
      }
      else
      {
        String msg = "Failed to open message key index '%s'. Message key processing stopped! See"
            + " log for exception.";
        msg = String.format(msg, indexFilePath);
        processingEnv.getMessager().printMessage(Kind.ERROR, msg);
        LOG.log(Level.SEVERE, msg, ex);
        return false;
      }
    }

    messageKeysInProject = index;

    return true;
  }

  /*
   * updates: contents of messageKeysInProject
   *
   * loadMessageKeyIndex() must be called before this method
   */
  protected void updateMessageKeyIndex()
  {
    messageKeysInProject.setModuleName(projectConf.getModuleName());
    messageKeysInProject.setTargetLocales(projectConf.getTargetLocales());
    messageKeysInProject.setBundleMappings(projectConf.getBundleMappings());
    messageKeysInProject.setImportedModules(new ArrayList<>(projectConf.getImports()));
    messageKeysInProject.setMessageFormat(projectConf.getMessageFormat());


    // determine the message keys that have a default message (i.e. in the root bundle file)
    Set<String> keysWithDefaultMessages = new HashSet<>();
    if (projectConf.getBundleDir() != null && messageBundleManager == null)
    {
      messageBundleManager = projectConf.createDefaultMessageBundleManager();
      for (BundleFile<Path, IOException> bundleFile : messageBundleManager
          .getBundleFiles(Locale.ROOT))
      {
        keysWithDefaultMessages.addAll(bundleFile.getKeys());
      }
    }


    for (TypeElement sourceTypeElement : messageKeysPerType.keySet())
    {
      Set<MessageKeyWithSourceLocation> keys = new HashSet<>();

      Set<ProcessedMessageKey> pmks = messageKeysPerType.get(sourceTypeElement);
      for (ProcessedMessageKey pmk : pmks)
      {
        String code = pmk.getCode();
        String sourceType = sourceTypeElement.getQualifiedName().toString();

        boolean defaultMessageAvailable = keysWithDefaultMessages.contains(code);

        MessageKeyWithSourceLocation keyWithLocation;

        if (pmk.getSourceElement() instanceof TypeElement)
        {
          keyWithLocation =
              new MessageKeyWithSourceLocation(code, pmk.getArgTypes(), pmk.getArgNames(),
                  sourceType, pmk.getLocalPart(), ElementType.TYPE, "", defaultMessageAvailable);
        }
        else if (pmk.getSourceElement() instanceof VariableElement)
        {
          keyWithLocation = new MessageKeyWithSourceLocation(code, pmk.getArgTypes(),
              pmk.getArgNames(), sourceType, pmk.getLocalPart(), ElementType.FIELD,
              pmk.getSourceElement().getSimpleName().toString(), defaultMessageAvailable);
        }
        else if (pmk.getSourceElement() instanceof ExecutableElement)
        {
          keyWithLocation = new MessageKeyWithSourceLocation(code, pmk.getArgTypes(),
              pmk.getArgNames(), sourceType, pmk.getLocalPart(), ElementType.METHOD,
              pmk.getSourceElement().getSimpleName().toString(), defaultMessageAvailable);
        }
        else
        {
          continue;
        }

        keys.add(keyWithLocation);
      }

      messageKeysInProject.updateMessageKeysFor(sourceTypeElement.getQualifiedName().toString(),
          keys);
      messageKeysInProject.updateCodegenPackageFor(sourceTypeElement.getQualifiedName().toString(),
          codegenPackagesPerType.get(sourceTypeElement));
    }

    String indexFilePath = MessageKeyIndex.getPreferredFilePath(projectConf.getModuleName());
    try
    {
      FileObject indexFile =
          processingEnv.getFiler().createResource(StandardLocation.CLASS_OUTPUT, "", indexFilePath);
      try (OutputStream out = indexFile.openOutputStream())
      {
        messageKeysInProject.writeTo(out);
      }
      processingEnv.getMessager().printMessage(Kind.NOTE,
          String.format("Message key index of module '%s' updated.", projectConf.getModuleName()));
    }
    catch (IOException | PersistableDataException | RuntimeException ex)
    {
      String msg = "Failed to write message key index '%s'.";
      msg = String.format(msg, indexFilePath);
      processingEnv.getMessager().printMessage(Kind.ERROR, msg);
      LOG.log(Level.SEVERE, msg, ex);
    }
  }

  protected void processBundleFiles()
  {
    if (projectConf.getMissingMessagePolicy() == MissingMessagePolicy.NONE
        && projectConf.getMessageArgPolicy() == MessageArgPolicy.NONE
        && projectConf.getUndeclaredKeyPolicy() == UndeclaredKeyPolicy.NONE
        && projectConf.getBundleMismatchPolicy() == BundleMismatchPolicy.NONE)
      return;


    if (messageFormatSupport == null)
    {
      messageFormatSupport = MessageFormatSupport.getSupport(projectConf);

      processingEnv.getMessager().printMessage(Kind.NOTE,
          String.format("Using '%s' message formatter to validate message arguments.",
              projectConf.getMessageFormat()));
    }


    loadImportedModules();

    try
    {
      projectConf.resolveBundleMappings(importedMessageKeysPerModule::get);
    }
    catch (ProjectConfException ex)
    {
      processingEnv.getMessager().printMessage(Kind.ERROR, ex.getMessage());
      return;
    }

    if (messageKeysInProject == null)
      return;

    long messageBundleProcessingStart = System.currentTimeMillis();

    if (messageBundleManager == null)
      messageBundleManager = projectConf.createDefaultMessageBundleManager();

    if (cleanBuild)
    {
      /*
       * These validations cannot be restricted to keys that are part of a partial build, so only
       * perform them on a full/clean build.
       */

      checkUndeclaredKeys();

      checkBundleMismatches();
    }

    checkMessages();


    try
    {
      messageBundleManager.save(true);
    }
    catch (BundleException ex)
    {
      String msg = "Failed to save all message bundle files of module '%s'.";
      msg = String.format(msg, projectConf.getModuleName());
      processingEnv.getMessager().printMessage(Kind.ERROR, msg);
      LOG.log(Level.SEVERE, msg, ex);
    }

    processingEnv.getMessager().printMessage(Kind.NOTE,
        String.format("Processing message bundle files of module '%s' took %sms.",
            projectConf.getModuleName(),
            (System.currentTimeMillis() - messageBundleProcessingStart)));
  }

  /**
   * Does not work from within Eclipse! CLASS_PATH not supported. However, the Eclipse plug-in has
   * its own validation and does not rely on this to work.
   */
  private void loadImportedModules()
  {
    // load imported modules
    Deque<String> importedModulesToLoad = new ArrayDeque<>(projectConf.getImports());
    Set<String> loadedModules = new HashSet<>();
    while (!importedModulesToLoad.isEmpty())
    {
      String moduleName = importedModulesToLoad.pop();

      // avoid duplicates
      if (!loadedModules.add(moduleName))
        continue;

      String keysFilePath = MessageKeyIndex.getPreferredFilePath(moduleName);
      try
      {
        InputStreamSupplier importedKeysFile = processingEnv.getFiler()
            .getResource(StandardLocation.CLASS_PATH, "", keysFilePath)::openInputStream;

        MessageKeyIndex importedKeysIndex;
        try (InputStream in = importedKeysFile.openInputStream())
        {
          importedKeysIndex = MessageKeyIndex.readFrom(in);
        }

        importedKeysIndex.getImportedModules().stream().forEach(importedModulesToLoad::push);

        importedMessageKeysPerModule.put(moduleName, importedKeysIndex);

        if (!messageFormatSupport.supportsFormat(importedKeysIndex.getMessageFormat()))
        {
          String msg =
              "Message format '%s' used by imported message keys '%s' [%s] not supported by message format '%s'.";
          msg = String.format(msg, importedKeysIndex.getMessageFormat(), moduleName, keysFilePath,
              projectConf.getMessageFormat());
          processingEnv.getMessager().printMessage(Kind.ERROR, msg);
        }
      }
      catch (FileNotFoundException | NoSuchFileException ex)
      {
        String msg = "Imported message keys '%s' [%s] not found.";
        msg = String.format(msg, moduleName, keysFilePath);
        processingEnv.getMessager().printMessage(Kind.ERROR, msg);
        LOG.log(Level.SEVERE, msg, ex);
        messageKeysIncomplete = true;
        importsLoaded = false;
      }
      catch (IOException | PersistableDataException | RuntimeException ex)
      {
        // in Eclipse a generic ResourceException is wrapped in an IOException
        if (ex.getMessage() != null && ex.getMessage().contains("does not exist"))
        {
          String msg = "Imported message keys '%s' [%s] not found.";
          msg = String.format(msg, moduleName, keysFilePath);
          processingEnv.getMessager().printMessage(Kind.ERROR, msg);
          LOG.log(Level.SEVERE, msg, ex);
        }
        else
        {
          String msg = "Failed to import message keys '%s' [%s].";
          msg = String.format(msg, moduleName, keysFilePath);
          processingEnv.getMessager().printMessage(Kind.ERROR, msg);
          LOG.log(Level.SEVERE, msg, ex);
        }
        messageKeysIncomplete = true;
        importsLoaded = false;
      }
    }
  }

  /**
   * Check for declared message keys with no bundle mapping. Every module needs to provide bundle
   * mappings for the keys it declares; unless it doesn't provide message bundles.
   *
   * @return true when all checked message keys could be mapped
   */
  private boolean checkUnmappedKeys()
  {
    if (projectConf.getBundleDir() == null)
      return true;

    AtomicBoolean containsUnmapped = new AtomicBoolean(false);

    messageKeysInProject.getKeys().forEach(key ->
    {
      if (!projectConf.toTargetBundleName(key.getCode(), true).isPresent())
      {
        containsUnmapped.set(true);

        String msg = "No message bundle mapping found for declared message key '%s'.";
        msg = String.format(msg, key.getCode());

        TypeElement sourceTypeElement =
            processingEnv.getElementUtils().getTypeElement(key.getSourceType());

        Element errorLocationElement = null;
        if (sourceTypeElement != null)
        {
          errorLocationElement = switch (key.getSourceElementType())
          {
            case FIELD -> LangModelUtils.getDeclaredFields(sourceTypeElement).stream()
                .filter(ve -> ve.getSimpleName().toString().equals(key.getSourceElementName()))
                .findFirst().orElse(null);

            case METHOD -> LangModelUtils.getDeclaredMethods(sourceTypeElement).stream()
                .filter(ee -> ee.getSimpleName().toString().equals(key.getSourceElementName()))
                .findFirst().orElse(null);

            // TYPE
            default -> sourceTypeElement;
          };
        }

        if (errorLocationElement == null)
          errorLocationElement = sourceTypeElement;

        processingEnv.getMessager().printMessage(Kind.ERROR, msg, errorLocationElement);
      }
    });

    return !containsUnmapped.get();
  }

  private void checkUndeclaredKeys()
  {
    if (messageKeysIncomplete || projectConf.getUndeclaredKeyPolicy() == UndeclaredKeyPolicy.NONE)
      return;

    // the declared keys are the ones from this module ...
    Set<String> declaredKeys =
        messageKeysInProject.getKeys().map(MessageKeyWithSourceLocation::getCode).collect(toSet());

    // ... and the imported ones
    for (String moduleName : listImportedModules())
    {
      MessageKeyIndex messageKeyIndex = importedMessageKeysPerModule.get(moduleName);
      messageKeyIndex.getKeys().map(MessageKeyWithSourceLocation::getCode)
          .forEach(declaredKeys::add);
    }


    for (BundleFile<Path, IOException> bundleFile : messageBundleManager.getBundleFiles())
    {
      Set<String> keys = bundleFile.getKeys();

      for (String key : keys)
      {
        if (!declaredKeys.contains(key))
        {
          if (bundleFile.isMessageEmpty(key))
          {
            bundleFile.removeMessage(key);
            continue;
          }

          if (projectConf.getUndeclaredKeyPolicy() == UndeclaredKeyPolicy.ADD_COMMENT_AND_WARN)
            bundleFile.setComment(key, projectConf.getUndeclaredKeyComment());

          if (projectConf.getUndeclaredKeyPolicy() == UndeclaredKeyPolicy.ADD_COMMENT_AND_WARN
              || projectConf.getUndeclaredKeyPolicy() == UndeclaredKeyPolicy.WARN
              || projectConf.getUndeclaredKeyPolicy() == UndeclaredKeyPolicy.FAIL)
          {
            String msg = "Undeclared message key '%s' in message bundle file '%s'.";
            msg = String.format(msg, key, bundleFile.getDisplayPath());

            Kind kind;
            if (projectConf.getUndeclaredKeyPolicy() == UndeclaredKeyPolicy.FAIL)
              kind = Kind.ERROR;
            else
              kind = Kind.WARNING;

            processingEnv.getMessager().printMessage(kind, msg);
          }

          if (projectConf.getUndeclaredKeyPolicy() == UndeclaredKeyPolicy.REMOVE)
          {
            bundleFile.removeMessage(key);

            String msg = "Undeclared message key '%s' in message bundle file '%s' will be removed.";
            msg = String.format(msg, key, bundleFile.getDisplayPath());
            processingEnv.getMessager().printMessage(Kind.WARNING, msg);
          }
        }
        else
        {
          if (projectConf.getUndeclaredKeyPolicy() == UndeclaredKeyPolicy.ADD_COMMENT_AND_WARN
              && projectConf.getUndeclaredKeyComment().equals(bundleFile.getComment(key)))
          {
            bundleFile.setComment(key, null);
          }
        }
      }
    }
  }

  private void checkBundleMismatches()
  {
    if (projectConf.getBundleMismatchPolicy() == BundleMismatchPolicy.NONE)
      return;

    for (BundleFile<Path, IOException> bundleFile : messageBundleManager.getBundleFiles())
    {
      for (String key : bundleFile.getKeys())
      {
        if (processedMessageKeys.containsKey(key))
        {
          String expectedBundleName = projectConf.toTargetBundleName(key).orElse(null);
          if (expectedBundleName == null)
            continue;

          BundleFile<Path, IOException> expectedBundleFile =
              messageBundleManager.getBundleFile(expectedBundleName, bundleFile.getLocale(), true);

          if (bundleFile.getBundleName().equals(expectedBundleName))
            continue;

          if (projectConf.getBundleMismatchPolicy() == BundleMismatchPolicy.MOVE)
          {
            String message = bundleFile.getMessage(key);
            String comment = bundleFile.getComment(key);

            if (expectedBundleFile.hasMessageKey(key))
            {
              // target bundle file already contains message key

              String messageInTarget = expectedBundleFile.getMessage(key);
              if (isEmpty(messageInTarget))
              {
                // message in target bundle is empty -> overwrite
                String msg = "Message key '%s' does not match bundle file '%s'. Moving to '%s'.";
                msg = String.format(msg, key, bundleFile.getDisplayPath(),
                    expectedBundleFile.getDisplayPath());
                processingEnv.getMessager().printMessage(Kind.WARNING, msg);

                bundleFile.removeMessage(key);
                expectedBundleFile.setMessage(key, message);
                expectedBundleFile.setComment(key, comment);
              }
              else if (isEmpty(message))
              {
                // misplaced key's message is empty -> removing from source bundle is enough
                String msg = "Message key '%s' does not match bundle file '%s'. Message is empty,"
                    + " so just removing.";
                msg = String.format(msg, key, bundleFile.getDisplayPath());
                processingEnv.getMessager().printMessage(Kind.WARNING, msg);

                bundleFile.removeMessage(key);
              }
              else if (message.equals(messageInTarget))
              {
                // both message are the same -> removing from source bundle is enough
                String msg = "Message key '%s' does not match bundle file '%s'. Message in '%s' is"
                    + " already the same, so just removing.";
                msg = String.format(msg, key, bundleFile.getDisplayPath(),
                    expectedBundleFile.getDisplayPath());
                processingEnv.getMessager().printMessage(Kind.WARNING, msg);

                bundleFile.removeMessage(key);
              }
              else
              {
                // cannot reconcile automatically -> show error
                String msg = "Message key '%s' does not match bundle file '%s'. However, the"
                    + " message key already exists in '%s' with a different message. You need to"
                    + " manually reconcile this.";
                msg = String.format(msg, key, bundleFile.getDisplayPath(),
                    expectedBundleFile.getDisplayPath());
                processingEnv.getMessager().printMessage(Kind.WARNING, msg);
              }
            }
            else
            {
              String msg = "Message key '%s' does not match bundle file '%s'. Moving to '%s'.";
              msg = String.format(msg, key, bundleFile.getDisplayPath(),
                  expectedBundleFile.getDisplayPath());
              processingEnv.getMessager().printMessage(Kind.WARNING, msg);

              bundleFile.removeMessage(key);
              expectedBundleFile.setMessage(key, message);
              expectedBundleFile.setComment(key, comment);
            }
          }
          else if (projectConf.getBundleMismatchPolicy() == BundleMismatchPolicy.WARN)
          {
            String msg = "Message key '%s' does not match bundle file '%s'.";
            msg = String.format(msg, key, bundleFile.getDisplayPath());
            processingEnv.getMessager().printMessage(Kind.WARNING, msg);
          }
        }
      }
    }
  }

  /**
   * On a clean build, messages for all local and imported message keys will be checked. On
   * incremental / non-clean builds only message stubs are created where necessary.
   *
   * updates: messagesInProject (for clean builds)
   */
  private void checkMessages()
  {
    if (cleanBuild)
    {
      messagesInProject = HashMultimap.create();
    }

    // if the key is missing completely for the locale
    ListMultimap<Locale, Pair<MessageKeyWithArgs, Element>> missingKeys =
        ArrayListMultimap.create();
    // if the message is missing or empty (no matter whether the key is present)
    ListMultimap<Locale, Pair<MessageKeyWithArgs, Element>> missingMessages =
        ArrayListMultimap.create();


    // check local message keys
    for (ProcessedMessageKey pmk : processedMessageKeys.values())
    {
      Set<Locale> requiredLocales = getRequiredLocale(pmk.getCode());

      Pair<Boolean, Boolean> resultForRoot =
          checkMessageForLocale(pmk, Locale.ROOT, pmk.getSourceElement());
      boolean keyFoundForRoot = resultForRoot.getLeft();
      boolean messageFoundForRoot = resultForRoot.getRight();

      if (keyFoundForRoot && messagesInProject != null)
        messagesInProject.put(Locale.ROOT, pmk);

      if (keyFoundForRoot && !messageFoundForRoot)
        missingMessages.put(Locale.ROOT, Pair.of(pmk, pmk.getSourceElement()));

      for (Locale locale : projectConf.getTargetLocales())
      {
        boolean requiredForLocale = requiredLocales.contains(locale);

        Pair<Boolean, Boolean> resultForLocale =
            checkMessageForLocale(pmk, locale, pmk.getSourceElement());
        boolean keyFoundForLocale = resultForLocale.getLeft();
        boolean messageFoundForLocale = resultForLocale.getRight();

        if (keyFoundForLocale && messagesInProject != null)
          messagesInProject.put(locale, pmk);

        // key with empty message is always dodgy
        if (keyFoundForLocale && !messageFoundForLocale)
          missingMessages.put(locale, Pair.of(pmk, pmk.getSourceElement()));

        if (!keyFoundForRoot && !keyFoundForLocale && requiredForLocale)
        {
          missingKeys.put(locale, Pair.of(pmk, pmk.getSourceElement()));
          missingMessages.put(locale, Pair.of(pmk, pmk.getSourceElement()));
        }
      }
    }

    // check imported message keys
    if (cleanBuild && importsLoaded)
    {
      for (String moduleName : listImportedModules())
      {
        MessageKeyIndex importedKeys = importedMessageKeysPerModule.get(moduleName);

        // nothing to check if imported module already provides all target locales of this module
        List<Locale> missingLocales = new ArrayList<>(projectConf.getTargetLocales());
        missingLocales.removeAll(importedKeys.getTargetLocales());
        if (missingLocales.isEmpty())
          continue;

        importedKeys.getKeys().forEach(key ->
        {
          Pair<Boolean, Boolean> resultForRoot = checkMessageForLocale(key, Locale.ROOT, null);
          boolean keyFoundForRoot = resultForRoot.getLeft();
          boolean messageFoundForRoot = resultForRoot.getRight();

          if (keyFoundForRoot && !messageFoundForRoot)
          {
            missingMessages.put(Locale.ROOT, Pair.of(key, null));
          }

          Set<Locale> requiredLocales = getRequiredLocale(key.getCode());

          for (Locale locale : missingLocales)
          {
            boolean requiredForLocale = requiredLocales.contains(locale);

            Pair<Boolean, Boolean> resultForLocale = checkMessageForLocale(key, locale, null);
            boolean keyFoundForLocale = resultForLocale.getLeft();
            boolean messageFoundForLocale = resultForLocale.getRight();

            // key with empty message is always dodgy
            if (keyFoundForLocale && !messageFoundForLocale)
              missingMessages.put(locale, Pair.of(key, null));

            if (!key.isDefaultMessageAvailable() && !keyFoundForRoot && !keyFoundForLocale
                && requiredForLocale)
            {
              missingKeys.put(locale, Pair.of(key, null));
              missingMessages.put(locale, Pair.of(key, null));
            }
          }
        });
      }
    }


    if (!missingKeys.isEmpty())
    {
      if (projectConf.getMissingMessagePolicy() == MissingMessagePolicy.ADD_AND_WARN
          || projectConf.getMissingMessagePolicy() == MissingMessagePolicy.ADD)
      {
        missingKeys.forEach((locale, pair) ->
        {
          MessageKeyWithArgs messageKey = pair.getLeft();
          Element sourceElement = pair.getRight();

          String targetBundleName =
              projectConf.toTargetBundleName(messageKey.getCode()).orElse(null);
          if (targetBundleName == null)
            return;

          BundleFile<Path, IOException> bundleFile =
              messageBundleManager.getBundleFile(targetBundleName, locale, true);
          bundleFile.setMessage(messageKey.getCode(), "");

          updateMessageArgDocComment(messageKey, bundleFile);

          String msg = "%s:%s [%s] Empty message stub created.";
          msg = String.format(msg, messageKey.getCode(), locale, bundleFile.getDisplayPath());
          processingEnv.getMessager().printMessage(Kind.NOTE, msg, sourceElement);
        });
      }
    }

    if (cleanBuild)
    {
      Kind messageKind = switch (projectConf.getMissingMessagePolicy())
      {
        case FAIL -> Kind.ERROR;
        case ADD_AND_WARN, WARN -> Kind.WARNING;
        case ADD, NONE -> null;
      };

      if (!missingMessages.isEmpty() && messageKind != null)
      {
        missingMessages.forEach((locale, pair) ->
        {
          MessageKeyWithArgs messageKey = pair.getLeft();
          Element sourceElement = pair.getRight();

          String targetBundleName =
              projectConf.toTargetBundleName(messageKey.getCode()).orElse(null);
          if (targetBundleName == null)
            return;

          BundleFile<Path, IOException> bundleFile =
              messageBundleManager.getBundleFile(targetBundleName, locale, true);

          String msg = "%s:%s [%s] Message is missing.";
          msg = String.format(msg, messageKey.getCode(), locale, bundleFile.getDisplayPath());
          processingEnv.getMessager().printMessage(messageKind, msg, sourceElement);
        });
      }
    }
  }

  private Set<Locale> getRequiredLocale(String messageKeyCode)
  {
    Map<String, MessageKeyIndex> importedIndexes = new HashMap<>();
    for (String importedModule : messageKeysInProject.getImportedModules())
    {
      MessageKeyIndex importedIndex = importedMessageKeysPerModule.get(importedModule);
      if (importedIndex == null)
        throw new IllegalStateException("Imported module not found: " + importedModule);

      importedIndexes.put(importedModule, importedIndex);
    }

    return MessageValidationUtils.getRequiredLocales(messageKeyCode, messageKeysInProject,
        importedIndexes, (module, code) ->
        {
          return ((MessageKeyIndex) module).getKey(code) != null;
        });
  }

  private Pair<Boolean, Boolean> checkMessageForLocale(MessageKeyWithArgs messageKey, Locale locale,
      Element sourceElement)
  {
    // check in target bundle first
    String targetBundleName = projectConf.toTargetBundleName(messageKey.getCode()).orElse(null);
    if (targetBundleName == null)
      return Pair.of(false, false);

    boolean checkNonTargetBundles =
        projectConf.getBundleMismatchPolicy() != BundleMismatchPolicy.MOVE;

    boolean keyFound = false;
    boolean messageFound = false;

    BundleFile<Path, IOException> targetBundleFile =
        messageBundleManager.getBundleFile(targetBundleName, locale);

    if (targetBundleFile != null && targetBundleFile.hasMessageKey(messageKey.getCode()))
    {
      keyFound = true;

      if (!targetBundleFile.isMessageEmpty(messageKey.getCode()))
      {
        messageFound = true;
        checkMessageArgs(messageKey, locale, targetBundleFile, sourceElement);
      }

      updateMessageArgDocComment(messageKey, targetBundleFile);
    }

    else if (checkNonTargetBundles)
    {
      // also check other bundle files
      for (BundleFile<Path, IOException> bundleFile : messageBundleManager.getBundleFiles(locale))
      {
        if (bundleFile.getBundleName().equals(targetBundleName))
          // already checked
          continue;

        if (bundleFile.hasMessageKey(messageKey.getCode()))
        {
          keyFound = true;

          if (!bundleFile.isMessageEmpty(messageKey.getCode()))
          {
            messageFound = true;
            checkMessageArgs(messageKey, locale, bundleFile, sourceElement);
          }

          updateMessageArgDocComment(messageKey, bundleFile);

          break;
        }
      }
    }

    return Pair.of(keyFound, messageFound);
  }

  private void checkMessageArgs(MessageKeyWithArgs messageKey, Locale locale,
      BundleFile<Path, IOException> bundleFile, Element sourceElement)
  {
    Kind severity = switch (projectConf.getMessageArgPolicy())
    {
      case FAIL -> Kind.ERROR;
      case WARN -> Kind.WARNING;
      case NONE -> null;
    };

    if (severity == null)
      return;


    String message = bundleFile.getMessage(messageKey.getCode());

    List<String> errors = messageFormatSupport.checkMessage(message, messageKey.getArgTypes(),
        messageKey.getArgNames(), (typeName, allowedTypeNames) ->
        {
          if (PRIMITIVE_TYPES.isEmpty())
          {
            for (TypeKind typeKind : TypeKind.values())
            {
              if (!typeKind.isPrimitive())
                continue;

              PRIMITIVE_TYPES.put(typeKind.name().toLowerCase(Locale.ENGLISH),
                  processingEnv.getTypeUtils().getPrimitiveType(typeKind));
            }
          }

          if (allowedTypeNames.contains(typeName))
            return true;

          TypeMirror argTypeMirror;
          TypeElement argTypeElement = processingEnv.getElementUtils().getTypeElement(typeName);
          if (argTypeElement == null)
            argTypeMirror = PRIMITIVE_TYPES.get(typeName);
          else
            argTypeMirror = argTypeElement.asType();

          // no primitive type and not on the class path?
          if (argTypeMirror == null)
            return false;


          for (String expectedArgType : allowedTypeNames)
          {
            TypeElement expectedArgTypeElement =
                processingEnv.getElementUtils().getTypeElement(expectedArgType);
            if (expectedArgTypeElement == null)
              continue;

            if (processingEnv.getTypeUtils().isAssignable(argTypeMirror,
                expectedArgTypeElement.asType()))
              return true;
          }
          return false;
        });

    for (String error : errors)
    {
      String msg = "%s:%s [%s] %s";
      msg = String.format(msg, messageKey, locale, bundleFile.getDisplayPath(), error);
      processingEnv.getMessager().printMessage(severity, msg, sourceElement);
    }
  }

  private void updateMessageArgDocComment(MessageKeyWithArgs messageKey,
      BundleFile<Path, IOException> bundleFile)
  {
    if (projectConf.getMissingMessagePolicy() == MissingMessagePolicy.ADD_AND_WARN
        || projectConf.getMissingMessagePolicy() == MissingMessagePolicy.ADD)
    {
      String comment = messageFormatSupport.createMessageBundleComment(messageKey);
      bundleFile.setComment(messageKey.getCode(), comment);
    }
  }


  private void addMsgKey(Element sourceElement, TypeElement owningTypeElement, String prefix,
      String localPart)
  {
    addMsgKey(sourceElement, owningTypeElement, prefix, null, localPart, false);
  }

  private void addMsgKey(Element sourceElement, TypeElement owningTypeElement, String prefix,
      String localPart, boolean skipDuplicate)
  {
    addMsgKey(sourceElement, owningTypeElement, prefix, null, localPart, skipDuplicate);
  }

  private void addMsgKey(Element sourceElement, TypeElement owningTypeElement, String prefix,
      String localPartPrefix, String localPart, boolean skipDuplicate)
  {
    String fullLocalPart;
    String[] argTypes = null;
    String[] argNames = null;

    // split off the message arguments from the local part
    if (localPart != null && (localPart.contains("->") || localPart.contains(",")))
    {
      List<String> argsList;
      if (localPart.contains("->"))
      {
        argsList = Arrays.asList(splitPreserveAllTokens(substringAfter(localPart, "->"), ","));
        localPart = trim(substringBefore(localPart, "->"));
      }
      else // if (localPart.contains(","))
      {
        String[] tokens = splitPreserveAllTokens(localPart, ",");
        localPart = tokens[0].trim();
        argsList = Arrays.asList(tokens).subList(1, tokens.length);
      }

      List<String> argTypesList = new ArrayList<>();
      List<String> argNamesList = new ArrayList<>();

      for (String arg : argsList)
      {
        arg = arg.trim();

        String argName = null;
        String argTypeString = null;
        if (arg.contains(":"))
        {
          String[] argTokens = splitPreserveAllTokens(arg, ":", 2);
          argName = argTokens[0].trim();
          argTypeString = argTokens[1].trim();
        }

        if (argName == null || argTypeString == null)
        {
          String msg = "Illegal format for message arguments.";
          msg = String.format(msg);
          processingEnv.getMessager().printMessage(Kind.ERROR, msg, sourceElement);
          continue;
        }
        if (argName.isEmpty())
        {
          String msg = "Message argument name must not be empty.";
          msg = String.format(msg);
          processingEnv.getMessager().printMessage(Kind.ERROR, msg, sourceElement);
          continue;
        }

        if (!argTypeString.contains("."))
        {
          String argType = projectConf.getTypeAbbreviations().get(argTypeString);
          if (argType == null)
            argType = DEFAULT_TYPE_ABBREVIATIONS.get(argTypeString);
          if (argType != null)
            argTypeString = argType;
        }

        argNamesList.add(argName);
        argTypesList.add(argTypeString);
      }

      argNames = argNamesList.toArray(new String[argNamesList.size()]);
      argTypes = argTypesList.toArray(new String[argTypesList.size()]);
    }

    if (isNotEmpty(localPartPrefix))
    {
      if (isNotEmpty(localPart))
        fullLocalPart = localPartPrefix + "_" + localPart;
      else
        fullLocalPart = localPartPrefix;
    }
    else
    {
      if (isNotEmpty(localPart))
        fullLocalPart = localPart;
      else
        fullLocalPart = null;
    }

    if (fullLocalPart == null)
    {
      String msg = "Local part of message key must not be empty.";
      msg = String.format(msg, fullLocalPart);
      processingEnv.getMessager().printMessage(Kind.ERROR, msg, sourceElement);
      return;
    }
    if (!SourceVersion.isIdentifier(fullLocalPart))
    {
      String msg;
      if (SourceVersion.isName(fullLocalPart))
      {
        msg = "Local part '%s' of message key is not a valid Java identifier.";
      }
      else
      {
        msg = "Local part '%s' of message key is a Java keyword. Keywords are not allowed.";
      }
      msg = String.format(msg, fullLocalPart);
      processingEnv.getMessager().printMessage(Kind.ERROR, msg, sourceElement);
      return;
    }


    ProcessedMessageKey key = new ProcessedMessageKey(sourceElement, owningTypeElement, prefix,
        fullLocalPart, argTypes, argNames);

    if (processedMessageKeys.containsKey(key.getCode()))
    {
      if (!skipDuplicate)
      {
        String msg = "Message key '%s' already declared.";
        msg = String.format(msg, key.getCode());
        processingEnv.getMessager().printMessage(Kind.ERROR, msg, sourceElement);
      }
    }
    else
    {
      messageKeysPerType.put(owningTypeElement, key);
      processedMessageKeys.put(key.getCode(), key);
    }
  }

  private void addMsgKeys(Element sourceElement, TypeElement owningTypeElement, String prefix,
      Collection<String> localParts)
  {
    for (String localPart : localParts)
    {
      addMsgKey(sourceElement, owningTypeElement, prefix, localPart);
    }
  }

  private void addMsgKeys(Element sourceElement, TypeElement owningTypeElement, String prefix,
      String localPartPrefix, Collection<String> localParts)
  {
    addMsgKeys(sourceElement, owningTypeElement, prefix, localPartPrefix, localParts, false);
  }

  private void addMsgKeys(Element sourceElement, TypeElement owningTypeElement, String prefix,
      String localPartPrefix, Collection<String> localParts, boolean skipDuplicates)
  {
    for (String localPart : localParts)
    {
      addMsgKey(sourceElement, owningTypeElement, prefix, localPartPrefix, localPart,
          skipDuplicates);
    }
  }

  private Set<ProcessedMessageKey> getMsgKeys(TypeElement owningTypeElement)
  {
    return messageKeysPerType.get(owningTypeElement);
  }


  private ClassName getMessageConstantsClassName(TypeElement sourceTypeElement)
  {
    String packageName = getCodegenPackageName(sourceTypeElement);
    String simpleClassName = MessageCodegenUtils
        .getMessageConstantsSimpleTypeNameFor(getMessageOwnerSimpleClassName(sourceTypeElement));
    return ClassName.get(packageName, simpleClassName);
  }

  private String getMessageConstantsClassNameString(TypeElement sourceTypeElement)
  {
    String packageName = getCodegenPackageName(sourceTypeElement);
    String simpleClassName = MessageCodegenUtils
        .getMessageConstantsSimpleTypeNameFor(getMessageOwnerSimpleClassName(sourceTypeElement));
    return packageName + "." + simpleClassName;
  }

  private ClassName getMessageProxiesClassName(TypeElement sourceTypeElement)
  {
    String packageName = getCodegenPackageName(sourceTypeElement);
    String simpleClassName = MessageCodegenUtils
        .getMessageProxiesSimpleTypeNameFor(getMessageOwnerSimpleClassName(sourceTypeElement));
    return ClassName.get(packageName, simpleClassName);
  }

  // private String getMessageProxiesClassNameString(TypeElement sourceTypeElement)
  // {
  // String packageName = getCodegenPackageName(sourceTypeElement);
  // String simpleClassName = MessageCodegenUtils
  // .getMessageProxiesSimpleTypeNameFor(getMessageOwnerSimpleClassName(sourceTypeElement));
  // return packageName + "." + simpleClassName;
  // }

  private String getCodegenPackageName(TypeElement sourceTypeElement)
  {
    String packageName = codegenPackagesPerType.get(sourceTypeElement);

    if (packageName == null)
    {
      String sourceTypeName = sourceTypeElement.getQualifiedName().toString();

      packageName = messageKeysInProject.getCodegenPackage(sourceTypeName);

      /*
       * This won't work in Eclipse as loading the imported modules from CLASS_PATH is not
       * supported. loadImportedModules() would also have to be called earlier (before this method
       * is called) but there's no point as it won't work in Eclipse anyway.
       *
       * In essence it's currently not supported to extend a source type in module C when the source
       * type itself is in module A and message key constants for it have been created in module B
       * via @MessagesFor.
       */
      for (MessageKeyIndex mki : importedMessageKeysPerModule.values())
      {
        packageName = mki.getCodegenPackage(sourceTypeName);
        if (packageName != null)
          break;
      }
    }

    if (packageName == null)
      packageName = LangModelUtils.getPackageName(sourceTypeElement);

    return packageName;
  }

  /* package */ static String getMessageOwnerSimpleClassName(TypeElement sourceTypeElement)
  {
    return LangModelUtils.getTypeNesting(sourceTypeElement).stream()
        .map(te -> te.getSimpleName().toString()).collect(joining("."));
  }


  private boolean hasMessageConstants(TypeElement sourceTypeElement)
  {
    String sourceTypeName = sourceTypeElement.getQualifiedName().toString();

    if (hasMessageConstantsCache.contains(sourceTypeName))
      return true;

    /*
     * check if message key constants simply exist in the classpath (e.g. in a dependency created
     * with MessagesFor)
     */
    if (processingEnv.getElementUtils()
        .getTypeElement(getMessageConstantsClassNameString(sourceTypeElement)) != null)
    {
      hasMessageConstantsCache.add(sourceTypeName);
      return true;
    }

    return false;
  }

  // private boolean hasMessageProxies(TypeElement sourceTypeElement)
  // {
  // String sourceTypeName = sourceTypeElement.getQualifiedName().toString();
  //
  // if (hasMessageProxiesCache.contains(sourceTypeName))
  // return true;
  //
  // // if message key constants simply exist in the classpath (e.g. in a dependency created with
  // // MessagesFor)
  // if (processingEnv.getElementUtils()
  // .getTypeElement(getMessageProxiesClassNameString(sourceTypeElement)) != null)
  // {
  // hasMessageProxiesCache.add(sourceTypeName);
  // return true;
  // }
  //
  // return false;
  // }

  private List<String> listImportedModules()
  {
    Set<String> foundModules = new HashSet<>();

    List<String> importedModules = new ArrayList<>();

    Deque<String> stack = new ArrayDeque<>(projectConf.getImports());
    while (!stack.isEmpty())
    {
      String moduleName = stack.pop();

      // avoid duplicates
      if (!foundModules.add(moduleName))
        continue;

      importedModules.add(moduleName);

      MessageKeyIndex messageKeyIndex = importedMessageKeysPerModule.get(moduleName);
      messageKeyIndex.getImportedModules().stream().forEach(stack::push);
    }

    return importedModules;
  }


  @FunctionalInterface
  public interface InputStreamSupplier
  {

    InputStream openInputStream()
      throws IOException;

  }

}
