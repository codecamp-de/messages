package de.codecamp.messages.processor;


import static com.google.testing.compile.CompilationSubject.assertThat;
import static com.google.testing.compile.Compiler.javac;
import static org.assertj.core.api.Assertions.assertThat;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.body.FieldDeclaration;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.expr.ObjectCreationExpr;
import com.github.javaparser.ast.type.Type;
import com.google.testing.compile.Compilation;
import com.google.testing.compile.JavaFileObjects;
import de.codecamp.messages.MessageKey;
import de.codecamp.messages.MessageKeyWithArgs;
import de.codecamp.messages.codegen.MessageCodegenUtils;
import de.codecamp.messages.shared.conf.MissingMessagePolicy;
import de.codecamp.messages.shared.conf.ProjectConf;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;
import javax.tools.JavaFileObject;
import org.junit.jupiter.api.Test;


class MessageKeyProcessorTests
{

  @Test
  void keyGenerationBasics()
    throws IOException
  {
    Compilation compilation = buildTestSource("Basics");
    assertThat(compilation).succeededWithoutWarnings();
    TestBuildResults testBuildResults = getTestBuildResults(compilation, "Basics");

    assertThat(testBuildResults.keyLocalParts).containsExactlyInAnyOrder("key1", "key2", "field",
        "field2_key1", "field2_key2", "field3", "field3_key1", "field3_key2", "key3", "key4");
  }

  @Test
  void keyGenerationBasicsNotOnType()
    throws IOException
  {
    Compilation compilation = buildTestSource("BasicsNotOnType");
    assertThat(compilation).succeededWithoutWarnings();
    TestBuildResults testBuildResults = getTestBuildResults(compilation, "BasicsNotOnType");

    assertThat(testBuildResults.keyLocalParts).containsExactlyInAnyOrder("field", "field2_key1",
        "field2_key2", "field3", "field3_key1", "field3_key2", "key3", "key4");
  }

  @Test
  void keyGenerationNested()
    throws IOException
  {
    Compilation compilation = buildTestSource("Nested");
    assertThat(compilation).succeededWithoutWarnings();
    TestBuildResults testBuildResults = getTestBuildResults(compilation, "Nested_Inner");

    assertThat(testBuildResults.keyLocalParts).containsExactlyInAnyOrder("key1", "key2", "field",
        "field2_key1", "field2_key2", "field3", "field3_key1", "field3_key2", "key3", "key4");
  }

  @Test
  void keyGenerationEnum()
    throws IOException
  {
    Compilation compilation = buildTestSource("Enum", "Enum2");
    assertThat(compilation).succeededWithoutWarnings();

    TestBuildResults testBuildResults = getTestBuildResults(compilation, "Enum");
    assertThat(testBuildResults.keyLocalParts).containsExactlyInAnyOrder("ENUM_CONSTANT1",
        "ENUM_CONSTANT2", "ENUM_CONSTANT2_addKey1", "ENUM_CONSTANT2_addKey2");

    testBuildResults = getTestBuildResults(compilation, "Enum2");
    assertThat(testBuildResults.keyLocalParts).containsExactlyInAnyOrder("ENUM_CONSTANT2_addKey1",
        "ENUM_CONSTANT2_addKey2");
  }

  @Test
  void keyGenerationMetaAnnotation()
    throws IOException
  {
    Compilation compilation = buildTestSource("Meta");
    assertThat(compilation).succeededWithoutWarnings();
    TestBuildResults testBuildResults = getTestBuildResults(compilation, "Meta");

    assertThat(testBuildResults.keyLocalParts).containsExactlyInAnyOrder("fromMeta", "fromMeta2",
        "field_fromMeta", "field_fromMeta2");
  }

  @Test
  void beanPropertyKeyGeneration()
    throws IOException
  {
    Compilation compilation = buildTestSource("Bean");
    assertThat(compilation).succeededWithoutWarnings();
    TestBuildResults testBuildResults = getTestBuildResults(compilation, "Bean");

    assertThat(testBuildResults.keyLocalParts).containsExactlyInAnyOrder(MessageKey.TYPE_KEY_SUFFIX,
        "addKey", "booleanProperty", "property1", "property3", "property3_addKey");
  }

  @Test
  void keyGenerationMessagesForBasics()
    throws IOException
  {
    Compilation compilation = buildTestSource("ExternallyBasic");
    assertThat(compilation).succeededWithoutWarnings();

    TestBuildResults testBuildResults = getTestBuildResults(compilation, "Type");
    assertThat(testBuildResults.keyLocalParts).containsExactlyInAnyOrder("property1", "property3");

    testBuildResults = getTestBuildResults(compilation, "Enum");
    assertThat(testBuildResults.keyLocalParts).containsExactlyInAnyOrder("ENUM_CONSTANT1",
        "ENUM_CONSTANT3");
  }

  @Test
  void keyGenerationMessagesForHierarchy()
    throws IOException
  {
    Compilation compilation = buildTestSource("ExternallyHierarchy");
    assertThat(compilation).succeededWithoutWarnings();

    TestBuildResults testBuildResults = getTestBuildResults(compilation, "TypeTop");
    assertThat(testBuildResults.keyLocalParts).containsExactlyInAnyOrder("property1",
        "propertyOverride");

    testBuildResults = getTestBuildResults(compilation, "ExternallyHierarchy");
    assertThat(testBuildResults.keyLocalParts).containsExactlyInAnyOrder("propertyOverride_addKey",
        "propertyOverride2", "property2", "property3");
  }

  @Test
  void keyGenerationMessagesForHierarchy2()
    throws IOException
  {
    Compilation compilation = buildTestSource("ExternallyHierarchy2");
    assertThat(compilation).succeededWithoutWarnings();

    TestBuildResults testBuildResults = getTestBuildResults(compilation, "TypeTop");
    assertThat(testBuildResults.keyLocalParts).containsExactlyInAnyOrder("property1");

    testBuildResults = getTestBuildResults(compilation, "TypeMiddle");
    assertThat(testBuildResults.keyLocalParts).containsExactlyInAnyOrder("property2");

    testBuildResults = getTestBuildResults(compilation, "ExternallyHierarchy2");
    assertThat(testBuildResults.keyLocalParts).containsExactlyInAnyOrder("property3");
  }

  @Test
  void keyGenerationInlinedMultipleSources()
    throws IOException
  {
    /*
     * Inheriting a property from multiple classes and/or interfaces should not trigger the
     * duplicate key detection.
     */
    Compilation compilation = buildTestSource("InlinedMultipleSources");
    assertThat(compilation).succeededWithoutWarnings();

    TestBuildResults testBuildResults = getTestBuildResults(compilation, "InlinedMultipleSources");
    assertThat(testBuildResults.keyLocalParts).containsExactlyInAnyOrder("id");
  }

  @Test
  void keyGenerationWithArguments()
    throws IOException
  {
    Compilation compilation = buildTestSource("Arguments");
    assertThat(compilation).succeededWithoutWarnings();

    TestBuildResults testBuildResults = getTestBuildResults(compilation, "Arguments");
    assertThat(testBuildResults.keyLocalParts).containsExactlyInAnyOrder("field1", "field2",
        "field3");

    assertThat(testBuildResults.keys.get("field1").getArgNames()).isNullOrEmpty();
    assertThat(testBuildResults.keys.get("field1").getArgTypes()).isNullOrEmpty();

    assertThat(testBuildResults.keys.get("field2").getArgNames()).containsExactly("firstName",
        "lastName", "age");
    assertThat(testBuildResults.keys.get("field2").getArgTypes())
        .containsExactly(String.class.getName(), String.class.getName(), int.class.getName());

    assertThat(testBuildResults.keys.get("field3").getArgNames()).containsExactly("firstName",
        "lastName", "age");
    assertThat(testBuildResults.keys.get("field3").getArgTypes())
        .containsExactly(String.class.getName(), String.class.getName(), int.class.getName());
  }


  private Compilation buildTestSource(String... testClassNames)
  {
    JavaFileObject[] javaFiles =
        Stream.of(testClassNames).map(testClassName -> "/tests/" + testClassName + ".java")
            .map(resourceName -> JavaFileObjects.forResource(getClass().getResource(resourceName)))
            .toArray(JavaFileObject[]::new);

    return javac() //
        .withProcessors(new MessageKeyProcessor()) //
        .withOptions("-A" + ProjectConf.CONF_TARGET_LOCALES + "=en",
            "-A" + ProjectConf.CONF_MISSING_MESSAGE_POLICY + "=" + MissingMessagePolicy.NONE) //
        .compile(javaFiles);
  }

  private TestBuildResults getTestBuildResults(Compilation compilation, String testClassName)
    throws IOException
  {
    String constantsClassName =
        MessageCodegenUtils.getMessageConstantsSimpleTypeNameFor(testClassName);

    assertThat(compilation).generatedSourceFile(constantsClassName);

    CompilationUnit compilationUnit;
    try (InputStream in =
        compilation.generatedSourceFile(constantsClassName).get().openInputStream())
    {
      compilationUnit = StaticJavaParser.parse(in);
    }

    Set<String> keyLocalParts = new HashSet<>();
    Map<String, TestSuiteMessageKey> keys = new HashMap<>();

    compilationUnit.findAll(FieldDeclaration.class).stream()
        .flatMap(field -> field.getVariables().stream()).forEach(vc ->
        {
          List<String> argTypes = null;
          List<String> argNames = null;

          Expression fieldExpression = vc.getInitializer().get();
          if (!(fieldExpression instanceof ObjectCreationExpr))
            return;

          ObjectCreationExpr constructorCallExpr = fieldExpression.asObjectCreationExpr();
          if (!constructorCallExpr.getArgument(2).isNullLiteralExpr())
          {
            argTypes = new ArrayList<>();
            argNames = new ArrayList<>();

            NodeList<Expression> argTypesValues = constructorCallExpr.getArgument(2)
                .asArrayCreationExpr().getInitializer().get().getValues();
            NodeList<Expression> argNamesValues = constructorCallExpr.getArgument(3)
                .asArrayCreationExpr().getInitializer().get().getValues();

            for (int i = 0; i < argTypesValues.size(); i++)
            {
              Type type = argTypesValues.get(i).asClassExpr().getType();
              argTypes.add(type.asString());
              argNames.add(argNamesValues.get(i).asStringLiteralExpr().asString());
            }
          }
          keyLocalParts.add(vc.getNameAsString());
          keys.put(vc.getNameAsString(),
              new TestSuiteMessageKey(vc.getNameAsString(), argTypes, argNames));
        });

    return new TestBuildResults(keyLocalParts, keys);
  }

}

class TestBuildResults
{

  Set<String> keyLocalParts;

  Map<String, TestSuiteMessageKey> keys;


  public TestBuildResults(Set<String> keyLocalParts, Map<String, TestSuiteMessageKey> keys)
  {
    this.keyLocalParts = keyLocalParts;
    this.keys = keys;
  }

}

class TestSuiteMessageKey
  implements
    MessageKeyWithArgs
{

  private final String code;

  private final String[] argTypes;

  private final String[] argNames;


  public TestSuiteMessageKey(String code, List<String> argTypes, List<String> argNames)
  {
    if (code == null)
      throw new IllegalArgumentException("code must not be null");

    this.code = code;

    if (argTypes != null)
      this.argTypes = argTypes.toArray(new String[argTypes.size()]);
    else
      this.argTypes = null;

    if (argNames != null)
      this.argNames = argNames.toArray(new String[argNames.size()]);
    else
      this.argNames = null;
  }


  @Override
  public String getCode()
  {
    return code;
  }

  @Override
  public boolean hasArgs()
  {
    return argNames != null && argNames.length > 0;
  }

  @Override
  public String[] getArgTypes()
  {
    return argTypes;
  }

  @Override
  public String[] getArgNames()
  {
    return argNames;
  }

}
