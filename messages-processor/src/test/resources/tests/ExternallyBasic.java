import de.codecamp.messages.Messages;
import de.codecamp.messages.MessagesFor;


@MessagesFor(type = Type.class, forProperties = true, ignoreImplicitKeys = {"property2"})
@MessagesFor(type = Enum.class, ignoreImplicitKeys = {"ENUM_CONSTANT2"})
public class ExternallyBasic
{
  
}

class Type
{

  public String getProperty1() {
    return null;
  }

  public String getProperty2() {
    return null;
  }

  public String getProperty3() {
    return null;
  }

}

enum Enum
{

  ENUM_CONSTANT1,

  ENUM_CONSTANT2,

  ENUM_CONSTANT3;

}
