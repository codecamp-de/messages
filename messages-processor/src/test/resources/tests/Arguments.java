import de.codecamp.messages.Messages;


public class Arguments
{

  @Messages({""})
  private String field1;

  @Messages({" -> firstName:s, lastName:s, age:i"})
  private String field2;

  @Messages({" -> firstName:java.lang.String, lastName:String, age:int"})
  private String field3;

}
