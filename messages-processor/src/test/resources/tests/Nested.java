import de.codecamp.messages.Messages;


public class Nested
{
  
  // Creates: "key1", "key2"
  @Messages({"key1", "key2"})
  public static class Inner
  {
    
    // Fields:
    // Keys on fields also have the field name prefixed.
    
    // Without explicit keys, the default key is created with the field's name as local part.
    // Creates: "field"
    @Messages
    private String field;
    
    // With explicit key(s), no default key is created.
    // Creates: "field2_key1", "field2_key2"
    @Messages({"key1", "key2"})
    private String field2;
    
    // To still have the default key, explicitly include "".
    // Creates: "field3", "field3_key1", "field3_key2"
    @Messages({"", "key1", "key2"})
    private String field3;
    
    
    // Methods:
    // Keys on methods will NOT have the method name prefixed. They're in the same
    // namespace so to speak as keys declared on the containing type.
    
    
    // Creates: "key3", "key4"
    @Messages({"key3", "key4"})
    public void someMethod()
    {
    }
    
  }

}
