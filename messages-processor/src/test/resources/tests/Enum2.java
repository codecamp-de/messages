import de.codecamp.messages.Messages;


/* Not annotated at the type-level, therefore it's not treated as an enum. */
public enum Enum2
{

  ENUM_CONSTANT1,

  // Not treated as an enum constant, so the default key is NOT created.
  // creates "ENUM_CONSTANT2_addKey1", "ENUM_CONSTANT2_addKey2"
  @Messages({"addKey1", "addKey2"})
  ENUM_CONSTANT2,

  ENUM_CONSTANT3;

}
