import de.codecamp.messages.Messages;


/*
 * Class that isn't annotated on the type-level. Should make no difference for other @Messages in
 * class.
 */
public class BasicsNotOnType
{

  @Messages
  private String field;

  @Messages({"key1", "key2"})
  private String field2;

  @Messages({"", "key1", "key2"})
  private String field3;


  @Messages({"key3", "key4"})
  public void someMethod()
  {
  }

}
