import de.codecamp.messages.Messages;
import de.codecamp.messages.MessagesFor;


@MessagesFor(type = TypeTop.class, forProperties = true)
@Messages(forProperties = true)
public class ExternallyHierarchy
  extends TypeMiddle
{

  public String getProperty3() {
    return null;
  }

  @Messages(keys={"addKey"})
  @Override
  public String getPropertyOverride() {
    return null;
  }

  @Override
  public String getPropertyOverride2() {
    return null;
  }

}

class TypeMiddle
  extends TypeTop
{

  public String getProperty2() {
    return null;
  }

  public String getPropertyOverride2() {
    return null;
  }

}

class TypeTop
{

  public String getProperty1() {
    return null;
  }

  public String getPropertyOverride() {
    return null;
  }

}
