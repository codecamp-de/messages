import de.codecamp.messages.Messages;


/**
 * @Override is not part of the resulting class file. As such, it is not reliable to use in the
 * processor.
 */
@Messages(forProperties = true)
public class InlinedMultipleSources
  extends Impl
{

//  @Override
  public String getId() {
    return null;
  }

}

class Impl
  implements Iface
{

//  @Override
  public String getId() {
    return null;
  }

}

interface Iface
{

  String getId();

}
