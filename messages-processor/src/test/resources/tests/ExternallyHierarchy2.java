import de.codecamp.messages.Messages;
import de.codecamp.messages.MessagesFor;


@MessagesFor(type = ExternallyHierarchy2.class, forProperties = true)
@MessagesFor(type = TypeMiddle.class, forProperties = true)
@MessagesFor(type = TypeTop.class, forProperties = true)
public class ExternallyHierarchy2
  extends TypeMiddle
{

  public String getProperty3() {
    return null;
  }

}

class TypeMiddle
  extends TypeTop
{

  public String getProperty2() {
    return null;
  }

}

class TypeTop
{

  public String getProperty1() {
    return null;
  }

}
