import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import de.codecamp.messages.Messages;


@ComposedAnnotation
@ComposedAnnotation2
public class Meta
{

  @ComposedAnnotation
  @ComposedAnnotation2
  private Object field;

}

@Messages({"fromMeta"})
@Target({ElementType.TYPE, ElementType.FIELD, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@interface ComposedAnnotation
{

}

@Messages({"fromMeta2"})
@Target({ElementType.TYPE, ElementType.FIELD, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@interface ComposedAnnotation2
{

}
