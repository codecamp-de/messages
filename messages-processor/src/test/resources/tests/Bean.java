import de.codecamp.messages.Messages;


@Messages(forType = true, forProperties = true, keys = {"addKey"})
public class Bean
{

  public boolean isBooleanProperty()
  {
    return false;
  }

  public String getProperty1()
  {
    return null;
  }

  @Messages(ignoreImplicit = true)
  public String getProperty2()
  {
    return null;
  }

  // keys on methods don't inculude the method name; but for getters the property name is included 
  @Messages("addKey")
  public String getProperty3()
  {
    return null;
  }

  public String notAProperty()
  {
    return null;
  }

  public void getAlsoNotAProperty()
  {
  }

  public static String getAlsoNotAProperty2()
  {
    return null;
  }

}
