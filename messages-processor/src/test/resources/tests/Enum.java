import de.codecamp.messages.Messages;


/* To get the enum treatment, it must be annotated at the type-level. */
@Messages
public enum Enum
{

  ENUM_CONSTANT1,

  // for enum constants the default key is always created
  // creates "ENUM_CONSTANT2", "ENUM_CONSTANT2_addKey1", "ENUM_CONSTANT2_addKey2"
  @Messages({"addKey1", "addKey2"})
  ENUM_CONSTANT2,

  @Messages(ignoreImplicit = true)
  ENUM_CONSTANT3;

}
