package de.codecamp.messages.shared.conf;


/**
 * Determines what to do when a key doesn't match the bundle name it's found in.
 */
public enum BundleMismatchPolicy
{
  FAIL,
  MOVE,
  WARN,
  NONE,
}
