package de.codecamp.messages.shared.model;


import de.codecamp.messages.shared.conf.BundleMapping;
import java.util.List;
import java.util.Locale;


public interface MessageModule
{

  String getModuleName();

  List<Locale> getTargetLocales();

  List<BundleMapping> getBundleMappings();

  List<String> getImportedModules();

  String getMessageFormat();

}
