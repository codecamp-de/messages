package de.codecamp.messages.shared.model;


import com.google.common.collect.SortedSetMultimap;
import com.google.common.collect.TreeMultimap;
import de.codecamp.messages.shared.conf.BundleMapping;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.stream.Stream;


/**
 * The message key index file contains the information which message keys this project / module
 * declares and where they are declared in the source.
 */
public class MessageKeyIndex
  extends
    AbstractPersistableData
  implements
    MessageModule
{

  private static final String FILE_NAME_SUFFIX = "_messagekeys.json";


  private String moduleName;

  private List<Locale> targetLocales = new ArrayList<>();

  private List<BundleMapping> bundleMappings;

  private List<String> importedModules = new ArrayList<>();

  private String messageFormat;

  /** FQ source type -> message key */
  private SortedSetMultimap<String, MessageKeyWithSourceLocation> keysPerType =
      TreeMultimap.create();

  /** FQ source type -> codegen package (where non-default) */
  private final SortedMap<String, String> codegenPackagePerType = new TreeMap<>();


  private volatile Map<String, MessageKeyWithSourceLocation> keyCache;


  public MessageKeyIndex()
  {
  }


  @Override
  public String getModuleName()
  {
    return moduleName;
  }

  public void setModuleName(String moduleName)
  {
    this.moduleName = moduleName;
  }

  @Override
  public List<Locale> getTargetLocales()
  {
    return targetLocales;
  }

  public void setTargetLocales(List<Locale> targetLocales)
  {
    this.targetLocales = targetLocales;
  }

  @Override
  public List<BundleMapping> getBundleMappings()
  {
    return new ArrayList<>(bundleMappings);
  }

  public void setBundleMappings(List<BundleMapping> bundleMappings)
  {
    this.bundleMappings = bundleMappings;
  }

  @Override
  public List<String> getImportedModules()
  {
    return new ArrayList<>(importedModules);
  }

  public void setImportedModules(List<String> importedModules)
  {
    this.importedModules = importedModules;
  }

  @Override
  public String getMessageFormat()
  {
    return messageFormat;
  }

  public void setMessageFormat(String messageFormat)
  {
    this.messageFormat = messageFormat;
  }

  public boolean containsKey(String key)
  {
    return keysPerType.values().stream().map(MessageKeyWithSourceLocation::getCode)
        .filter(key::equals).findAny().isPresent();
  }

  public boolean hasSourceType(String sourceType)
  {
    return keysPerType.containsKey(sourceType);
  }

  public Stream<String> getSourceTypes()
  {
    return keysPerType.keys().stream();
  }

  public int countSourceTypes()
  {
    return keysPerType.keys().size();
  }

  public Stream<MessageKeyWithSourceLocation> getKeysForType(String sourceType)
  {
    return keysPerType.get(sourceType).stream();
  }

  public MessageKeyWithSourceLocation getKey(String code)
  {
    if (keyCache == null)
    {
      keyCache = new HashMap<>();
      for (MessageKeyWithSourceLocation messageKey : keysPerType.values())
      {
        keyCache.put(messageKey.getCode(), messageKey);
      }
    }

    return keyCache.get(code);
  }

  public int countKeys(String sourceType)
  {
    return keysPerType.get(sourceType).size();
  }

  public Stream<MessageKeyWithSourceLocation> getKeys()
  {
    return keysPerType.values().stream();
  }

  public int countKeys()
  {
    return keysPerType.values().size();
  }

  public void updateMessageKeysFor(String sourceType, Set<MessageKeyWithSourceLocation> messageKeys)
  {
    Set<MessageKeyWithSourceLocation> newKeysPlusLocation;
    if (messageKeys != null)
      newKeysPlusLocation = new HashSet<>(messageKeys);
    else
      newKeysPlusLocation = Collections.emptySet();

    keysPerType.replaceValues(sourceType, newKeysPlusLocation);

    keyCache = null;
  }

  public void updateCodegenPackageFor(String sourceType, String codegenPackage)
  {
    codegenPackagePerType.put(sourceType, codegenPackage);
  }

  public int getSize()
  {
    return keysPerType.size();
  }

  public String getCodegenPackage(String sourceType)
  {
    return codegenPackagePerType.get(sourceType);
  }



  public String getPreferredFilePath()
  {
    if (moduleName == null)
      throw new IllegalStateException("moduleName must be set");

    return getPreferredFilePath(getModuleName());
  }

  public static String getPreferredFilePath(String moduleName)
  {
    if (moduleName == null)
      throw new IllegalArgumentException("moduleName must be set");

    /*
     * Putting them in META-INF doesn't work because annotation processors can't load resources from
     * there; the Filer filters it out.
     */
    return moduleName + FILE_NAME_SUFFIX;
  }

  public static boolean isIndexFile(String filename)
  {
    return filename.endsWith(FILE_NAME_SUFFIX);
  }

  public static MessageKeyIndex readFrom(InputStream inputStream)
    throws PersistableDataException
  {
    return readFrom(inputStream, MessageKeyIndex.class);
  }

  public static MessageKeyIndex readFrom(Path path)
    throws PersistableDataException
  {
    return readFrom(path, MessageKeyIndex.class);
  }

  public static MessageKeyIndex copyOf(MessageKeyIndex index)
  {
    MessageKeyIndex copy = new MessageKeyIndex();
    copy.setModuleName(index.getModuleName());
    copy.setTargetLocales(index.getTargetLocales());
    copy.setBundleMappings(index.getBundleMappings());
    copy.setImportedModules(index.getImportedModules());
    copy.setMessageFormat(index.getMessageFormat());
    copy.keysPerType = TreeMultimap.create(index.keysPerType);
    return copy;
  }

}
