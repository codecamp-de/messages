package de.codecamp.messages.shared.conf;


/**
 * Determines what to do when a key or message is missing from a bundle file.
 */
public enum MissingMessagePolicy
{
  FAIL,
  ADD,
  ADD_AND_WARN,
  WARN,
  NONE,
}
