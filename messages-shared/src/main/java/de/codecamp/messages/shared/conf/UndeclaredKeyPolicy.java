package de.codecamp.messages.shared.conf;


/**
 * Determines what to do when a key in a bundle file is not declared anywhere.
 */
public enum UndeclaredKeyPolicy
{
  FAIL,
  WARN,
  ADD_COMMENT_AND_WARN,
  REMOVE,
  NONE,
}
