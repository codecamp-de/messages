package de.codecamp.messages.shared.conf;


public enum Mode
{

  RELEASE(
      MissingMessagePolicy.FAIL,
      MessageArgPolicy.FAIL,
      UndeclaredKeyPolicy.WARN,
      BundleMismatchPolicy.WARN),

  SNAPSHOT(
      MissingMessagePolicy.WARN,
      MessageArgPolicy.WARN,
      UndeclaredKeyPolicy.WARN,
      BundleMismatchPolicy.WARN),

  DEV(
      MissingMessagePolicy.ADD_AND_WARN,
      MessageArgPolicy.WARN,
      UndeclaredKeyPolicy.ADD_COMMENT_AND_WARN,
      BundleMismatchPolicy.MOVE),

  DEV_ECLIPSE(
      MissingMessagePolicy.NONE,
      MessageArgPolicy.NONE,
      UndeclaredKeyPolicy.NONE,
      BundleMismatchPolicy.NONE),
  // MissingMessagePolicy.ADD,
  // MessageArgPolicy.WARN,
  // UndeclaredKeyPolicy.ADD_COMMENT_AND_WARN,
  // BundleMismatchPolicy.MOVE),

  KEYS_ONLY(
      MissingMessagePolicy.NONE,
      MessageArgPolicy.NONE,
      UndeclaredKeyPolicy.NONE,
      BundleMismatchPolicy.NONE);


  private final MissingMessagePolicy missingMessagePolicy;

  private final MessageArgPolicy messageArgPolicy;

  private final UndeclaredKeyPolicy undeclaredKeyPolicy;

  private final BundleMismatchPolicy bundleMismatchPolicy;


  Mode(MissingMessagePolicy missingMessagePolicy, MessageArgPolicy messageArgPolicy,
      UndeclaredKeyPolicy undeclaredKeyPolicy, BundleMismatchPolicy bundleMismatchPolicy)
  {
    this.missingMessagePolicy = missingMessagePolicy;
    this.messageArgPolicy = messageArgPolicy;
    this.undeclaredKeyPolicy = undeclaredKeyPolicy;
    this.bundleMismatchPolicy = bundleMismatchPolicy;
  }


  public MissingMessagePolicy missingMessagePolicy()
  {
    return missingMessagePolicy;
  }

  public MessageArgPolicy messageArgPolicy()
  {
    return messageArgPolicy;
  }

  public UndeclaredKeyPolicy undeclaredKeyPolicy()
  {
    return undeclaredKeyPolicy;
  }

  public BundleMismatchPolicy bundleMismatchPolicy()
  {
    return bundleMismatchPolicy;
  }

}
