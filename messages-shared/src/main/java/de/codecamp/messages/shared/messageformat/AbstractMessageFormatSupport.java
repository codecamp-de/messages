package de.codecamp.messages.shared.messageformat;


import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.OffsetTime;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang3.StringUtils;


public abstract class AbstractMessageFormatSupport
  implements
    MessageFormatSupport
{

  public static final List<String> JAVATYPES_INTEGER = List.of( //
      byte.class.getName(), Byte.class.getName(), //
      short.class.getName(), Short.class.getName(), //
      int.class.getName(), Integer.class.getName(), //
      long.class.getName(), Long.class.getName(), //
      BigInteger.class.getName() //
  );

  public static final List<String> JAVATYPES_DECIMAL = List.of( //
      float.class.getName(), Float.class.getName(), //
      double.class.getName(), Double.class.getName(), //
      BigDecimal.class.getName() //
  );

  public static final List<String> JAVATYPES_DATETIME = List.of( //
      Date.class.getName(), Calendar.class.getName(), LocalDateTime.class.getName(),
      ZonedDateTime.class.getName(), OffsetDateTime.class.getName(), Instant.class.getName() //
  );

  public static final List<String> JAVATYPES_DATE = List.of( //
      LocalDate.class.getName() //
  );

  public static final List<String> JAVATYPES_TIME = List.of( //
      LocalTime.class.getName(), OffsetTime.class.getName() //
  );


  /**
   * Formats the given Java-type of a message argument to something less technical where possible.
   * The result is only intended for display purposes.
   *
   * @param argType
   *          the Java-type of the message argument
   * @return the formatted message argument type
   */
  @Override
  public String formatArgType(String argType)
  {
    if (String.class.getName().equals(argType))
    {
      return "text";
    }

    else if (JAVATYPES_INTEGER.contains(argType))
    {
      return "integer";
    }
    else if (JAVATYPES_DECIMAL.contains(argType))
    {
      return "decimal";
    }

    else if (JAVATYPES_DATETIME.contains(argType))
    {
      return "date/time";
    }
    else if (JAVATYPES_DATE.contains(argType))
    {
      return "date";
    }
    else if (JAVATYPES_TIME.contains(argType))
    {
      return "time";
    }

    else
    {
      return StringUtils.removeStart(argType, "java.lang.");
    }
  }

}
