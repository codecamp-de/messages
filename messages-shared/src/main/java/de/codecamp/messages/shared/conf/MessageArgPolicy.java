package de.codecamp.messages.shared.conf;


public enum MessageArgPolicy
{
  FAIL,
  WARN,
  NONE,
}
