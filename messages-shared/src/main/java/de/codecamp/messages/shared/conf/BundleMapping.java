package de.codecamp.messages.shared.conf;


import java.util.Objects;
import java.util.regex.Pattern;


public class BundleMapping
{

  public static final BundleMapping IMPORTS_PLACEHOLDER = new BundleMapping(null, null);

  private static final Pattern MATCH_ALL = Pattern.compile(".*");


  private final String messageKeyPattern;

  // should not be serialized into message key index
  private transient Pattern messageKeyPatternRegex;

  private final String bundleNamePattern;


  public BundleMapping(String messageKeyPattern, String bundleNamePattern)
  {
    this.messageKeyPattern = messageKeyPattern;
    this.bundleNamePattern = bundleNamePattern;
  }


  /**
   * Return the message key pattern.
   *
   * @return the message key pattern
   */
  public String getMessageKeyPattern()
  {
    return messageKeyPattern;
  }

  public Pattern getMessageKeyPatternAsRegex()
  {
    if (messageKeyPatternRegex == null)
      messageKeyPatternRegex = toRegexPattern(messageKeyPattern);

    return messageKeyPatternRegex;
  }

  /**
   * Return the target bundle name.
   *
   * @return the target bundle name
   */
  public String getBundleNamePattern()
  {
    return bundleNamePattern;
  }


  @Override
  public int hashCode()
  {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((messageKeyPattern == null) ? 0 : messageKeyPattern.hashCode());
    result = prime * result + ((bundleNamePattern == null) ? 0 : bundleNamePattern.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj)
  {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;

    BundleMapping other = (BundleMapping) obj;
    if (!Objects.equals(messageKeyPattern, other.messageKeyPattern))
      return false;
    if (!Objects.equals(bundleNamePattern, other.bundleNamePattern))
      return false;

    return true;
  }


  private static Pattern toRegexPattern(String messageKeyPattern)
  {
    if (messageKeyPattern.isEmpty())
      return MATCH_ALL;

    StringBuilder patternBuilder = new StringBuilder();
    patternBuilder.append("^");
    boolean firstSegment = true;
    for (String segment : messageKeyPattern.split("\\."))
    {
      if (firstSegment)
        firstSegment = false;
      else
        patternBuilder.append(Pattern.quote("."));

      // split segments around any number of "*", while also keeping the delimiter
      for (String token : segment.split("(?<=\\*+)(?!\\*)|(?<!\\*)(?=\\*+)"))
      {
        if (token.charAt(0) == '*')
        {
          if (token.length() == 1)
          {
            patternBuilder.append("[^\\.]*");
          }
          else
          {
            patternBuilder.append(".*");
          }
        }
        else
        {
          patternBuilder.append(Pattern.quote(token));
        }
      }
    }

    patternBuilder.append("(\\..*)");

    patternBuilder.append("$");

    return Pattern.compile(patternBuilder.toString());
  }

}
