package de.codecamp.messages.shared.validation;


import static java.util.stream.Collectors.toSet;

import de.codecamp.messages.shared.model.MessageModule;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Deque;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.function.BiPredicate;


public final class MessageValidationUtils
{

  private MessageValidationUtils()
  {
    // utility class
  }


  public static Set<Locale> getRequiredLocales(Collection<Locale> locales)
  {
    Set<Locale> requiredLocales = new LinkedHashSet<>();
    for (String language : getLanguages(locales))
    {
      Locale languageOnlyLocale = getLanguageOnlyLocale(locales, language);
      if (languageOnlyLocale != null)
      {
        requiredLocales.add(languageOnlyLocale);
      }
      else
      {
        requiredLocales.addAll(getLocalesForLanguage(locales, language));
      }
    }
    return requiredLocales;
  }

  private static Set<String> getLanguages(Collection<Locale> locales)
  {
    return locales.stream().map(Locale::getLanguage).collect(toSet());
  }

  private static Set<Locale> getLocalesForLanguage(Collection<Locale> locales, String language)
  {
    return locales.stream().filter(locale -> locale.getLanguage().equals(language))
        .collect(toSet());
  }

  private static Locale getLanguageOnlyLocale(Collection<Locale> locales, String language)
  {
    return locales.stream()
        .filter(locale -> locale.getLanguage().equals(language) && locale.getCountry().isEmpty())
        .findAny().orElse(null);
  }


  public static Set<Locale> getRequiredLocales(String messageKeyCode, MessageModule index,
      Map<String, ? extends MessageModule> importedIndexes,
      BiPredicate<MessageModule, String> keyChecker)
  {
    Set<Locale> requiredLocales = getRequiredLocales(
        getTargetLocales(messageKeyCode, false, index, importedIndexes, keyChecker));
    // remove target locales already provided by imports
    requiredLocales
        .removeAll(getTargetLocales(messageKeyCode, true, index, importedIndexes, keyChecker));
    return requiredLocales;
  }

  public static Set<Locale> getTargetLocales(String messageKeyCode, boolean importsOnly,
      MessageModule index, Map<String, ? extends MessageModule> importedIndexes,
      BiPredicate<MessageModule, String> keyChecker)
  {
    Deque<List<MessageModule>> stack = new ArrayDeque<>();

    if (importsOnly)
    {
      for (String importedModule : index.getImportedModules())
      {
        MessageModule importedIndex = importedIndexes.get(importedModule);
        if (importedIndex == null)
          throw new IllegalStateException("Imported module not found: " + importedModule);

        stack.push(new ArrayList<>(Arrays.asList(importedIndex)));
      }
    }
    else
    {
      stack.push(new ArrayList<>(Arrays.asList(index)));
    }

    Set<Locale> result = new HashSet<>();

    while (!stack.isEmpty())
    {
      List<MessageModule> chain = stack.pop();

      MessageModule i = chain.get(chain.size() - 1);

      if (keyChecker.test(i, messageKeyCode))
      {
        chain.stream().flatMap(in -> in.getTargetLocales().stream()).forEach(result::add);
      }
      else
      {
        for (String importedModule : i.getImportedModules())
        {
          MessageModule importedIndex = importedIndexes.get(importedModule);
          if (importedIndex != null)
          {
            List<MessageModule> newChain = new ArrayList<>(chain);
            newChain.add(importedIndex);
            stack.push(newChain);
          }
        }
      }
    }

    return result;
  }

}
