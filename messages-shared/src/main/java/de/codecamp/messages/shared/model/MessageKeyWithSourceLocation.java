package de.codecamp.messages.shared.model;


import de.codecamp.messages.MessageKeyWithArgs;
import java.lang.annotation.ElementType;
import java.util.Objects;
import org.apache.commons.lang3.ArrayUtils;


public class MessageKeyWithSourceLocation
  implements
    MessageKeyWithArgs,
    Comparable<MessageKeyWithSourceLocation>
{

  private static final String[] EMPTY_ARG_ARRAY = {};


  private final String code;

  private final String[] argTypes;

  private final String[] argNames;

  private final String sourceType;

  private final String localPart;

  private final ElementType sourceElementType;

  private final String sourceElementName;

  private final boolean defaultMessageAvailable;


  public MessageKeyWithSourceLocation(String code, String[] argTypes, String[] argNames,
      String sourceType, String localPart, ElementType sourceElementType, String sourceElementName,
      boolean defaultMessageAvailable)
  {
    Objects.requireNonNull(code, "code must not be null");
    Objects.requireNonNull(sourceType, "sourceType must not be null");
    Objects.requireNonNull(localPart, "localPart must not be null");
    Objects.requireNonNull(sourceElementType, "sourceElementType must not be null");
    Objects.requireNonNull(sourceElementName, "sourceElementName must not be null");

    this.code = code;
    this.argTypes = argTypes == null ? null : argTypes.clone();
    this.argNames = argNames == null ? null : argNames.clone();
    this.sourceType = sourceType;
    this.localPart = localPart;
    this.sourceElementType = sourceElementType;
    this.sourceElementName = sourceElementName;
    this.defaultMessageAvailable = defaultMessageAvailable;
  }


  @Override
  public String getCode()
  {
    return code;
  }

  @Override
  public boolean hasArgs()
  {
    return ArrayUtils.isNotEmpty(argTypes);
  }

  @Override
  public String[] getArgTypes()
  {
    if (argTypes == null)
      return EMPTY_ARG_ARRAY;
    else
      return argTypes.clone();
  }

  @Override
  public String[] getArgNames()
  {
    if (argNames == null)
      return EMPTY_ARG_ARRAY;
    else
      return argNames.clone();
  }

  public String getSourceType()
  {
    return sourceType;
  }

  public String getLocalPart()
  {
    return localPart;
  }

  public ElementType getSourceElementType()
  {
    return sourceElementType;
  }

  public String getSourceElementName()
  {
    return sourceElementName;
  }

  /**
   * Whether the module that declares the message key provides a locale-agnostic default message.
   *
   * @return the module that declares the message key provides a locale-agnostic default message
   */
  public boolean isDefaultMessageAvailable()
  {
    return defaultMessageAvailable;
  }


  @Override
  public int compareTo(MessageKeyWithSourceLocation o)
  {
    return getCode().compareTo(o.getCode());
  }

  @Override
  public int hashCode()
  {
    return getCode().hashCode();
  }

  @Override
  public boolean equals(Object obj)
  {
    if (obj == null)
      return false;
    if (obj == this)
      return true;
    if (!(obj instanceof MessageKeyWithSourceLocation))
      return false;

    MessageKeyWithSourceLocation other = (MessageKeyWithSourceLocation) obj;

    return getCode().equals(other.getCode());
  }

}
