package de.codecamp.messages.shared.messageformat;


import static java.util.stream.Collectors.toSet;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.SetMultimap;
import com.ibm.icu.text.MessageFormat;
import com.ibm.icu.text.MessagePatternUtil;
import com.ibm.icu.text.MessagePatternUtil.ArgNode;
import com.ibm.icu.text.MessagePatternUtil.ComplexArgStyleNode;
import com.ibm.icu.text.MessagePatternUtil.MessageContentsNode;
import com.ibm.icu.text.MessagePatternUtil.MessageContentsNode.Type;
import com.ibm.icu.text.MessagePatternUtil.MessageNode;
import com.ibm.icu.text.MessagePatternUtil.VariantNode;
import com.ibm.icu.util.Calendar;
import com.ibm.icu.util.CurrencyAmount;
import de.codecamp.messages.MessageKeyWithArgs;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Stream;
import org.apache.commons.lang3.StringUtils;


/**
 * Implements {@link MessageFormatSupport} for {@link MessageFormat ICU's MessageFormat}.
 * <p>
 * If changes are made to the checked types, this must be reflected in
 * {@link de.codecamp.messages.runtime.IcuMessageArgConverter}.
 */
public class IcuMessageFormatSupport
  extends
    AbstractMessageFormatSupport
{

  public static final String ID = "icu";


  private static final String ARGTYPE_NUMBER = "number";

  private static final String ARGTYPE_DATE = "date";

  private static final String ARGTYPE_TIME = "time";


  private static final List<String> JAVATYPES_CURRENCY = List.of( //
      CurrencyAmount.class.getName(), "javax.money.MonetaryAmount" //
  );


  /**
   * the supported Java types for each argument type of the formatter
   */
  private static final SetMultimap<String, String> ARGTYPE_TO_JAVA_MAPPING = HashMultimap.create();
  static
  {
    ARGTYPE_TO_JAVA_MAPPING.putAll(ARGTYPE_NUMBER, Arrays.asList(Number.class.getName()));
    ARGTYPE_TO_JAVA_MAPPING.putAll(ARGTYPE_NUMBER, JAVATYPES_CURRENCY);

    ARGTYPE_TO_JAVA_MAPPING.putAll(ARGTYPE_DATE,
        Arrays.asList(Calendar.class.getName(), Number.class.getName()));
    ARGTYPE_TO_JAVA_MAPPING.putAll(ARGTYPE_DATE, JAVATYPES_DATETIME);
    ARGTYPE_TO_JAVA_MAPPING.putAll(ARGTYPE_DATE, JAVATYPES_DATE);
    ARGTYPE_TO_JAVA_MAPPING.putAll(ARGTYPE_DATE, JAVATYPES_TIME);

    ARGTYPE_TO_JAVA_MAPPING.putAll(ARGTYPE_TIME,
        Arrays.asList(Calendar.class.getName(), Number.class.getName()));
    ARGTYPE_TO_JAVA_MAPPING.putAll(ARGTYPE_TIME, JAVATYPES_DATETIME);
    ARGTYPE_TO_JAVA_MAPPING.putAll(ARGTYPE_TIME, JAVATYPES_DATE);
    ARGTYPE_TO_JAVA_MAPPING.putAll(ARGTYPE_TIME, JAVATYPES_TIME);

    ARGTYPE_TO_JAVA_MAPPING.putAll("spellout", Arrays.asList(Number.class.getName()));
    ARGTYPE_TO_JAVA_MAPPING.putAll("ordinal", Arrays.asList(Number.class.getName()));
    ARGTYPE_TO_JAVA_MAPPING.putAll("duration", Arrays.asList(Number.class.getName()));
    ARGTYPE_TO_JAVA_MAPPING.putAll("plural", Arrays.asList(Number.class.getName()));
    ARGTYPE_TO_JAVA_MAPPING.putAll("select", Arrays.asList(String.class.getName()));
    ARGTYPE_TO_JAVA_MAPPING.putAll("choice", Arrays.asList(Number.class.getName()));
  }

  /**
   * the matching or most likely argument type for each supported Java type
   */
  private static final Map<String, String> JAVA_TO_ARGTYPE__MAPPING = new HashMap<>();
  static
  {
    JAVATYPES_INTEGER.forEach(type -> JAVA_TO_ARGTYPE__MAPPING.put(type, ARGTYPE_NUMBER));
    JAVATYPES_DECIMAL.forEach(type -> JAVA_TO_ARGTYPE__MAPPING.put(type, ARGTYPE_NUMBER));

    JAVATYPES_CURRENCY
        .forEach(type -> JAVA_TO_ARGTYPE__MAPPING.put(type, ARGTYPE_NUMBER + ",currency"));

    JAVATYPES_DATETIME.forEach(type -> JAVA_TO_ARGTYPE__MAPPING.put(type, ARGTYPE_DATE));
    JAVATYPES_DATE.forEach(type -> JAVA_TO_ARGTYPE__MAPPING.put(type, ARGTYPE_DATE));
    JAVA_TO_ARGTYPE__MAPPING.put(Calendar.class.getName(), ARGTYPE_DATE);

    JAVATYPES_TIME.forEach(type -> JAVA_TO_ARGTYPE__MAPPING.put(type, ARGTYPE_TIME));
  }


  @Override
  public boolean supportsFormat(String messageFormatId)
  {
    return ID.equals(messageFormatId) || DefaultMessageFormatSupport.ID.equals(messageFormatId);
  }

  @Override
  public boolean hasArgNameSupport()
  {
    return true;
  }


  @Override
  public List<ArgInsert> getArgInsertOptions(MessageKeyWithArgs key)
  {
    List<ArgInsert> result = new ArrayList<>();

    String[] argTypes = key.getArgTypes();
    String[] argNames = key.getArgNames();

    for (int i = 0; i < argTypes.length; i++)
    {
      String label = argNames[i] + " : " + formatArgType(argTypes[i]);
      String reference = toMessageFormateArgument(argNames[i], argTypes[i]);

      result.add(new ArgInsert(label, reference));
    }

    return result;
  }

  @Override
  public String createMessageBundleComment(MessageKeyWithArgs key)
  {
    String comment;
    if (key.hasArgs())
    {
      String[] argTypes = key.getArgTypes();
      String[] argNames = key.getArgNames();

      StringBuilder messageArgComment = new StringBuilder(50);

      messageArgComment.append("Arguments: ");

      boolean first = true;
      for (int i = 0; i < argTypes.length; i++)
      {
        if (first)
          first = false;
        else
          messageArgComment.append(" | ");

        messageArgComment.append(argNames[i]);
        messageArgComment.append(" (").append(formatArgType(argTypes[i])).append(")");
        messageArgComment.append(" -> ");
        messageArgComment.append(toMessageFormateArgument(argNames[i], argTypes[i]));
      }

      comment = messageArgComment.toString();
    }
    else
    {
      comment = null;
    }
    return comment;
  }

  private static String toMessageFormateArgument(String argName, String javaArgType)
  {
    StringBuilder reference = new StringBuilder(20);
    reference.append("{");
    reference.append(argName);

    String mfArgType = JAVA_TO_ARGTYPE__MAPPING.get(javaArgType);
    if (mfArgType != null)
    {
      reference.append(",").append(mfArgType);
    }

    reference.append("}");
    return reference.toString();
  }

  @Override
  public String formatArgType(String argType)
  {
    if ("javax.money.MonetaryAmount".equals(argType)
        || CurrencyAmount.class.getName().equals(argType))
    {
      return "currency";
    }
    else
    {
      return super.formatArgType(argType);
    }
  }


  @Override
  public List<String> checkMessage(String message, String[] argTypes, String[] argNames,
      TypeChecker argTypeChecker)
  {
    List<String> errors = new ArrayList<>();

    MessageFormat mf;
    try
    {
      mf = new MessageFormat(message);
    }
    catch (IllegalArgumentException ex)
    {
      errors.add(String.format("The message is not a valid ICU pattern: %s", ex.getMessage()));
      return errors;
    }


    if (mf.usesNamedArguments())
    {
      Set<String> availableArgNames = Stream.of(argNames).filter(Objects::nonNull).collect(toSet());

      Deque<MessageNode> messageNodes = new ArrayDeque<>();
      messageNodes.push(MessagePatternUtil.buildMessageNode(message));
      while (!messageNodes.isEmpty())
      {
        MessageNode messageNode = messageNodes.pop();
        for (MessageContentsNode node : messageNode.getContents())
        {
          if (node.getType() == Type.ARG)
          {
            ArgNode argNode = (ArgNode) node;

            if (argNode.getNumber() > -1)
            {
              errors.add("The message mixes named and indexed arguments.");
              continue;
            }

            ComplexArgStyleNode complexStyle = argNode.getComplexStyle();
            if (complexStyle != null)
            {
              for (VariantNode variantNode : complexStyle.getVariants())
              {
                messageNodes.push(variantNode.getMessage());
              }
            }

            String messageArgName = argNode.getName();
            if (!availableArgNames.contains(messageArgName))
            {
              errors.add(
                  String.format("The message uses an unavailable argument: %s", argNode.getName()));
              continue;
            }


            String typeName = argNode.getTypeName();
            if (typeName != null)
            {
              String argType = null;
              for (int i = 0; i < argNames.length; i++)
              {
                if (messageArgName.equals(argNames[i]))
                  argType = argTypes[i];
              }
              if (argType != null)
                checkTypeName(typeName, argType, argTypeChecker, errors);
            }
          }
        }
      }
    }

    else
    {
      int maxIndex = -1;

      Deque<MessageNode> messageNodes = new ArrayDeque<>();
      messageNodes.push(MessagePatternUtil.buildMessageNode(message));
      while (!messageNodes.isEmpty())
      {
        MessageNode messageNode = messageNodes.pop();
        for (MessageContentsNode node : messageNode.getContents())
        {
          if (node.getType() == Type.ARG)
          {
            ArgNode argNode = (ArgNode) node;

            ComplexArgStyleNode complexStyle = argNode.getComplexStyle();
            if (complexStyle != null)
            {
              for (VariantNode variantNode : complexStyle.getVariants())
              {
                messageNodes.push(variantNode.getMessage());
              }
            }

            if (argNode.getNumber() > maxIndex)
              maxIndex = argNode.getNumber();

            String typeName = argNode.getTypeName();
            if (typeName != null)
            {
              String argType = null;
              if (argTypes.length > argNode.getNumber())
                argType = argTypes[argNode.getNumber()];
              checkTypeName(typeName, argType, argTypeChecker, errors);
            }
          }
        }
      }

      int usedArgCount = maxIndex + 1;
      int argCount = argTypes == null ? 0 : argTypes.length;
      if (usedArgCount > argCount)
      {
        errors.add(String.format("The message uses more arguments (%d) than are declared (%d)",
            usedArgCount, argCount));
      }
    }

    return errors;
  }

  /**
   * Checks if the argType matches the Java type of the argument.
   *
   * @param argType
   *          the {@link ArgNode#getTypeName() type name} of the argument in the message
   * @param argJavaType
   *          the (Java) type of the declared message argument
   */
  private static void checkTypeName(String argType, String argJavaType, TypeChecker argTypeChecker,
      List<String> errors)
  {
    Set<String> expectedJavaTypes = ARGTYPE_TO_JAVA_MAPPING.get(argType);

    if (!argTypeChecker.isCompatibleWith(argJavaType, expectedJavaTypes))
    {
      errors.add(
          String.format("The message format type %s does not match the expected argument types: %s",
              argType, StringUtils.join(expectedJavaTypes, ",")));
    }
  }

}
