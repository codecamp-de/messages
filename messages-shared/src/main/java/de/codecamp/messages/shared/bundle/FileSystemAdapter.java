package de.codecamp.messages.shared.bundle;


import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;


public interface FileSystemAdapter<DIR, FILE, EX extends Exception>
{

  DIR getDirectory(String path);

  List<FILE> listFiles(DIR dir, boolean recursive)
    throws EX;

  FILE getFile(DIR dir, String fileName);


  String getFileName(FILE file);

  String getRelativeFilePath(DIR dir, FILE file);

  String getDisplayPath(DIR dir, FILE file);

  boolean exists(FILE file);

  void createParentDirectories(FILE file)
    throws EX;

  void deleteIfExists(FILE file)
    throws EX;

  InputStream newInputStream(FILE file)
    throws EX;

  OutputStream newOutputStream(FILE file)
    throws EX;

}
