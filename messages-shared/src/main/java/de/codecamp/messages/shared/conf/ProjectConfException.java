package de.codecamp.messages.shared.conf;


public class ProjectConfException
  extends
    Exception
{

  private final String errorCode;

  private final Object[] errorCodeArgs;


  public ProjectConfException(String message, String errorCode, Object... errorCodeArgs)
  {
    super(message);
    this.errorCode = errorCode;
    this.errorCodeArgs = errorCodeArgs.clone();
  }

  public ProjectConfException(String message, Throwable cause, String errorCode,
      Object... errorCodeArgs)
  {
    super(message, cause);
    this.errorCode = errorCode;
    this.errorCodeArgs = errorCodeArgs.clone();
  }


  public String getErrorCode()
  {
    return errorCode;
  }

  public Object[] getErrorCodeArgs()
  {
    return errorCodeArgs.clone();
  }

}
