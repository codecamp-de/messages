package de.codecamp.messages.shared.messageformat;


import de.codecamp.messages.MessageKeyWithArgs;
import de.codecamp.messages.shared.conf.ProjectConf;
import java.util.List;
import java.util.Set;


public interface MessageFormatSupport
{

  boolean supportsFormat(String messageFormatId);

  boolean hasArgNameSupport();


  /**
   * Returns a readable term for what the given Java argument type represents.
   *
   * @param argType
   *          the Java argument type
   * @return a readable term for what the given Java argument type represents
   */
  String formatArgType(String argType);

  List<ArgInsert> getArgInsertOptions(MessageKeyWithArgs key);

  String createMessageBundleComment(MessageKeyWithArgs key);


  List<String> checkMessage(String message, String[] argTypes, String[] argNames,
      TypeChecker argTypeChecker);


  static MessageFormatSupport getSupport(ProjectConf projectConf)
  {
    MessageFormatSupport argsSupport;
    if (projectConf.getMessageFormat().equals(IcuMessageFormatSupport.ID))
    {
      argsSupport = new IcuMessageFormatSupport();
    }
    else if (projectConf.getMessageFormat().equals(DefaultMessageFormatSupport.ID))
    {
      argsSupport = new DefaultMessageFormatSupport();
    }
    else
    {
      throw new IllegalStateException(
          String.format("Unknown message formatter: %s", projectConf.getMessageFormat()));
    }
    return argsSupport;
  }


  class ArgInsert
  {

    private final String label;

    private final String reference;


    public ArgInsert(String label, String reference)
    {
      this.label = label;
      this.reference = reference;
    }


    public String getLabel()
    {
      return label;
    }

    public String getReference()
    {
      return reference;
    }

  }

  interface TypeChecker
  {

    boolean isCompatibleWith(String typeName, Set<String> allowedTypeNames);

  }

}
