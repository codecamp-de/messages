package de.codecamp.messages.shared.conf;


import java.util.Objects;


public class PackageMapping
{

  private final String sourcePackageName;

  private final String targetPackageName;


  public PackageMapping(String sourcePackageName, String targetPackageName)
  {
    this.sourcePackageName = sourcePackageName;
    this.targetPackageName = targetPackageName;
  }


  public String getSourcePackageName()
  {
    return sourcePackageName;
  }

  public String getTargetPackageName()
  {
    return targetPackageName;
  }


  @Override
  public int hashCode()
  {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((sourcePackageName == null) ? 0 : sourcePackageName.hashCode());
    result = prime * result + ((targetPackageName == null) ? 0 : targetPackageName.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj)
  {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;

    PackageMapping other = (PackageMapping) obj;
    if (!Objects.equals(sourcePackageName, other.sourcePackageName))
      return false;
    if (!Objects.equals(targetPackageName, other.targetPackageName))
      return false;

    return true;
  }

}
