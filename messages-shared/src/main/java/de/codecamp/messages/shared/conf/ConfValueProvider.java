package de.codecamp.messages.shared.conf;


@FunctionalInterface
public interface ConfValueProvider
{

  String getConf(String name);

}
