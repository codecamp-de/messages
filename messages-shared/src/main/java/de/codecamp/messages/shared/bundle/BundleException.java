package de.codecamp.messages.shared.bundle;


public class BundleException
  extends
    RuntimeException
{

  public BundleException(String message)
  {
    super(message);
  }

  public BundleException(String message, Throwable cause)
  {
    super(message, cause);
  }

}
